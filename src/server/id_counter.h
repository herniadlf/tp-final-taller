#ifndef __ID_COUNTER_H__
#define __ID_COUNTER_H__

#include <mutex>

namespace server{
	class IdCounter{
	private:
		size_t id;
		std::mutex idMutex;
	public:
		IdCounter();
		~IdCounter();
		size_t newId();
	};
}
#endif
