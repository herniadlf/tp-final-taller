#ifndef __HORD_BUILDER_H__
#define __HORD_BUILDER_H__
#include <vector>
#include <tuple>
#include "builder.h"
#include "../shared/map/hord_info.h"
#include "../shared/map/circuit.h"
#include "id_counter.h"
#include "enemy.h"
#define TICKS_BETWEEN_ENEMIES 70

namespace server{
  class HordBuilder : public Builder{
  private:
    IdCounter& idCounter;
    const size_t hordsQuantity;
    size_t hordsCounter;
    const std::vector<Shared::Circuit*>& circuits;
    const std::vector<Shared::HordInfo*>& hordes;
    size_t ticksBetweenEnemies;
  public:
    HordBuilder(const std::vector<Shared::HordInfo*>& hordes,
                  IdCounter& idCounter,
                const std::vector<Shared::Circuit*>& paths);
    ~HordBuilder();
    bool newGameTick() override; // false means that no more
    bool readyToCreate() override;
    std::vector<Enemy*> build();
  };
}

#endif
