#include "map_change.h"

MapChange::MapChange(const std::string& map,size_t matchId) :
							map(map), matchId(matchId) {}

const std::string MapChange::ELECTION_CREATE_MAP_HEADER = "Ecreate-map";
const std::string MapChange::ID_ARGUMENT 				= "id";

const YAML::Node* MapChange::toYaml() const {
	YAML::Node *command = new YAML::Node();
	(*command)[this->ELECTION_CREATE_MAP_HEADER] 	= this->map;
	(*command)[this->ID_ARGUMENT] 					= this->matchId;
	return command;
}