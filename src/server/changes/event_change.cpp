#include "event_change.h"

EventChange::EventChange(Shared::GAME_OBJECT kind, 
						unsigned int objectId, 
						unsigned int x, 
						unsigned int y, 
						Event request,
						bool result) :
	kind(kind),objectId(objectId), x(x), y(y),
	request(request), result(result) {}

const std::string EventChange::NEW_TOWER_HEADER = "Tnew-tower";
const std::string EventChange::NEW_SPELL_HEADER = "Snew-spell";
const std::string EventChange::RETURN_LABEL 	= "return";
const std::string EventChange::ID_LABEL 		= "id";

unsigned int EventChange::getObjectId() const{
	return this->objectId;
}

unsigned int EventChange::getX() const{
	return this->x;
}

unsigned int EventChange::getY() const{
	return this->y;
}

Shared::GAME_OBJECT EventChange::getKind() const{
	return this->kind;
}

Event EventChange::getRequest() const {
	return this->request;
}

const YAML::Node* EventChange::toYaml() const{
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(x);
	arguments.push_back(y);
	std::string commandHeader;
	switch (this->request){
		case Event::NEW_TOWER:
			commandHeader = this->NEW_TOWER_HEADER;
			break;
		case Event::NEW_SPELL:
			commandHeader = this->NEW_SPELL_HEADER;
			break;
		default:
			return NULL;
	}
	(*command)[commandHeader] = arguments;
	(*command)[RETURN_LABEL] = this->result ? 0 : 1;
	(*command)[ID_LABEL] = objectId;
	return command;
}
