#include "info_change.h"
#include <iostream>
InfoChange::InfoChange(Shared::TowerInfo& info) : yamlInfo(NULL) {
	yamlInfo = info.getInfo();
}

InfoChange::~InfoChange(){}

const std::string InfoChange::INFO_TOWER_HEADER = "Tinfo-tower";

const YAML::Node* InfoChange::toYaml() const{
	YAML::Node* command = new YAML::Node();
	(*command)[INFO_TOWER_HEADER] = *yamlInfo;
	return command;
}