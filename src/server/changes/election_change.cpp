#include "election_change.h"

ElectionChange::ElectionChange() {}

const std::string ElectionChange::ELECTION_START_HEADER = "Estart";

const YAML::Node* ElectionChange::toYaml() const {
	YAML::Node *command = new YAML::Node();
	(*command)[this->ELECTION_START_HEADER] = 0;
	return command;
}
