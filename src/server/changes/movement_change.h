#ifndef __MOVEMENT_CHANGE_H__
#define __MOVEMENT_CHANGE_H__
#include "changes.h"
#include "event_change.h"

class MovementChange: public EventChange{
private:
	Shared::ENEMY_KIND enemyKind;
public:
	MovementChange(Shared::GAME_OBJECT kind,
			unsigned int objectId,
			unsigned int x,
			unsigned int y,
			Event request,
			bool result);
	MovementChange(Shared::GAME_OBJECT kind,
			Shared::ENEMY_KIND enemyKind, 
			unsigned int objectId,
			unsigned int x,
			unsigned int y,
			Event request,
			bool result);
	static const std::string MOVE_HEADER;
	static const std::string CLEAN_HEADER;
	const YAML::Node* toYaml() const override;
};

#endif
