#ifndef __LIST_MATCH_CHANGE_H__
#define __LIST_MATCH_CHANGE_H__
#include "changes.h"
#include <tuple>

//a tuple with id + scenario type
typedef std::tuple<size_t,int> idScenarioTuple;

class ListMatchChange: public Change{
private:
	std::vector<idScenarioTuple> matchs;
public:
	ListMatchChange(std::vector<idScenarioTuple>& matchs);
	const YAML::Node* toYaml() const override;
	static const std::string ELECTION_LIST_MATCHS_COMMAND;
};

#endif
