#ifndef __LIST_MAP_CHANGE_H__
#define __LIST_MAP_CHANGE_H__
#include "changes.h"

class ListMapChange: public Change{
private:
	const std::vector<std::string>& maps;
public:
	ListMapChange(const std::vector<std::string>& maps);
	const YAML::Node* toYaml() const override;
	static const std::string ELECTION_LIST_MAPS_COMMAND;
};

#endif
