#include "movement_change.h"

MovementChange::MovementChange(Shared::GAME_OBJECT kind, 
								unsigned int objectId, 
								unsigned int x, 
								unsigned int y, 
								Event request,
								bool result) :
	EventChange(kind,objectId,x,y,request,result) {}

MovementChange::MovementChange(Shared::GAME_OBJECT kind,
								Shared::ENEMY_KIND enemyKind, 
								unsigned int objectId,
								unsigned int x,
								unsigned int y,
								Event request,
								bool result) :
	EventChange(kind,objectId,x,y,request,result), enemyKind(enemyKind) {}

const std::string MovementChange::MOVE_HEADER = "Mmove-object";
const std::string MovementChange::CLEAN_HEADER = "Mclean-object";

const YAML::Node* MovementChange::toYaml() const{
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(getObjectId());
	arguments.push_back(getX());
	arguments.push_back(getY());
	arguments.push_back(static_cast<unsigned int>(getKind()));
	if (getKind() == Shared::GAME_OBJECT::ENEMY)
		arguments.push_back(static_cast<unsigned int>(this->enemyKind));
	std::string commandHeader;
	switch(getRequest()){
		case Event::MOVEMENT:
			commandHeader = this->MOVE_HEADER;
			break;
		case Event::CLEAN:
			commandHeader = this->CLEAN_HEADER;
			break;
		default:
			return NULL;
	}
	(*command)[commandHeader] = arguments;
	return command;
}