#ifndef __EVENT_CHANGE_H__
#define __EVENT_CHANGE_H__
#include "changes.h"

class EventChange: public Change{
private:	
	Shared::GAME_OBJECT kind;
	unsigned int objectId;
	unsigned int x;
	unsigned int y;
	const Event request;
	const bool result;
protected:
	unsigned int getObjectId() const;
	unsigned int getX() const;
	unsigned int getY() const;
	Shared::GAME_OBJECT getKind() const;
	Event getRequest() const;
public:
	EventChange(Shared::GAME_OBJECT kind,
			unsigned int objectId,
			unsigned int x,
			unsigned int y,
			Event request,
			bool result);
	virtual const YAML::Node* toYaml() const override;
	static const std::string NEW_TOWER_HEADER;
	static const std::string NEW_SPELL_HEADER;
	static const std::string RETURN_LABEL;
	static const std::string ID_LABEL;
};

#endif
