#include "finish_change.h"

FinishChange::FinishChange(const size_t matchId, Event event) : 
								matchId(matchId), event(event) {}

const std::string FinishChange::ID_LABEL 			= "id";
const std::string FinishChange::CODE_LABEL 			= "code";
const std::string FinishChange::FINISH_WON_HEADER 	= "FWon";
const std::string FinishChange::FINISH_LOOSE_HEADER = "FLoose";

const YAML::Node* FinishChange::toYaml() const {
	YAML::Node *command = new YAML::Node();
	std::string commandHeader;
	if (event == Event::WON)
		commandHeader = FINISH_WON_HEADER;
	else if (event == Event::LOOSE)
		commandHeader = FINISH_LOOSE_HEADER;
	(*command)[commandHeader] = 0;
	(*command)[ID_LABEL] = this->matchId;
	return command;
}

const YAML::Node* FinishChange::toInternalYaml() const{
	YAML::Node *command = new YAML::Node();
	YAML::Node codeNode;
	std::string commandHeader;
	if (event == Event::WON)
		commandHeader = FINISH_WON_HEADER;
	else if (event == Event::LOOSE)
		commandHeader = FINISH_LOOSE_HEADER;
	codeNode[commandHeader] = 0;
	(*command)[CODE_LABEL] 	= codeNode;
	(*command)[ID_LABEL] 	= this->matchId;
	return command;
}