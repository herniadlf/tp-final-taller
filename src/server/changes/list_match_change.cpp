#include "list_match_change.h"

const std::string ListMatchChange::ELECTION_LIST_MATCHS_COMMAND = "Elist-match";

#define ID_LABEL "id"
#define SCENARIO_LABEL "scenario"

ListMatchChange::ListMatchChange(std::vector<idScenarioTuple>& matchs) {
	for (idScenarioTuple tuple: matchs){
		idScenarioTuple newTuple(tuple);
		this->matchs.push_back(newTuple);
	}
}

const YAML::Node* ListMatchChange::toYaml() const {
	YAML::Node* command = new YAML::Node();
	YAML::Node matchVector;
	for (const idScenarioTuple& tuple: this->matchs){
		YAML::Node tupleNode;
		tupleNode[ID_LABEL] = std::get<0>(tuple);
		tupleNode[SCENARIO_LABEL] = std::get<1>(tuple);
		matchVector.push_back(tupleNode);
	}
	(*command)[this->ELECTION_LIST_MATCHS_COMMAND] = matchVector;
	return command;
}
