#ifndef __FINISH_CHANGE_H__
#define __FINISH_CHANGE_H__
#include "changes.h"

class FinishChange: public Change{
private:
	const size_t matchId;
	Event event;
public:
	FinishChange(const size_t matchId, Event event);
	const YAML::Node* toYaml() const override;
	const YAML::Node* toInternalYaml() const;
	static const std::string ID_LABEL;
	static const std::string CODE_LABEL;
	static const std::string FINISH_WON_HEADER;
	static const std::string FINISH_LOOSE_HEADER;
};

#endif
