#ifndef __CHANGES_H__
#define __CHANGES_H__
#include "yaml-cpp/yaml.h"
#include "../movable.h"
#include <string>

enum class Event{NEW_TOWER, NEW_SPELL, MOVEMENT, 
				CLEAN, WON, LOOSE, UPGRADE_TOWER,
				INFO_TOWER};

class Change{
public:
	Change();
	virtual ~Change();
	virtual const YAML::Node* toYaml() const = 0;
};

typedef std::tuple<unsigned int, const YAML::Node*> clientmessage;
class Changes{
private:
	typedef std::vector<const YAML::Node*> changevector;
	typedef std::map<unsigned int,changevector>::iterator clientiterator;
	std::map<unsigned int, changevector> clientChanges;
public:
	Changes(unsigned int id);
	Changes(const std::vector<unsigned int>& clients);
	~Changes();
	void add(unsigned int clientId, const Change* change);
	void updateClients(const std::vector<unsigned int>& clients);
	const std::vector<clientmessage> get() const;
};

#endif
