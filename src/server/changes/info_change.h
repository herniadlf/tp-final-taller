#ifndef __INFO_CHANGE_H__
#define __INFO_CHANGE_H__
#include "changes.h"
#include "../../shared/map/tower_info.h"

class InfoChange: public Change{
private:
	YAML::Node* yamlInfo;
public:
	InfoChange(Shared::TowerInfo& info);
	~InfoChange();
	virtual const YAML::Node* toYaml() const override;
	static const std::string INFO_TOWER_HEADER;
};

#endif
