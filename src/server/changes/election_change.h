#ifndef __ELECTION_CHANGE_H__
#define __ELECTION_CHANGE_H__
#include "changes.h"

class ElectionChange: public Change{
public:
	ElectionChange();
	const YAML::Node* toYaml() const override;
	static const std::string ELECTION_START_HEADER;
};

#endif
