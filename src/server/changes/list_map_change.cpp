#include "list_map_change.h"

ListMapChange::ListMapChange(const std::vector<std::string>& maps): 
								maps(maps) {}

const std::string ListMapChange::ELECTION_LIST_MAPS_COMMAND = "Elist-map";
const YAML::Node* ListMapChange::toYaml() const{
	YAML::Node* command = new YAML::Node();
	YAML::Node mapVector;
	for(const std::string& map: maps){
		mapVector.push_back(map);
	}
	(*command)[this->ELECTION_LIST_MAPS_COMMAND] = mapVector;
	return command;
}								