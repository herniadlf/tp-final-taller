#ifndef __CANCEL_CHANGE_H__
#define __CANCEL_CHANGE_H__
#include "changes.h"

class CancelChange: public Change{
private:
	const size_t matchId;
public:
	CancelChange(const size_t matchId);
	const YAML::Node* toYaml() const override;
	static const std::string CANCEL_HEADER;
};

#endif
