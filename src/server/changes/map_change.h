#ifndef __MAP_CHANGE_H__
#define __MAP_CHANGE_H__
#include "changes.h"

class MapChange: public Change{
private:
	const std::string& map;
	size_t matchId;
public:
	MapChange(const std::string& map, size_t matchId);
	const YAML::Node* toYaml() const override;
	static const std::string ELECTION_CREATE_MAP_HEADER;
	static const std::string ID_ARGUMENT;
};

#endif
