#include "cancel_change.h"

CancelChange::CancelChange(const size_t matchId) : matchId(matchId) {}

const std::string CancelChange::CANCEL_HEADER = "Ecancel-create";

const YAML::Node* CancelChange::toYaml() const {
	YAML::Node *command = new YAML::Node();
	(*command)[this->CANCEL_HEADER] = this->matchId;
	return command;
}