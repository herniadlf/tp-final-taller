 #include "changes.h"
#include <iostream> //FORTEST

///////////////////////
// CHANGE
///////////////////////

Change::Change() {}

Change::~Change() {}

///////////////////////
// CHANGES
///////////////////////

Changes::Changes(unsigned int id) {
	changevector vector;
	this->clientChanges.insert(std::pair<unsigned int,changevector>(id,vector));	
}


Changes::Changes(const std::vector<unsigned int>& clients) {
	for (const unsigned int id: clients){
		changevector vector;
		this->clientChanges.insert(std::pair<unsigned int,changevector>(id,vector));
	}
}

Changes::~Changes() {}

void Changes::add(unsigned int clientId, const Change* change){
	if (clientId == 0){ //is broadcast
		const clientiterator init = this->clientChanges.begin();
		const clientiterator end = this->clientChanges.end();
		for (clientiterator it=init; it!=end; ++it)
			it->second.push_back(change->toYaml());
	} else {
		this->clientChanges[clientId].push_back(change->toYaml());
	}
	delete change;
}

const std::vector<clientmessage> Changes::get() const{
	std::vector<clientmessage> vector;
	auto init = this->clientChanges.begin();
	auto end = this->clientChanges.end();
	for (auto it=init; it!=end; ++it){
		if (it->second.size() > 0) {
			YAML::Node* mainNode = new YAML::Node();
			unsigned int clientId = it->first;
			for (const YAML::Node* changeNode: it->second){
				mainNode->push_back(*changeNode);
				delete changeNode;
			}
			clientmessage clientMessage(std::make_tuple(clientId,mainNode));
			vector.push_back(clientMessage);
		}
	}
	return vector;
}

void Changes::updateClients(const std::vector<unsigned int>& clients){
	for (unsigned int client: clients){
		clientiterator it = this->clientChanges.find(client);
		if (it == this->clientChanges.end()){
			changevector vector;
			this->clientChanges.insert(std::pair<unsigned int,changevector>(client,vector));			
		}
	}
}


