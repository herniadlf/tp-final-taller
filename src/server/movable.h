#ifndef __MOVABLE_H__
#define __MOVABLE_H__
#include <utility>
#include <vector>
#include <cstddef>
#include "../shared/map/position.h"
#include "../shared/map/game_object.h"

#define CIRCUIT_FINISHED -1
#define CIRCUIT_ON 1
#define NOT_YET 0

#define STEP_SIZE 1
#define RATE_POS_COORD 40.0
#define RATE_COORD_POS 1.0/RATE_POS_COORD

#define MOVEMENT_VERSOR 1 // significa 1 paso cada 10 game ticks

namespace server{
  class Movable{
  protected:
    std::vector<Shared::Position> circuit;
    unsigned int id;
    const size_t speed;
    size_t moveCountDown;
    size_t step_counter;
    size_t tickCounter;
    size_t ratio;
    bool isMovableActive;
    std::vector<Shared::Position>::iterator iterator;
    Shared::Position* actualPosition;
    bool canMove();
  public:
    Movable(const std::vector<Shared::Position>& circuit,
              unsigned int id,
              const size_t speed,
              size_t moveCountDown);
    virtual ~Movable();
    virtual int move();
    virtual Shared::GAME_OBJECT getKind() = 0;
    virtual bool isActive() = 0;
    bool isMoving();
    unsigned int getX();
    unsigned int getY();
    unsigned int getId();
    size_t getRatio();
  };
}
#endif
