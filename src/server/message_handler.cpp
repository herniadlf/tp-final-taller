#include "message_handler.h"

using namespace server;

MessageHandler::MessageHandler() {}
MessageHandler::~MessageHandler() {
	auto gameMessagesBegin = this->handler.begin();
	auto gameMessagesEnd = this->handler.end();
	for (auto it=gameMessagesBegin; it!=gameMessagesEnd; ++it)
		delete it->second;
}

void MessageHandler::insert(const char key,GameExecutor* value){
	this->handler.insert(std::pair<const char, GameExecutor*>(key,value));
}

const GameExecutor& MessageHandler::get(char key) {
	const GameExecutor* executor = handler[key];
	return *executor;
}

