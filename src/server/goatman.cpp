#include "goatman.h"
using namespace server;
Goatman::Goatman(const std::vector<Shared::Position>& circuit,
                  					unsigned int id,
                  					const size_t speed,
                  					const size_t moveCountDown,
                  					const size_t lifePoints) :
    Enemy(circuit,id,speed,moveCountDown,lifePoints) {}

Shared::ENEMY_KIND Goatman::getEnemyKind() const {
	return Shared::ENEMY_KIND::GOATMAN;
}