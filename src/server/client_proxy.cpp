#include "client_proxy.h"
#include <string>
#define ID_ARGUMENT "id"
#define CODE "code"

using namespace server;

ClientProxy::ClientProxy(const unsigned int id,
				Shared::Socket* socket,
			    Shared::LockedQueue* eventQueue,
				bool& running):
		id(id), socket(socket), eventQueue(eventQueue),
		connected(true), running(running), output(*socket,connected) {
}

ClientProxy::~ClientProxy() {
	delete this->socket;
}

bool ClientProxy::operator==(const ClientProxy& other){
	return (this->id == other.id);
}

const YAML::Node* ClientProxy::appendId(const YAML::Node* aux){
	YAML::Node* newYaml = new YAML::Node();
	(*newYaml)[CODE] 		= *aux;
	(*newYaml)[ID_ARGUMENT] = this->id;
	return newYaml;
}

void ClientProxy::run(){
	this->output.start();
	Shared::Receiver receiver(*socket);
	while (this->running && this->connected){
		receiver.prepareSize();
		if (receiver.getSize() > 0 && receiver.read()){
			const YAML::Node* msgAux = Shared::Conversor::msgToYaml(receiver.msg(), receiver.getSize());
			YAML::Emitter a;
			a << *msgAux;
			const YAML::Node* msg = this->appendId(msgAux);
			delete msgAux;
			YAML::Emitter o;
			o << *msg;
			this->eventQueue->push(msg);
			receiver.clean();
		} else {
			this->connected = false;
		}
	}
	// It means that was the client who shut the connection
	// But the server is running.
	if (this->running){
		this->output.close();
		this->output.join();
		this->socket->s_shutdown(SHUT_RDWR);
		this->connected = false;
	}
}

unsigned int ClientProxy::getId() const{
	return this->id;
}

Shared::LockedQueue* ClientProxy::getOutputQueue() {
	return this->output.getQueue();
}

void ClientProxy::sendMessage(const char* message, size_t size){
	Shared::Sender sender(*socket);
	sender.doSend(message, size);
}

void ClientProxy::changeEventQueue(Shared::LockedQueue* eventQueue){
	this->eventQueue = eventQueue;
}

void ClientProxy::disconnect(){
	this->output.close();
	this->output.join();
	this->socket->s_shutdown(SHUT_RDWR);
	this->eventQueue->setFinished();
	this->connected = false;
}

bool ClientProxy::isConnected() const{
	return this->connected;
}
