////////////////
// Para correr ./server [port]
// Se termina la ejecución ingresando 'q' por stdin
///////////////
#include "server.h"
#include "../shared/exception/game_exception.h"

int main(int argc, char *argv[]) {
	try {
		server::Server server;
		if (server.isGood())
			server.run();
	} catch (Shared::GameException &e){
		return EXIT_FAILURE;
	} catch (std::exception &e){
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
