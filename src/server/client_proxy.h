#ifndef __CLIENT_PROXY_H__
#define __CLIENT_PROXY_H__
#include "../shared/thread/thread.h"
#include "../shared/communication/locked_queue.h"
#include "../shared/communication/receiver.h"
#include "../shared/communication/sender.h"
#include "message_handler.h"
#include "output_handler.h"
#include <vector>

namespace server{
	class ClientProxy: public Thread{
	private:
		const unsigned int id;
		Shared::Socket* socket;
		Shared::LockedQueue* eventQueue;
		bool connected;
		bool& running;
		OutputHandler output;
		const YAML::Node* appendId(const YAML::Node* aux);
	public:
		ClientProxy(const unsigned int id,
				Shared::Socket* socket,
			    Shared::LockedQueue* eventQueue,
				bool& running);
		~ClientProxy();
		bool operator==(const ClientProxy& other);
		void run() override;
		void sendMessage(const char* message, size_t size);
		unsigned int getId() const;
		Shared::LockedQueue* getOutputQueue();
		void changeEventQueue(Shared::LockedQueue* eventQueue);
		void disconnect();
		bool isConnected() const;
	};
}

#endif
