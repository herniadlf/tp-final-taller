#include "event_info.h"
#define ID_ARGUMENT "id"
#define CODE "code"

using namespace server;

EventInfo::EventInfo(const YAML::Node* yamlMessage) : size(0) {
	if (yamlMessage != NULL){
		YAML::Emitter emitter;
		emitter << *yamlMessage;
		if (emitter.size() > 0){
			this->message = new char[emitter.size()];
			memcpy(this->message,emitter.c_str(),emitter.size());
			this->size = emitter.size();
			this->clientId = (*yamlMessage)[ID_ARGUMENT].as<unsigned int>();
			YAML::Node codeNode = (*yamlMessage)[CODE];
			std::string code = codeNode.begin()->first.as<std::string>();
			this->letterCode = code[0];
		} else {
			this->message = NULL;
			this->letterCode = 0;
		}
	} else {
		this->size = 0;
		this->message = NULL;
		this->letterCode = 0;
	}
}

EventInfo::~EventInfo() {
	if (this->size > 0)
		delete[] this->message;
}

const char EventInfo::msgCode(){
	return this->letterCode;
}

const char* EventInfo::msg(){
	return this->message;
}

const size_t EventInfo::getSize() const{
	return this->size;
}

const unsigned int EventInfo::getClientId() const{
	return this->clientId;
}
