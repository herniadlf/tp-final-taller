#include "builder.h"
#include "yaml-cpp/yaml.h"

using namespace server;

Builder::Builder(const std::string& configPath) : ticksCounter(0) {
  	YAML::Node config = YAML::LoadFile(configPath);
  	YAML::Node coolDown 	= config["cool-down"];
  	this->coolDown        	= coolDown.as<unsigned int>();
}

Builder::Builder(const size_t coolDown) : ticksCounter(0), coolDown(coolDown) {}
Builder::~Builder() {}