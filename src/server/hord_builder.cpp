#include "hord_builder.h"
#include "enemy.h"
#include "abmonible.h"
#include "goatman.h"
#include "yaml-cpp/yaml.h"
#include <iostream>
#include <random>

using namespace server;

HordBuilder::HordBuilder(const std::vector<Shared::HordInfo*>& hordes,
                          IdCounter& idCounter,
                          const std::vector<Shared::Circuit*>& paths) :
  Builder(coolDown), idCounter(idCounter), hordsQuantity(hordes.size()), 
  hordsCounter(0), circuits(paths), hordes(hordes) {}

HordBuilder::~HordBuilder() {}

bool HordBuilder::newGameTick() {
  if (this->hordsCounter == this->hordsQuantity){
    return false;
  }
  this->ticksCounter++;
  return true;
}

bool HordBuilder::readyToCreate() {
  size_t coolDown = this->hordes[hordsCounter]->getCoolDown();
  if (this->ticksCounter == coolDown){
    this->ticksCounter = 0;
    return true;
  }
  return false;
}

std::vector<Enemy*> HordBuilder::build() {
  std::vector<Enemy*> enemies;
  Shared::HordInfo *actualHord = this->hordes[hordsCounter];
  size_t enemiesPerHord = actualHord->getQty();
  Shared::ENEMY_KIND enemyKind = actualHord->getEnemyKind();
  for (size_t i=0; i < enemiesPerHord; ++i){
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(0,this->circuits.size()-1);
    size_t randomIndex = distr(eng);
    Shared::Circuit* hordCircuit = this->circuits[randomIndex];
    Enemy * enemy = NULL;
    switch(enemyKind){
      case Shared::ENEMY_KIND::ABMONIBLE:
        enemy = new Abmonible(hordCircuit->get(),
                                idCounter.newId(),
                                actualHord->getSpeed(),
                                i*TICKS_BETWEEN_ENEMIES,
                                actualHord->getLifePoints());
        break;
      case Shared::ENEMY_KIND::GOATMAN:
        enemy = new Goatman(hordCircuit->get(),
                                idCounter.newId(),
                                actualHord->getSpeed(),
                                i*TICKS_BETWEEN_ENEMIES,
                                actualHord->getLifePoints());
        break;

    } 
    enemies.push_back(enemy);
  }
  this->hordsCounter++;
  return std::move(enemies);
}