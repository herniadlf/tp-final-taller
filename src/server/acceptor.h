#ifndef __ACCEPTOR_H__
#define __ACCEPTOR_H__
#include "../shared/thread/thread.h"
#include "message_handler.h"
#include "match_election.h"
#include <vector>

namespace server{
	class Acceptor: public Thread{
	private:
		Shared::Socket& listener;
		MessageHandler messageHandler;
		std::vector<Match*>& matches;
		bool& running;
		const std::vector<std::string>& mapFiles;
		std::vector<ClientProxy*> clients;
		unsigned int clientSeq;
		MatchElection matchElection;
		unsigned int nextId();
	public:
		Acceptor(Shared::Socket& listener,
					   std::vector<Match*>& matches,
					   bool& running,
					   const std::vector<std::string>& mapFiles);
		~Acceptor();
		void run() override;
		void disconnect();
	};
}
#endif
