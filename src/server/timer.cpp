#include "timer.h"
#include "yaml-cpp/yaml.h"
using namespace server;
using namespace std::chrono;

Timer::Timer(const size_t timeTick) : 
		init(system_clock::now()), timeTick(timeTick) {}

Timer::~Timer() {
	system_clock::time_point end = system_clock::now();
	double elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end-init).count();
	if (elapsed<this->timeTick){
		usleep((this->timeTick-elapsed)*1000); // because 1milisec = 1000microsec, and usleep takes microsec
	}
}
