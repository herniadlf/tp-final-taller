#ifndef __PROJECTILE_BUILDER_H__
#define __PROJECTILE_BUILDER_H__

#include "projectile.h"
#include "builder.h"
#include "hord_builder.h"
#include <map>

namespace server{
  class ProjectileBuilder: public Builder{
  protected:
    IdCounter& idCounter;
    Shared::Position position;
    Experience* experience;
    std::map<unsigned int, std::vector<Shared::Position>*> circuits;
    size_t projectileSpeed;
    size_t projectileMoveCountDown;
    size_t projectileHittingPoints;
    void readConfig(const std::string& configPath);
  public:
    ProjectileBuilder(const std::string& configPath,
                      IdCounter& idCounter, 
                      const Shared::Point& point,
                      Experience* experience);
    ProjectileBuilder(const std::string& configPath,IdCounter& idCounter);
    bool newGameTick() override;
    bool readyToCreate() override;
    Projectile* build(Movable& aimedEnemy);
    void upgrade();
  };
}

#endif
