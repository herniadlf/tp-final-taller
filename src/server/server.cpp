#include "server.h"
#include "yaml-cpp/yaml.h"
#include <string>
#define MAPS_PATH "maps/available-maps.yaml"
#define CONFIG_PATH "config/config.yaml"

using namespace server;

Server::Server() : running(true) {
	YAML::Node serverConfig = YAML::LoadFile(CONFIG_PATH);
	std::string port = serverConfig["port"].as<std::string>();
	int result = this->socket.bind_n_listen(port.c_str());
	if (result != 0){
		this->running=false; 
        throw Shared::GameException("No pudo conectarse el servidor a la red - Server.server.cpp");
	}
	YAML::Node info_maps = YAML::LoadFile(MAPS_PATH);
	for (auto it=info_maps.begin(); it!=info_maps.end(); ++it){
		this->mapFiles.push_back(it->as<std::string>());
	}
}
Server::~Server() {
	for (Match* match : this->matches)
		delete match;
}

bool Server::run(){
	Acceptor acceptor(this->socket,
							this->matches,
							this->running,
							this->mapFiles);
	acceptor.start();
	std::string input;
	std::cin >> input;
	while (input != "q"){
		input.clear();
		std::cin >> input;
	}
	this->running = false;
	acceptor.disconnect();
	this->socket.s_shutdown(SHUT_RDWR);
	acceptor.join();
	return true;
}

bool Server::isGood() const{
	return this->running;
}
