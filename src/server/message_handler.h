#ifndef __MESSAGE_HANDLER_H__
#define __MESSAGE_HANDLER_H__
#include <map>
#include "executor/game_executor.h"

namespace server{
	class MessageHandler{
	private:
		std::map<const char,GameExecutor*> handler;
	public:
		MessageHandler();
		~MessageHandler();
		void insert(const char key,GameExecutor* value);
		const GameExecutor& get(const char key);
	};
}
#endif
