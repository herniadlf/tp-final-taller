#include "match_election.h"
using namespace server;

MatchElection::MatchElection(std::vector<ClientProxy*>& clients,
								std::vector<Match*>& matches,
								const std::vector<std::string>& mapFiles,
								bool& running) : 
	clients(clients), matches(matches), mapFiles(mapFiles), running(running) {}

MatchElection::~MatchElection() {	
	while (this->eventQueue.getLength()>0){
		delete (this->eventQueue.pop());
	}
}

Shared::LockedQueue* MatchElection::getEventQueue() {
	return &this->eventQueue;
}

void MatchElection::run(){
	while (this->running){
		const YAML::Node* event = this->eventQueue.pop();
		EventInfo eventInfo(event);
		if (eventInfo.getSize() > 0){
			MatchExecutor* executor = NULL;
			switch(eventInfo.msgCode()){
				case ELECTION_CODE:
					executor = new ElectionExecutor(matches,clients,mapFiles,eventQueue);
					break;
				case FINISH_CODE:
					executor = new FinishExecutor(matches,clients,mapFiles,eventQueue);
					break;
				default:
					throw Shared::GameException("Ejecutor no encontrado - match_election.cpp");
			}
			Changes *changes = executor->doExecute(eventInfo.msg(), 
													eventInfo.getSize());
			this->notifyChanges(changes);
			delete executor;
		}
		delete event;
	}
}

void MatchElection::notifyChanges(Changes* changes){
	if (changes != NULL){
		for (const clientmessage& clientMessage: changes->get()){
			const YAML::Node* message = std::get<1>(clientMessage);
			size_t clientId = std::get<0>(clientMessage);
			Shared::LockedQueue* clientQueue = NULL;
			for (ClientProxy* client: this->clients){
				if (client->getId() == clientId)
					clientQueue = client->getOutputQueue();	
			} 
			if (clientQueue->hasWork())
				clientQueue->push(message);
			else
				delete message;
		}
		delete changes;
	}
}

void MatchElection::disconnect(){
	for (Match* match: this->matches){
		MATCH_STATE matchState = match->getState();
		if (matchState == MATCH_STATE::PLAYING){
			match->disconnect();
			match->join();
		} else if (matchState == MATCH_STATE::WAITING){
			match->disconnect();
		}
	}
	this->eventQueue.setFinished();
}
