#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

#include "movable.h"
#include "builder.h"
#include "hord_builder.h"
#include "experience.h"
#include <map>

namespace server{
  class Projectile: public Movable{
  protected:
    const unsigned int hittingPoints;
    bool hit;
    Experience* experience;
  public:
    Projectile(const std::vector<Shared::Position>& circuit,
                        unsigned int id,
                        const size_t speed,
                        const size_t moveCountDown,
                        unsigned int hittingPoints,
                        Experience* experience);
    ~Projectile();
    Shared::GAME_OBJECT getKind() override;
    virtual bool isActive() override;
    virtual bool hasHit();
    virtual const unsigned int getHittingPoints();
    virtual const unsigned int doHit();
    virtual void enemyKilled(const size_t enemyLifePoints);
  };
}

#endif
