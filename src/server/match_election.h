#ifndef __MATCH_ELECTION_H__
#define __MATCH_ELECTION_H__
#include <vector>
#include "../shared/communication/locked_queue.h"
#include "../shared/exception/game_exception.h"
#include "changes/changes.h"
#include "client_proxy.h"
#include "match.h"
#include "executor/game_executor.h"
#include "executor/election_executor.h"
#include "executor/finish_executor.h"

namespace server{
	class MatchElection: public Thread{
	private:
		std::vector<ClientProxy*>& clients;
		std::vector<Match*>& matches;
		const std::vector<std::string>& mapFiles;
		bool& running;
		Shared::LockedQueue eventQueue;
		size_t clientId;
		void notifyChanges(Changes* changes);
	public:
		MatchElection(std::vector<ClientProxy*>&clients,
						std::vector<Match*>& matches,
						const std::vector<std::string>& mapFiles,
						bool& running);
		~MatchElection();
		void run() override;
		Shared::LockedQueue* getEventQueue();
		void disconnect();
	};
}

#endif
