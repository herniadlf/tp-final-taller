#include "spell.h"
#include <iostream> // Just For Test - DELETEEE

using namespace server;

SpellProjectile::SpellProjectile(const std::vector<Shared::Position>& circuit,
                                        unsigned int id,
                                        const size_t speed,
                                        size_t moveCountDown,
                                        size_t hittingPoints,
                                        size_t duration) :
  Projectile(circuit, id, speed, moveCountDown,hittingPoints, NULL),
  duration(duration) {}

SpellProjectile::~SpellProjectile() {}

Shared::GAME_OBJECT SpellProjectile::getKind() {
  return Shared::GAME_OBJECT::SPELL;
}

int SpellProjectile::move(){
  duration--;
  return duration > 0 ? CIRCUIT_ON : CIRCUIT_FINISHED;
}

bool SpellProjectile::isActive(){
  return (duration > 0);
}

bool SpellProjectile::hasHit(){
  return hit;
}

const unsigned int SpellProjectile::doHit(){
  isMovableActive = false;
  return getHittingPoints();
}

void SpellProjectile::enemyKilled(const size_t enemyLifePoints){
  if (this->experience != NULL)
    this->experience->gainXp(enemyLifePoints);
}