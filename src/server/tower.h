#ifndef __TOWER_H__
#define __TOWER_H__
#include <vector>
#include "enemy.h"
#include "projectile_builder.h"
#include "experience.h"
#include "../shared/map/tower_info.h"
#define TOWER_CONFIG_PATH "config/tower.yaml"

namespace server{
  class Tower{
    IdCounter& idCounter;
    const unsigned int id;
    const unsigned int clientOwner;
    const Shared::Point point;
    Experience experience;
    ProjectileBuilder projectileBuilder;
    Movable* aimedEnemy;
    size_t shootingRatio;
  public:
    Tower(IdCounter& idCounter,
          const unsigned int clientOwner,
          const unsigned int x,
          const unsigned int y);
    ~Tower();
    const size_t getId() const;
    const unsigned int getX();
    const unsigned int getY();
    void getInfo(Shared::TowerInfo& towerInfo);
    bool newGameTick();
    bool readyToShoot(const std::vector<Enemy*>& enemies);
    Projectile* shoot();
    bool upgrade();
  };
}

#endif
