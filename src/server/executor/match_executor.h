#ifndef __MATCH_EXECUTOR_H__
#define __MATCH_EXECUTOR_H__

#include "../match.h"
#include "../client_proxy.h"
#include "../../shared/exception/game_exception.h"
#include "yaml-cpp/yaml.h"

namespace server{
	class MatchExecutor{
	protected:
		std::vector<Match*>& matches;
		std::vector<ClientProxy*>& clients;
		const std::vector<std::string>& mapFiles;
		Shared::LockedQueue& electionQueue;
	public:
		MatchExecutor(std::vector<Match*>& matches, 
						std::vector<ClientProxy*>& clients,
						const std::vector<std::string>& mapFiles,
						Shared::LockedQueue& electionQueue);
		virtual ~MatchExecutor();
		virtual Changes* doExecute(const char* message,
									const size_t msgSize) const = 0;
	};
}

#endif
