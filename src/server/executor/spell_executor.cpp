 #include "spell_executor.h"
#include <iostream> // RDELETEEE
#include <cstring> 

using namespace server;

SpellExecutor::SpellExecutor() {}

const void SpellExecutor::doExecute(Game& game, 
								Changes& changes,
								const char* message, 
								const size_t& msgSize) const{
	char *yamlMsg = new char[msgSize+1];
	memcpy(yamlMsg,message,msgSize);
	yamlMsg[msgSize] = '\0';
	YAML::Node command 	= YAML::Load(yamlMsg);
	YAML::Node code 	= command[CODE];
	std::string commandAux = code.begin()->first.as<std::string>();
	YAML::Node arguments = code[commandAux];
	unsigned int x = arguments[0].as<unsigned int>();
	unsigned int y = arguments[1].as<unsigned int>();
	unsigned int clientId = command[ID_ARGUMENT].as<unsigned int>(); 
	size_t newId = game.newSpell(x,y, clientId);
	bool result = newId > 0;
	Change* eventChange = new EventChange(Shared::GAME_OBJECT::SPELL,
										  newId,
										  x, 
										  y,
										  Event::NEW_SPELL,
										  result);
	//if its error, just notifies the client who requested it 
	changes.add(result ? 0 : clientId, eventChange);	
	delete[] yamlMsg;
}
