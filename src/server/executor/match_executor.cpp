#include "match_executor.h"
#include "../../shared/exception/game_exception.h"
using namespace server;

MatchExecutor::MatchExecutor(std::vector<Match*>& matches, 
								std::vector<ClientProxy*>& clients,
								const std::vector<std::string>& mapFiles,
								Shared::LockedQueue& electionQueue) :
	matches(matches), clients(clients), mapFiles(mapFiles), electionQueue(electionQueue) {}
MatchExecutor::~MatchExecutor () {}
