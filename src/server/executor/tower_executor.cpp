#include "tower_executor.h"
#include "../../shared/map/tower_info.h"
#include <cstring> 

using namespace server;

TowerExecutor::TowerExecutor() {}

const void TowerExecutor::doExecute(Game& game, 
										Changes& changes,
										const char* message, 
										const size_t& msgSize) const{
	char *yamlMsg = new char[msgSize+1];
	memcpy(yamlMsg,message,msgSize);
	yamlMsg[msgSize] = '\0';
	YAML::Node command 	= YAML::Load(yamlMsg);
	YAML::Node code 	= command[CODE];
	std::string commandAux = code.begin()->first.as<std::string>();
	if (commandAux == NEW_TOWER_COMMAND){
		YAML::Node arguments = code[commandAux];
		unsigned int x = arguments[0].as<unsigned int>();
		unsigned int y = arguments[1].as<unsigned int>();
		unsigned int clientId = command[ID_ARGUMENT].as<unsigned int>();
		size_t newId = game.newTower(x,y, clientId);
		bool result = newId > 0;
		Change* eventChange = new EventChange(Shared::GAME_OBJECT::TOWER,
											  newId,
											  x, 
											  y,
											  Event::NEW_TOWER,
											  result);
		//if its error, just notifies the client who requested it 
		changes.add(result ? 0 : clientId, eventChange);
	} else if (commandAux == INFO_TOWER_COMMAND || commandAux == UPGRADE_TOWER_COMMAND){
		YAML::Node arguments = code[commandAux];
		unsigned int x = arguments[0].as<unsigned int>();
		unsigned int y = arguments[1].as<unsigned int>();
		unsigned int clientId = command[ID_ARGUMENT].as<unsigned int>();
		if (commandAux == UPGRADE_TOWER_COMMAND)
			game.upgradeTower(x,y);
		Shared::TowerInfo towerInfo = game.getTowerInfo(x,y);
		Change* infoChange = new InfoChange(towerInfo);
		changes.add(clientId, infoChange);
	}
	delete[] yamlMsg;
}