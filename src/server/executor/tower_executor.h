#ifndef __TOWER_EXECUTOR_H__
#define __TOWER_EXECUTOR_H__

#include "game_executor.h"

#define TOWER_COMMAND_CODE 'T'
#define NEW_TOWER_COMMAND "Tnew-tower"
#define UPGRADE_TOWER_COMMAND "Tupgrade-tower"
#define INFO_TOWER_COMMAND "Tinfo-tower"

namespace server{
	class TowerExecutor: public GameExecutor{
	public:
		TowerExecutor();
		const void doExecute(Game& game, 
									Changes& changes,
									const char* message, 
									const size_t& msgSize) const override;
	};
}

#endif
