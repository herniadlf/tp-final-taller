#ifndef __GAME_EXECUTOR_H__
#define __GAME_EXECUTOR_H__

#include <map>
#include "../game.h"
#include "yaml-cpp/yaml.h"

#define ID_ARGUMENT "id"
#define CODE "code"
#define RETURN_COMMAND "return"

namespace server{
	class GameExecutor{
	public:
		GameExecutor();
		virtual ~GameExecutor();
		virtual const void doExecute(Game& game, 
											Changes& changes,
											const char* message, 
											const size_t& msg_size) const = 0;
	};
}

#endif
