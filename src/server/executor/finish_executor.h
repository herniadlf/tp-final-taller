#ifndef __FINISH_EXECUTOR_H__
#define __FINISH_EXECUTOR_H__

#include "match_executor.h"

#define FINISH_CODE 'F'
#define FINISH_WON_COMMAND "FWon"
#define FINISH_LOOSE_COMMAND "FWon"

namespace server{
	class FinishExecutor: public MatchExecutor{
	public:
		FinishExecutor(std::vector<Match*>& matches, 
							std::vector<ClientProxy*>& clients,
							const std::vector<std::string>& mapFiles,
							Shared::LockedQueue& electionQueue);
		Changes* doExecute(const char* message,
								const size_t msgSize) const override;
	};
}

#endif
