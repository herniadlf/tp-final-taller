#ifndef __ELECTION_EXECUTOR_H__
#define __ELECTION_EXECUTOR_H__

#include "match_executor.h"

#define ELECTION_CODE 'E'
#define ELECTION_CREATE_COMMAND "Ecreate"
#define ELECTION_JOIN_COMMAND "Ejoin"
#define ELECTION_CREATE_MAP_COMMAND "Ecreate-map"
#define ELECTION_LIST_MAPS_COMMAND "Elist-map"
#define ELECTION_LIST_MATCHS_COMMAND "Elist-match"
#define ELECTION_START_COMMAND "Estart"
#define ELECTION_CANCEL_CREATE_COMMAND "Ecancel-create"
#define ELECTION_CANCEL_JOIN_COMMAND "Ecancel-join"

namespace server{
	class ElectionExecutor: public MatchExecutor{
	public:
		ElectionExecutor(std::vector<Match*>& matches,
							std::vector<ClientProxy*>& clients,
							const std::vector<std::string>& mapFiles,
							Shared::LockedQueue& electionQueue);
		Changes* doExecute(const char* message,
								const size_t msgSize) const override;
	};
}

#endif
