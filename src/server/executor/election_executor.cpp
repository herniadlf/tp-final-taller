 #include "election_executor.h"
#include <iostream> // RDELETEEE
#include <cstring>

using namespace server;

ElectionExecutor::ElectionExecutor(std::vector<Match*>& matches,
									std::vector<ClientProxy*>& clients,
									const std::vector<std::string>& mapFiles,
									Shared::LockedQueue& electionQueue) :
	MatchExecutor(matches, clients, mapFiles, electionQueue) {}

Changes* ElectionExecutor::doExecute(const char* message,
										const size_t msgSize) const{
	char *yamlMsg = new char[msgSize+1];
	memcpy(yamlMsg,message,msgSize);
	yamlMsg[msgSize] = '\0';
	YAML::Node command = YAML::Load(yamlMsg);
	YAML::Node code = command[CODE];
  	std::string commandAux = code.begin()->first.as<std::string>();
	if (commandAux == ELECTION_CREATE_COMMAND){
		int mapNumber = code.begin()->second.as<int>();
		size_t clientId = command[ID_ARGUMENT].as<unsigned int>();
		size_t size = matches.size();
		std::string mapFile = mapFiles[mapNumber];
		Match *match = new Match(size+1, mapFile, electionQueue);
		matches.push_back(match);
		for (ClientProxy* client: clients){
			if (client->getId() == clientId){
				match->newClient(clientId, client->getOutputQueue());
				break;
			}
		}
	} else if (commandAux == ELECTION_JOIN_COMMAND){
		size_t clientId = command[ID_ARGUMENT].as<unsigned int>();
    	size_t matchNumber = code.begin()->second.as<unsigned int>();
		Match* match = NULL;
		for (Match* m: matches){
			if (m->getState() == MATCH_STATE::WAITING && m->getId() == matchNumber){
				match = m;
				break;
			}
		}
		if (match == NULL)
			throw Shared::GameException("No existen partidas. joinMatch.electionExecutor");
		for (ClientProxy* client: clients){
			if (client->getId() == clientId){
				match->newClient(clientId, client->getOutputQueue());
				break;
			}
		}
	} else if (commandAux == ELECTION_LIST_MAPS_COMMAND){
		size_t clientId = command[ID_ARGUMENT].as<unsigned int>();
		Changes *changes = new Changes(clientId);
		Change *change = new ListMapChange(mapFiles);
		changes->add(clientId, change);
		delete[] yamlMsg;
		return changes;
	} else if (commandAux == ELECTION_LIST_MATCHS_COMMAND){
		std::vector<idScenarioTuple> auxVector;
		for (Match* match: matches){
			if (match->getState() == MATCH_STATE::WAITING){
				size_t id 		= match->getId();
				int scenario 	= match->getScenario();
				idScenarioTuple tuple(id,scenario);
				auxVector.push_back(tuple);
			}
		}
		size_t clientId = command[ID_ARGUMENT].as<unsigned int>();
		Changes *changes = new Changes(clientId);
		Change* change = new ListMatchChange(auxVector);
		changes->add(clientId, change);
		delete[] yamlMsg;
		return changes;
	} else if (commandAux == ELECTION_START_COMMAND){
		size_t clientId = command[ID_ARGUMENT].as<unsigned int>();
		Changes* changes = NULL;
		for (Match* match: matches){
			if (match->getState() == MATCH_STATE::WAITING && match->getHost() == clientId){
				std::vector<unsigned int> ids = match->getIds();
				changes = new Changes(ids);
				Change* electionChange = new ElectionChange();
				changes->add(0, electionChange);
				for (size_t id: ids){
					for (ClientProxy* client: clients){
						if (client->getId() == id){
							client->changeEventQueue(&(match->getEventQueue()));
							break;
						}
					}
				}
				match->start();
				break;
			}
		}
		delete[] yamlMsg;
		return changes;
	} else if (commandAux == ELECTION_CANCEL_CREATE_COMMAND){
    	size_t matchNumber = code.begin()->second.as<unsigned int>();
    	Match* match = NULL;
		for (Match* m: matches){
			if (m->getState() == MATCH_STATE::WAITING && m->getId() == matchNumber){
				match = m;
				break;
			}
		}
		if (match == NULL)
			throw Shared::GameException("No existen partidas. joinMatch.electionExecutor");
		match->disconnect();
		Changes* changes = new Changes(match->getIds());
		Change *cancelChange = new CancelChange(matchNumber);
		changes->add(0,cancelChange);
		delete[] yamlMsg;
		return changes;
  } else if (commandAux == ELECTION_CANCEL_JOIN_COMMAND){
  		size_t clientId 	= command[ID_ARGUMENT].as<unsigned int>();
  		size_t matchId 		= code[commandAux].as<unsigned int>();
  		for (Match *match : matches){
  			if (match->getId() == matchId){
  				match->removeClient(clientId);
  			}
  		}
  		for (ClientProxy* client: clients){
  			if (client->getId() == clientId)
  				client->changeEventQueue(&electionQueue);
  		}
  }
	delete[] yamlMsg;
	return NULL;
}
