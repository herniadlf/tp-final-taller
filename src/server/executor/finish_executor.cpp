 #include "finish_executor.h"
#include <iostream> // RDELETEEE
#include <cstring> 

using namespace server;

FinishExecutor::FinishExecutor(std::vector<Match*>& matches, 
									std::vector<ClientProxy*>& clients,
									const std::vector<std::string>& mapFiles,
									Shared::LockedQueue& electionQueue) : 
	MatchExecutor(matches, clients, mapFiles, electionQueue) {}

Changes* FinishExecutor::doExecute(const char* message, 
										const size_t msgSize) const{
	char *yamlMsg = new char[msgSize+1];
	memcpy(yamlMsg,message,msgSize);
	yamlMsg[msgSize] = '\0';
	YAML::Node command = YAML::Load(yamlMsg);
	size_t matchId = command[ID_ARGUMENT].as<unsigned int>();
	for (Match* match: matches){
		if (match->getId() == matchId){
			std::vector<unsigned int> clientIds = match->getIds();
			for (size_t clientId: clientIds){
				for (ClientProxy* client: clients){
					if (client->getId() == clientId){
						client->changeEventQueue(&electionQueue);						
						break;
					}
				}
			}
			match->disconnect();
			match->join();
		}
	}
	delete[] yamlMsg;
	return NULL;
}