#ifndef __SPELL_EXECUTOR_H__
#define __SPELL_EXECUTOR_H__

#include "game_executor.h"

#define SPELL_COMMAND_CODE 'S'
#define NEW_SPELL_COMMAND "Snew-spell"

namespace server{
	class SpellExecutor: public GameExecutor{
	public:
		SpellExecutor();
		const void doExecute(Game& game, 
									Changes& changes,
									const char* message, 
									const size_t& msgSize) const override;
	};
}

#endif
