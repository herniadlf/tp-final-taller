#ifndef __ABMONIBLE_H__
#define __ABMONIBLE_H__

#include "enemy.h"

namespace server{
  class Abmonible: public Enemy{
  public:
    Abmonible(const std::vector<Shared::Position>& circuit,
                  unsigned int id,
                  const size_t speed,
                  const size_t moveCountDown,
                  const size_t lifePoints);
    Shared::ENEMY_KIND getEnemyKind() const override;
  };
}

#endif
