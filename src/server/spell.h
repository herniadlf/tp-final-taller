#ifndef __SPELL_PROJECTILE_H__
#define __SPELL_PROJECTILE_H__

#include "projectile.h"

namespace server{
    class SpellProjectile: public Projectile{
    private:
        size_t duration;
    public:
        SpellProjectile(const std::vector<Shared::Position>& circuit,
                        unsigned int id,
                        const size_t speed,
                        const size_t moveCountDown,
                        size_t hittingPoints,
                        size_t duration);
        ~SpellProjectile();
        Shared::GAME_OBJECT getKind() override;
        int move() override;
        bool isActive() override;
        bool hasHit() override;
        const unsigned int doHit() override;
        void enemyKilled(const size_t enemyLifePoints) override;
  };
}

#endif
