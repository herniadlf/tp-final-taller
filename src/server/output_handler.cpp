#include "output_handler.h"

using namespace server;

OutputHandler::OutputHandler(Shared::Socket& socket, const bool& clientConnected) : 
				socket(socket), clientConnected(clientConnected) {}
OutputHandler::~OutputHandler() {
	while (this->queue.getLength()>0){
		delete (this->queue.pop());
	}
}
void OutputHandler::run() {
	Shared::Sender sender(socket);
	while (this->queue.hasWork() && this->clientConnected){
		const YAML::Node* yamlMessage = this->queue.pop();
		if (yamlMessage != NULL){
			const char* msg = Shared::Conversor::yamlToMsg(yamlMessage);
			size_t size = Shared::Conversor::yamlGetSize(yamlMessage);
			sender.doSend(msg, size);
			delete yamlMessage;
			delete[] msg;
		}
 	}
}
Shared::LockedQueue* OutputHandler::getQueue() {
	return &queue;
}	

void OutputHandler::close() {
	this->queue.setFinished();
}
