#include "id_counter.h"

using namespace server;

IdCounter::IdCounter() : id(0) {}
IdCounter::~IdCounter() {}
size_t IdCounter::newId() {
	std::unique_lock<std::mutex> lock(idMutex);
	id++;
	return id;
}