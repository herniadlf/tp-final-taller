#ifndef __EVENT_INFO_H__
#define __EVENT_INFO_H__

#include "yaml-cpp/yaml.h"
#include <cstring>

namespace server{
	class EventInfo{
	private:
		char* message;
		char letterCode;
		size_t size;
		unsigned int clientId;
	public:
		EventInfo(const YAML::Node* yamlMessage);
		~EventInfo();
		const char msgCode();
		const char* msg();
		const size_t getSize() const;
		const unsigned int getClientId() const;
	};
}
#endif
