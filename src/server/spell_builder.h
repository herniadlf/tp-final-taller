#ifndef __SPELL_BUILDER_H__
#define __SPELL_BUILDER_H__

#include "projectile_builder.h"
#include "spell.h"
#define SPELL_CONFIG_PATH "config/spell.yaml"

namespace server{
	class SpellBuilder: public ProjectileBuilder{
		private:
			size_t spellDuration;
		public:
		  SpellBuilder(IdCounter& idCounter);
		  Projectile* build(unsigned int x, unsigned int y);
		};
}

#endif