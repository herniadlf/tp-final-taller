#include "acceptor.h"
#include "match_election.h"

using namespace server;

Acceptor::Acceptor(Shared::Socket& listener, 
			   std::vector<Match*>& matches,
			   bool& running,
			   const std::vector<std::string>& mapFiles) :
		listener(listener), matches(matches), 
		running(running), mapFiles(mapFiles), clientSeq(0),
		matchElection(clients,matches,mapFiles,running) {}



Acceptor::~Acceptor() {
	for (ClientProxy* handler : this->clients){
		handler->join();
		delete handler;
	}
}

void Acceptor::run(){
	MatchElection election(this->clients, this->matches, this->mapFiles, this->running);
	election.start();
	while (this->running){
		Shared::Socket* peer = new Shared::Socket();
		int result = this->listener.s_accept(peer);
		if (result == 0){
			Shared::LockedQueue* electionQueue = election.getEventQueue();
			ClientProxy* handler = new ClientProxy(this->nextId(),
															peer,
															electionQueue,
															this->running);
			this->clients.push_back(handler);
			handler->start();
		} else {
			delete peer;
		}
	}
	election.disconnect();
	election.join();
}

unsigned int Acceptor::nextId(){
	this->clientSeq++;
	return clientSeq;
}

void Acceptor::disconnect() {
	for (ClientProxy* client: this->clients){
		if (client->isConnected())
			client->disconnect();
	}
}