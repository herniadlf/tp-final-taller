#include "experience.h"
#include <iostream>

using namespace server;

Experience::Experience() : projectileCoolDown(0), projectileHittingPoints(0),
                            shootingRatio(0), points(0), level(1), upgradePoints(0)  {}
Experience::~Experience() {}

void Experience::setUpgradePoints() {
  this->upgradePoints = UPGRADE_OFFSET*this->level + projectileHittingPoints;
}

void Experience::upgradeCoolDown() {
  size_t upgradeAuxiliar = COOLDOWN_OFFSET*this->level;
  if (upgradeAuxiliar > this->projectileCoolDown)
    this->projectileCoolDown = 0;
  else
    this->projectileCoolDown -= upgradeAuxiliar;
}

void Experience::upgradeHittingPoints() {
  size_t upgradeAuxiliar = this->level*this->projectileHittingPoints/HITTING_POINTS_OFFSET;
  this->projectileHittingPoints += upgradeAuxiliar;
}

void Experience::upgradeShootingRatio() {
  size_t upgradeAuxiliar = this->shootingRatio/SHOOTING_RATIO_OFFSET;
  this->shootingRatio += upgradeAuxiliar;
}

void Experience::initialCoolDown(size_t projectileCoolDown){
  this->projectileCoolDown = projectileCoolDown;
}

void Experience::initialHittingPoints(size_t hittingPoints){
  this->projectileHittingPoints = hittingPoints;
  this->setUpgradePoints();
}

void Experience::initialShootingRatio(size_t shootingRatio){
  this->shootingRatio = shootingRatio;
}                          

bool Experience::upgrade() {
  if (this->points < this->upgradePoints)
    return false;
  this->upgradeCoolDown();
  this->upgradeHittingPoints();
  this->upgradeShootingRatio();
  this->level++;
  this->points -= this->upgradePoints;
  this->setUpgradePoints();
  return true;
}

void Experience::gainXp(const size_t gainedXp){
  this->points += gainedXp;
}

const size_t Experience::getLevel() const{
  return this->level;
}
const size_t Experience::getShootingRatio() const{
  return this->shootingRatio;
}
const size_t Experience::getHittingPoints() const{
  return this->projectileHittingPoints;
}
const size_t Experience::getCoolDown() const{
  return this->projectileCoolDown;
}
const size_t Experience::getPoints() const{
  return this->points;
}
const size_t Experience::getUpgradeXp() const{
  return this->upgradePoints;
}
