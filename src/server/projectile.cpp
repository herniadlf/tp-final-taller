#include "projectile.h"

using namespace server;

Projectile::Projectile(const std::vector<Shared::Position>& circuit,
                                        unsigned int id,
                                        const size_t speed,
                                        size_t moveCountDown,
                                        unsigned int hittingPoints,
                                        Experience* experience) :
  Movable(circuit, id, speed, moveCountDown), hittingPoints(hittingPoints), 
  hit(false), experience(experience) {}

Projectile::~Projectile() {}

Shared::GAME_OBJECT Projectile::getKind() {
  return Shared::GAME_OBJECT::PROJECTILE;
}

bool Projectile::isActive(){
  // Si no golpeo a un enemigo ni terminó su recorrido
  return (!hit && std::next(iterator,1) != circuit.end());
}

bool Projectile::hasHit(){
  return hit;
}

const unsigned int Projectile::getHittingPoints(){
  return hittingPoints;
}

const unsigned int Projectile::doHit(){
  hit = true;
  isMovableActive = false;
  this->experience->gainXp(this->hittingPoints);
  return hittingPoints;
}

void Projectile::enemyKilled(const size_t enemyLifePoints){
  this->experience->gainXp(enemyLifePoints);
}
