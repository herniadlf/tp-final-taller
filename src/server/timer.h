#ifndef __TIMER_H__
#define __TIMER_H__

#include <chrono>
#include <unistd.h>

namespace server{
	class Timer{
	private:
		std::chrono::system_clock::time_point init;
		const size_t timeTick;
	public:
		Timer(size_t timeTick);
		~Timer();
	};
}

#endif
