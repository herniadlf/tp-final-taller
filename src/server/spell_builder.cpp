#include "spell_builder.h"
#include "yaml-cpp/yaml.h"

using namespace server;

SpellBuilder::SpellBuilder(IdCounter& idCounter) :
          ProjectileBuilder(SPELL_CONFIG_PATH,idCounter) 
{
  YAML::Node config          = YAML::LoadFile(SPELL_CONFIG_PATH);
  YAML::Node duration        = config["duration"];
  this->spellDuration        = duration.as<unsigned int>();
}

Projectile* SpellBuilder::build(unsigned int x, unsigned int y) {
  Shared::Point point(x,y);
  Shared::Position position(point);
  size_t spellId = idCounter.newId();
  auto newCircuit = new std::vector<Shared::Position>();
  Shared::Position sourcePosition(position.getX(),position.getY());
  Shared::Position destPosition(position.getX(),position.getY());
  newCircuit->push_back(sourcePosition);
  newCircuit->push_back(destPosition);
  circuits[spellId] = newCircuit;
  Projectile * spell = new SpellProjectile(*(circuits[spellId]),
                                            spellId,
                                            this->projectileSpeed,
                                            this->projectileMoveCountDown,
                                            this->projectileHittingPoints,
                                            this->spellDuration);
  return spell;
}

