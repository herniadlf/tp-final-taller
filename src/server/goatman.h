#ifndef __GOATMAN_H__
#define __GOATMAN_H__

#include "enemy.h"

namespace server{
  class Goatman: public Enemy{
  public:
    Goatman(const std::vector<Shared::Position>& circuit,
                  unsigned int id,
                  const size_t speed,
                  const size_t moveCountDown,
                  const size_t lifePoints);
    Shared::ENEMY_KIND getEnemyKind() const override;
  };
}

#endif
