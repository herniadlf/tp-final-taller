#ifndef __OUTPUT_HANDLER_H__
#define __OUTPUT_HANDLER_H__
#include "../shared/thread/thread.h"
#include "../shared/socket/socket.h"
#include "../shared/communication/locked_queue.h"
#include "../shared/communication/sender.h"

namespace server{
	class OutputHandler: public Thread{
	private:
		Shared::Socket& socket;
		const bool& clientConnected;
		Shared::LockedQueue queue;
	public:
		OutputHandler(Shared::Socket& socket, const bool& clientConnected);
		~OutputHandler();
		void run() override;
		Shared::LockedQueue* getQueue();
		void close();
	};
}

#endif
