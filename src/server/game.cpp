#include "game.h"
#include <stdlib.h>
#include <iostream> // Just For Test - DELETEEE

#define CIRCUIT_FINISHED -1
#define CIRCUIT_ON 1
#define NOT_YET 0
using namespace server;

Game::Game(unsigned int id, const std::string& mapFile) :
								id(id),
								map(mapFile),
								idCounter(),
								hordBuilder(map.getHordes(),
											idCounter,
											map.getPaths()),
								spellBuilder(idCounter) {}

Game::~Game() {
	for (const Movable* object: this->objects)
		delete object;
	for (const Tower* tower: this->towers)
		delete tower;
}

int Game::getScenario() const {
	return this->map.getScenario();
}

bool Game::doMoveObjects(Changes& changes) {
	bool gameRunning = true;
	for (Movable* object: this->objects){
		int moveResult = object->move();
		if(moveResult == CIRCUIT_ON){
			Change* moveChange = NULL;
			if(object->getKind() == Shared::GAME_OBJECT::ENEMY)
				moveChange = new MovementChange(object->getKind(),
												((Enemy*)object)->getEnemyKind(),
												object->getId(),
												object->getX(),
												object->getY(),
												Event::MOVEMENT,
												true);
			else
				moveChange = new MovementChange(object->getKind(),
												object->getId(),
												object->getX(),
												object->getY(),
												Event::MOVEMENT,
												true);

			changes.add(0, moveChange); // 0 means broadcast change
		} else if (moveResult == CIRCUIT_FINISHED &&
					object->getKind() == Shared::GAME_OBJECT::ENEMY){
			gameRunning = false;
		}
	}
	return gameRunning;
}


bool Game::doCheckHordes(){
	bool gameRunning = true;
	if (hordBuilder.newGameTick()){
		if (hordBuilder.readyToCreate()){
			std::vector<Enemy*> newHord = hordBuilder.build();
			this->objects.insert(std::end(this->objects),
								std::begin(newHord),
								std::end(newHord));
			this->enemies.insert(std::end(this->enemies),
								std::begin(newHord),
								std::end(newHord));
		}
	} else {
		gameRunning = (this->enemies.size() > 0);
	}
	return gameRunning;
}

void Game::doCheckProjectils(){
	for (Tower* tower: this->towers){
		tower->newGameTick();
		if (tower->readyToShoot(this->enemies)){
			Projectile* newProjectile = tower->shoot();
			if(newProjectile){
				this->objects.push_back(newProjectile);
				this->projectiles.push_back(newProjectile);
			}
		}
	}
	spellBuilder.newGameTick();
}

void Game::detectCollisions(Changes& changes) {
	for (std::vector<Projectile*>::iterator it=this->projectiles.begin();
											it!=this->projectiles.end(); ){
		Projectile *projectile = *it;
		unsigned int xProjectile = projectile->getX();
		size_t i = 0;
		while (i < enemies.size() && !projectile->hasHit()){
				Enemy* enemy = enemies[i];
				unsigned int yProjectile = projectile->getY();
				unsigned int xEnemy = enemy->getX();
				if (abs(xProjectile - xEnemy) < projectile->getRatio() + enemy->getRatio()){
					unsigned int yEnemy = enemy->getY();
					if(abs(yProjectile - yEnemy) < projectile->getRatio() + enemy->getRatio()){
						enemy->getHit(projectile->doHit());
						if(!enemy->isActive()){
							// El puntero se borra de enemies, pero sigue vivo en objects.
							projectile->enemyKilled(enemy->getInitialLifePoints());
							enemies.erase(enemies.begin()+i);
							i--;
						}
					}
				}
				i++;
		}
		if (projectile->isActive())
			++it;
		else
			projectiles.erase(it);
	}
}

void Game::objectCleaner(Changes& changes) {
	for (std::vector<Movable*>::iterator it=this->objects.begin();
											it!=this->objects.end(); ){
		Movable* object = *it;
		if (!object->isActive()){
			Change* cleanChange = NULL;
			if(object->getKind() == Shared::GAME_OBJECT::ENEMY)
				cleanChange = new MovementChange(object->getKind(),
												((Enemy*)object)->getEnemyKind(),
												object->getId(),
												object->getX(),
												object->getY(),
												Event::CLEAN,
												true);
			else
				cleanChange = new MovementChange(object->getKind(),
												object->getId(),
												object->getX(),
												object->getY(),
												Event::CLEAN,
												true);


			changes.add(0,cleanChange);
			delete object;
			this->objects.erase(it);
		} else {
			++it;
		}
	}
}

size_t Game::newId(){
	return idCounter.newId();
}

size_t Game::newTower(unsigned int x, unsigned int y, unsigned int client){
	Shared::Point point(x,y);
	bool result = this->map.canBuildTower(point);
	if (result){
		this->map.insertNewTowerOn(point);
		Tower *newTower = new Tower(idCounter,client,x,y);
		this->towers.push_back(newTower);
		return newTower->getId();
	}
	return 0;
}

size_t Game::upgradeTower(unsigned int x, unsigned int y) {
	size_t towerId = 0;
	for (Tower* tower : this->towers){
		if (tower->getX() == x && tower->getY() == y){
			bool result = tower->upgrade();
			towerId = result? tower->getId() : 0;
			break;
		}
	}
	return towerId;
}

Shared::TowerInfo Game::getTowerInfo(unsigned int x, unsigned int y){
	Shared::TowerInfo towerInfo;
	for (Tower* tower : this->towers){
		if (tower->getX() == x && tower->getY() == y){
			tower->getInfo(towerInfo);
			break;
		}
	}
	return towerInfo;
}

size_t Game::newSpell(unsigned int x, unsigned int y, unsigned int client){
	if (spellBuilder.readyToCreate()){
		Shared::Point point(x,y);
		this->map.insertProjectile(point);
		Projectile* newSpell = spellBuilder.build(x,y);
		this->objects.push_back(newSpell);
		this->projectiles.push_back(newSpell);
		return newSpell->getId();
	}
	return 0;
}

const std::string Game::getMapForClient() {
	return this->map.getMapForClient();
}
