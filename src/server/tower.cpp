#include "tower.h"
#include <utility>
#include <stdlib.h>
#include <iostream>
#include "yaml-cpp/yaml.h"

#define RATE_POS_COORD 40.0
#define RATE_COORD_POS 1.0/RATE_POS_COORD

using namespace server;

Tower::Tower(IdCounter& idCounter,
              const unsigned int clientOwner,
              const unsigned int x,
              const unsigned int y) :
  idCounter(idCounter), id(idCounter.newId()), clientOwner(clientOwner),
  point(x,y), experience(), projectileBuilder(TOWER_CONFIG_PATH,idCounter,point,&experience)
{
  YAML::Node config         = YAML::LoadFile(TOWER_CONFIG_PATH);
  YAML::Node shootingRatio  = config["shooting-ratio"];
  this->shootingRatio       = shootingRatio.as<unsigned int>();
  this->experience.initialShootingRatio(this->shootingRatio);
}

Tower::~Tower(){}

const size_t Tower::getId() const{
  return this->id;
}

const unsigned int Tower::getX(){
  return point.getX();
}

const unsigned int Tower::getY(){
  return point.getY();
}

void Tower::getInfo(Shared::TowerInfo& towerInfo){
  towerInfo.addLevel(this->experience.getLevel());
  towerInfo.addShootingRatio(this->experience.getShootingRatio());
  towerInfo.addHittingPoints(this->experience.getHittingPoints());
  towerInfo.addCoolDown(this->experience.getCoolDown());
  towerInfo.addActualXp(this->experience.getPoints());
  towerInfo.addUpgradeXp(this->experience.getUpgradeXp());
  towerInfo.setValid();
}

bool Tower::newGameTick(){
  this->projectileBuilder.newGameTick();
  return true;
}

bool Tower::readyToShoot(const std::vector<Enemy*>& enemies) {
  if (this->projectileBuilder.readyToCreate()){
    // Chequeo por radio si hay enemigos a los que disparar.
    // Asigno a aimedEnemy y devuelvo true.
    for(Enemy* enemy : enemies){
      if (enemy->isMoving()){
        unsigned int xEnemy = enemy->getX()*RATE_COORD_POS;
        unsigned int yEnemy = enemy->getY()*RATE_COORD_POS;
        Shared::Point enemyPoint(xEnemy,yEnemy);
        if((abs(enemyPoint.getX()-getX()) < this->shootingRatio)
                                    &&
            (abs(enemyPoint.getY()-getY()) < this->shootingRatio)){
              aimedEnemy = enemy;
              return true;
        }
      }
    }
  }
  return false;
}

Projectile* Tower::shoot(){
  Projectile* newProjectile = this->projectileBuilder.build(*aimedEnemy);
  return newProjectile;
}

bool Tower::upgrade() {
  bool result = this->experience.upgrade();
  if (result){
    this->projectileBuilder.upgrade();
    this->shootingRatio = this->experience.getShootingRatio();
  }
  return result;
}
