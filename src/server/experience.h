#ifndef __EXPERIENCE_H__
#define __EXPERIENCE_H__

#define UPGRADE_OFFSET 5
#define COOLDOWN_OFFSET 2
#define HITTING_POINTS_OFFSET 2
#define SHOOTING_RATIO_OFFSET 2

#include <cstring>

namespace server{
  class Experience{
  private:
    size_t projectileCoolDown;
    size_t projectileHittingPoints;
    size_t shootingRatio;
    size_t points;
    size_t level;
    size_t upgradePoints;
    void setUpgradePoints();
    void upgradeCoolDown();
    void upgradeHittingPoints();
    void upgradeShootingRatio();
  public:
    Experience();
    ~Experience();
    void initialCoolDown(size_t projectileCoolDown);
    void initialHittingPoints(size_t hittingPoints);
    void initialShootingRatio(size_t shootingRatio);
    void gainXp(const size_t gainedXp);
    bool upgrade();
    const size_t getLevel() const;
    const size_t getShootingRatio() const;
    const size_t getHittingPoints() const;
    const size_t getCoolDown() const;
    const size_t getPoints() const;
    const size_t getUpgradeXp() const;
  };
}
#endif
