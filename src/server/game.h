#ifndef __GAME_H__
#define __GAME_H__

#include <vector>
#include <queue>
#include <utility>
#include "../shared/communication/locked_queue.h" // DELETE
#include "../shared/map/map.h"
#include "changes/changes.h"
#include "changes/event_change.h"
#include "changes/election_change.h"
#include "changes/movement_change.h"
#include "changes/map_change.h"
#include "changes/finish_change.h"
#include "changes/info_change.h"
#include "changes/list_map_change.h"
#include "changes/list_match_change.h"
#include "changes/cancel_change.h"
#include "hord_builder.h"
#include "tower.h"
#include "spell_builder.h"
#include "id_counter.h"

namespace server{
	class Game{
	private:
		unsigned int id;
		Shared::Map map;
		std::vector<Movable*> objects;
		std::vector<Tower*> towers;
		std::vector<Enemy*> enemies;
		std::vector<Projectile*> projectiles;
		std::vector<std::queue<std::pair<unsigned int, unsigned int>>*> circuits;
		IdCounter idCounter;
		HordBuilder hordBuilder;
		SpellBuilder spellBuilder;
	public:
		Game(unsigned int id, const std::string& mapFile);
		~Game();
		int getScenario() const;
		bool doMoveObjects(Changes& changes);
		bool doCheckHordes();
		void doCheckProjectils();
		void detectCollisions(Changes& changes);
		void objectCleaner(Changes& changes);
		size_t newId();
		size_t newTower(unsigned int x, unsigned int y, unsigned int client);
		size_t upgradeTower(unsigned int x, unsigned int y);
		Shared::TowerInfo getTowerInfo(unsigned int x, unsigned int y);
		size_t newSpell(unsigned int x, unsigned int y, unsigned int client);
		const std::string getMapForClient();
	};
}
#endif
	