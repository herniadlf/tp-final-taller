#include "match.h"
#include "timer.h"
#define ID_ARGUMENT "id"
using namespace server;

Match::Match(size_t id, const std::string& mapFile,
				Shared::LockedQueue& electionQueue) :
	id(id), game(id,mapFile), state(MATCH_STATE::WAITING), electionQueue(electionQueue)
 {
	GameExecutor *tower 	= new TowerExecutor();
	this->messageHandler.insert(TOWER_COMMAND_CODE, tower);
	GameExecutor *spell 	= new SpellExecutor();
	this->messageHandler.insert(SPELL_COMMAND_CODE, spell);

	YAML::Node config 	= YAML::LoadFile(TIMER_CONFIG_PATH);
	YAML::Node timeTick = config["time-tick"];
	this->timeTick 		= timeTick.as<unsigned int>();
}

Match::~Match() {
	while (this->eventQueue.getLength()>0){
		delete (this->eventQueue.pop());
	}
}

MATCH_STATE Match::getState() const{
	return this->state;
}

int Match::getScenario() const{
	return this->game.getScenario();
}

size_t Match::getId() const{
	return this->id;
}

void Match::processEvents(Changes& changes){
	size_t length = this->eventQueue.getLength();
	for (size_t i=0; i<length; ++i){
		const YAML::Node* event = this->eventQueue.pop();
		EventInfo eventInfo(event);
		if (eventInfo.getSize() > 0){
			const GameExecutor& executor = this->messageHandler.get(eventInfo.msgCode());
			executor.doExecute(this->game, changes, eventInfo.msg(),eventInfo.getSize());
		}
		delete event;
	}
}

void Match::moveObjects(Changes& changes){
	this->state = this->game.doMoveObjects(changes) ? this->state : MATCH_STATE::LOOSE;
}

void Match::checkBuilders(){
	if (this->state == MATCH_STATE::PLAYING){
		this->state = this->game.doCheckHordes() ? this->state : MATCH_STATE::WON;
		this->game.doCheckProjectils();
	}
}

void Match::collisionsDetector(Changes& changes){
	this->game.detectCollisions(changes);
}

void Match::objectCleaner(Changes& changes){
	this->game.objectCleaner(changes);
}

void Match::notifyChanges(Changes& changes){
	for (const clientmessage& clientMessage: changes.get()){
		const YAML::Node* message = std::get<1>(clientMessage);
		Shared::LockedQueue* clientQueue = this->clients[std::get<0>(clientMessage)];
		if (clientQueue->hasWork())
			clientQueue->push(message);
		else
			delete message;
	}

}

const std::vector<unsigned int> Match::getIds(){
	std::map<unsigned int, Shared::LockedQueue*>::iterator first = this->clients.begin();
	std::map<unsigned int, Shared::LockedQueue*>::iterator last = this->clients.end();
	std::vector<unsigned int> ids;
	for (auto it = first; it != last; ++it)
		ids.push_back(it->first);
	return ids;
}

void Match::notifyEnd(){
	if (this->state != MATCH_STATE::ABORTED){
		Changes changes(this->getIds());
		Event event = this->state == MATCH_STATE::WON ? Event::WON : Event::LOOSE;
		Change* lastChangeForClient = new FinishChange(this->id, event);
		changes.add(0,lastChangeForClient);
		this->notifyChanges(changes);
		FinishChange* lastChangeForServer = new FinishChange(this->id, event);
		const YAML::Node* finishMessage = lastChangeForServer->toInternalYaml();
		electionQueue.push(finishMessage);
	}
}

void Match::run(){
	this->state = MATCH_STATE::PLAYING;
	while (this->state == MATCH_STATE::PLAYING){
		Changes changes(this->getIds());
		Timer timer(this->timeTick);
		this->processEvents(changes);
		this->moveObjects(changes);
		this->checkBuilders();
		if (this->state == MATCH_STATE::PLAYING){
			this->collisionsDetector(changes);
			this->objectCleaner(changes);
			this->notifyChanges(changes);
		}
 	}
 	this->notifyEnd();
}

Shared::LockedQueue& Match::getEventQueue() {
	return this->eventQueue;
}

void Match::doBroadcast(const YAML::Node* message){
	std::map<unsigned int, Shared::LockedQueue*>::iterator first = this->clients.begin();
	std::map<unsigned int, Shared::LockedQueue*>::iterator last = this->clients.end();
	for (auto it = first; it != last; ++it)
		it->second->push(message);
}

void Match::newClient(unsigned int id, Shared::LockedQueue* clientQueue){
	std::pair<unsigned int, Shared::LockedQueue*> client(id,clientQueue);
	this->clients.insert(client);
	Changes changes(id);
	std::string map = game.getMapForClient();
	Change* mapChange = new MapChange(map,this->id);
	changes.add(id, mapChange);
	this->notifyChanges(changes);
}

void Match::removeClient(unsigned int id){
	this->clients.erase(id);
}

void Match::disconnect() {
	this->state = MATCH_STATE::ABORTED;
	this->eventQueue.setFinished();
}

const size_t Match::getHost(){
	return this->clients.begin()->first;
}
