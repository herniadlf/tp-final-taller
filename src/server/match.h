#ifndef __MATCH_H__
#define __MATCH_H__

#include <vector>
#include <map>
#include "../shared/communication/locked_queue.h"
#include "../shared/thread/thread.h"
#include "executor/game_executor.h"
#include "executor/tower_executor.h"
#include "executor/spell_executor.h"
#include "message_handler.h"
#include "event_info.h"

#define TIMER_CONFIG_PATH "config/timer.yaml"

enum class MATCH_STATE {WAITING, PLAYING, WON, LOOSE, ABORTED};

namespace server{
	class Match: public Thread{
	private:
		size_t id;
		Game game;
		MATCH_STATE state;
		Shared::LockedQueue& electionQueue;
		Shared::LockedQueue eventQueue;
		std::map<unsigned int, Shared::LockedQueue*> clients;
		MessageHandler messageHandler;
		size_t timeTick;
		void processEvents(Changes& changes);
		void moveObjects(Changes& changes);
		void checkBuilders();
		void collisionsDetector(Changes& changes);
		void objectCleaner(Changes& changes);
		void doBroadcast(const YAML::Node* message);
		void notifyChanges(Changes& changes);
		void notifyEnd();
	public:
		Match(size_t id, const std::string& mapFile,
				Shared::LockedQueue& electionQueue);
		~Match();
		void run() override;
		MATCH_STATE getState() const;
		int getScenario() const;
		size_t getId() const;
		Shared::LockedQueue& getEventQueue();
		void newClient(unsigned int id, Shared::LockedQueue* clientQueue);
		void removeClient(unsigned int id);
		const std::vector<unsigned int> getIds();
		const size_t getHost();
		void disconnect();
	};
}
#endif
