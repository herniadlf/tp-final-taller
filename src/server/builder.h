#ifndef __BUILDER_H__
#define __BUILDER_H__
#include <string>

namespace server{
	class Builder{
	protected:
	  size_t ticksCounter;
	  size_t coolDown;
	public:
	  Builder(const std::string& configPath);
	  Builder(const size_t coolDown);
	  ~Builder();
	  virtual bool newGameTick() = 0 ; 
	  virtual bool readyToCreate() = 0;
	};
}
#endif
