#ifndef __SERVER_H__
#define __SERVER_H__

#include "../shared/socket/socket.h"
#include "../shared/exception/game_exception.h"
#include "client_proxy.h"
#include "acceptor.h"
#include <string>
#include <vector>

namespace server{
	class Server{
	private:
		Shared::Socket socket;
		bool running;
		std::vector<Match*> matches;
		std::vector<std::string> mapFiles;
	public:
		Server();
		~Server();
		bool run();
		bool isGood() const;
	};
}
#endif
