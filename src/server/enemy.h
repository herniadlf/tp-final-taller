#ifndef __ENEMY_H__
#define __ENEMY_H__

#include "movable.h"

namespace server{
  class Enemy: public Movable{
  private:
    unsigned int initialLifePoints;
    unsigned int actualLifePoints;
  public:
    Enemy(const std::vector<Shared::Position>& circuit,
                  unsigned int id,
                  const size_t speed,
                  const size_t moveCountDown,
                  const size_t lifePoints);
    virtual ~Enemy();
    Shared::GAME_OBJECT getKind() override;
    virtual Shared::ENEMY_KIND getEnemyKind() const = 0;
    bool isActive() override;
    void getHit(unsigned int hittingPoints);
    unsigned int getInitialLifePoints() const;
  };
}

#endif
