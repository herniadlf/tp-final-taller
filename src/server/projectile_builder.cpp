#include "projectile_builder.h"
#include "yaml-cpp/yaml.h"

using namespace server;

// Constructor with point => for tower projectils
ProjectileBuilder::ProjectileBuilder(const std::string& configPath,
                                      IdCounter& idCounter, 
                                      const Shared::Point& point,
                                      Experience* experience) :
          Builder(configPath), idCounter(idCounter), position(point),
          experience(experience) 
{
  this->readConfig(configPath);
  this->experience->initialCoolDown(this->coolDown);
  this->experience->initialHittingPoints(this->projectileHittingPoints);
}

// Constructor w/out point => for spells projectils
ProjectileBuilder::ProjectileBuilder(const std::string& configPath,
                                      IdCounter& idCounter) :
          Builder(configPath), idCounter(idCounter), position(),
          experience(NULL) 
{
  this->readConfig(configPath);
}

void ProjectileBuilder::readConfig(const std::string& configPath){
  YAML::Node config               = YAML::LoadFile(configPath);
  YAML::Node speed                = config["speed"];
  this->projectileSpeed           = speed.as<unsigned int>();
  YAML::Node moveCountDown        = config["move-countdown"];
  this->projectileMoveCountDown   = moveCountDown.as<unsigned int>();
  YAML::Node hittingPoints        = config["hitting-points"];
  this->projectileHittingPoints   = hittingPoints.as<unsigned int>();
}

bool ProjectileBuilder::newGameTick() {
  this->ticksCounter++;
  return true;
}
bool ProjectileBuilder::readyToCreate() {
  if (this->ticksCounter >= this->coolDown){
    this->ticksCounter = 0;
    return true;
  }
  return false;
}

Projectile* ProjectileBuilder::build(Movable& aimedEnemy) {
  size_t projectileId = idCounter.newId();
  // Se debería calcular el circuito para el proyectil
  // Agregarle las coord de la torre al ProjectileBuilder
  // circuits.push_back(std::make_pair(0, 0));
  auto newCircuit = new std::vector<Shared::Position>();
  // circuits.push_back(newCircuit);
  Shared::Position sourcePosition(position.getX(),position.getY());
  Shared::Position destPosition(aimedEnemy.getX(), aimedEnemy.getY());
  newCircuit->push_back(sourcePosition);
  newCircuit->push_back(destPosition);
  circuits[projectileId] = newCircuit;
  // Se puede evitar el vector de circuitos? Solo crearla y pasarsela al proj?
  Projectile * projectile = new Projectile(*(circuits[projectileId]),
                                            projectileId,
                                            this->projectileSpeed,
                                            this->projectileMoveCountDown,
                                            this->projectileHittingPoints,
                                            this->experience);
  return projectile;
}

void ProjectileBuilder::upgrade() {
  this->coolDown = this->experience->getCoolDown();
  this->projectileHittingPoints = this->experience->getHittingPoints(); 
}
