#include "movable.h"
#include <iostream>
#define RATIO 6

using namespace server;
// La idea es que si cada 10 segundos hay 1 paso, cuanto más rapido es el enemigo
// Menos tiempo tiene que esperar para dar un paso.
Movable::Movable(const std::vector<Shared::Position>& circuit,
                                    unsigned int id,
                                    const size_t speed,
                                    size_t moveCountDown):
  circuit(circuit), id(id), speed(MOVEMENT_VERSOR+1-speed),
  moveCountDown(moveCountDown),
  step_counter(0),
  tickCounter(0),
  ratio(RATIO),
  iterator(this->circuit.begin())
{
  this->actualPosition = new Shared::Position(iterator->getX() + RATE_POS_COORD/2, iterator->getY() + RATE_POS_COORD/2);
}

Movable::~Movable(){
  delete this->actualPosition;
}

unsigned int Movable::getX(){
  return actualPosition->getX();
}

unsigned int Movable::getY(){
  return actualPosition->getY();
}

unsigned int Movable::getId(){
  return id;
}

bool Movable::isMoving() {
  return (isActive() && moveCountDown == 0);
}

size_t Movable::getRatio(){
  return ratio;
}

bool Movable::canMove() {
  this->tickCounter++;
  if (this->moveCountDown > 0){
    this->moveCountDown--;
    return false;
  }
  return (this->tickCounter >= this->speed);
}

int Movable::move(){
  if (!canMove())
    return NOT_YET;
  if (iterator != circuit.end()){
    auto iterator2 = std::next(iterator,1);
    if (iterator2 != circuit.end()){
      this->tickCounter=0;
      Shared::Position nextPosition(iterator2->getX() + RATE_POS_COORD/2, iterator2->getY() + RATE_POS_COORD/2);
      int diffX = nextPosition.getX() - actualPosition->getX();
      int diffY = nextPosition.getY() - actualPosition->getY();
      int dx = diffX == 0 ? 0 : (diffX < 0) ? -1 : 1;
      int dy = diffY == 0 ? 0 : (diffY < 0) ? -1 : 1;

      int actualPositionX = actualPosition->getX();
      int actualPositionY = actualPosition->getY();
      delete actualPosition;
      actualPosition = new Shared::Position(actualPositionX + dx*STEP_SIZE,
                                      actualPositionY + dy*STEP_SIZE);
      if((dx == 0) && (dy == 0)){
        iterator++;
      }
    } else {
      return CIRCUIT_FINISHED;     
    }
    step_counter++;
    if((step_counter >= RATE_POS_COORD)){
      step_counter = 0;
    }
    return CIRCUIT_ON;
  } 
    return CIRCUIT_FINISHED;    
}
