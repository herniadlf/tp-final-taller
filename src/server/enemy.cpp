#include "enemy.h"

using namespace server;
Enemy::Enemy(const std::vector<Shared::Position>& circuit,
                                        unsigned int id,
                                        const size_t speed,
                                        const size_t moveCountDown,
                                        const size_t lifePoints) :
  Movable(circuit, id, speed, moveCountDown),
  initialLifePoints(lifePoints), actualLifePoints(initialLifePoints) {}

Enemy::~Enemy() {}

Shared::GAME_OBJECT Enemy::getKind() {
  return Shared::GAME_OBJECT::ENEMY;
}

bool Enemy::isActive(){
  return (actualLifePoints > 0);
}

void Enemy::getHit(unsigned int hittingPoints){
  if(hittingPoints >= actualLifePoints){
    actualLifePoints = 0;
    isMovableActive = false;
  }else{
    actualLifePoints -= hittingPoints;
  }
}

unsigned int Enemy::getInitialLifePoints() const{
  return this->initialLifePoints;
}
