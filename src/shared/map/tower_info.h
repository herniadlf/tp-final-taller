#ifndef __TOWER_INFO_H__
#define __TOWER_INFO_H__

#include <vector>
#include "yaml-cpp/yaml.h"

namespace Shared {

	class TowerInfo{
	private:
		YAML::Node* info;
		void fillEmptyInfo();
	public:
		TowerInfo();
		~TowerInfo();
		void addLevel(size_t info);
		void addShootingRatio(size_t info);
		void addHittingPoints(size_t info);
		void addCoolDown(size_t info);
		void addActualXp(size_t info);
		void addUpgradeXp(size_t info);
		void addValidation(size_t info);
		void setValid();
		bool isValid();
		YAML::Node* getInfo();
		static const std::string LEVEL_LABEL;
		static const std::string SHOOTING_RATIO_LABEL;
		static const std::string HITTING_POINTS_LABEL;
		static const std::string COOLDOWN_LABEL;
		static const std::string ACTUAL_XP_LABEL;
		static const std::string UPGRADE_XP_LABEL;
		static const std::string VALIDATION_LABEL;
		bool validInfo;
	};
}

#endif
