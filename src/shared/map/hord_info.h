#ifndef __HORD_INFO_H__
#define __HORD_INFO_H__
#include <cstring>
#include <string>
#include "game_object.h"
#define ABMONIBLE_CONFIG_PATH "config/abmonible.yaml"
#define GOATMAN_CONFIG_PATH "config/goatman.yaml"

namespace Shared{
  class HordInfo{
  private:
    ENEMY_KIND enemyKind;
    size_t qty;
    size_t coolDown;
    size_t speed;
    size_t lifePoints;
  public:
    HordInfo(ENEMY_KIND enemyKind, 
                const size_t qty,
                const size_t coolDown);
    ~HordInfo();
    const ENEMY_KIND getEnemyKind() const;
    const size_t getQty() const;
    const size_t getCoolDown() const;
    const size_t getSpeed() const;
    const size_t getLifePoints() const;
  };
}

#endif
