#ifndef __CIRCUIT_H__
#define __CIRCUIT_H__
#include <list>
#include <vector>
#include <tuple>
#include "position.h"

namespace Shared{
	class Circuit{
	private:
	  std::vector<Position> circuit;
	public:
	  Circuit(const std::vector<Position>& paths);
	  ~Circuit();
	  const std::vector<Position>& get();
	};
}

#endif
