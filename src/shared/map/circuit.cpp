#include "circuit.h"

using namespace Shared;

Circuit::Circuit(const std::vector<Position>& paths) {
    for (const Position& path: paths){
      Position position(path);
      circuit.push_back(position);
    }
}

Circuit::~Circuit() {}

const std::vector<Position>& Circuit::get() {
  return this->circuit;
}
