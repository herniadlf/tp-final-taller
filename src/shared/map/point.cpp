#include "point.h"
using namespace Shared;
Point::Point() : x(0), y(0) {}
Point::Point(const size_t x,const size_t y) : x(x), y(y) {}
Point::Point(const Point& point) : x(point.x), y(point.y) {}
Point::~Point() {}
size_t Point::getX() const {
	return this->x;
}
size_t Point::getY() const {
	return this->y;
}
bool Point::operator==(const Point& other){
	return (this->x == other.getX() && this->y == other.getY());
}

Point Point::operator=(const Point& other){
	return Point(other);
}

bool PointComparator::operator() (const Point& left, const Point& right) const{
	if (left.getX() == right.getX())
		return left.getY() < right.getY();
	return left.getX() < right.getX();
}

bool Point::isInVector(std::vector<Shared::Point*>& points){
	bool found = false;
	size_t index = 0;
	while(index < points.size() && !found){
		Shared::Point * thisPoint = points[index];
		if(thisPoint->getX() == getX()){
			if((thisPoint->getY() == getY())){
				found = true;
			}
		}
	index++;
	}
	return found;
}
