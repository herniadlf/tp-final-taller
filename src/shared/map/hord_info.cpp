#include "hord_info.h"
#include "yaml-cpp/yaml.h"
#include <iostream>

using namespace Shared;

HordInfo::HordInfo(Shared::ENEMY_KIND enemyKind, 
                    const size_t qty, const size_t coolDown) :
  enemyKind(enemyKind), qty(qty), coolDown(coolDown) 
{
	std::string enemyConfigPath;
	switch(enemyKind){
		case ENEMY_KIND::ABMONIBLE:
			enemyConfigPath = ABMONIBLE_CONFIG_PATH;
			break;
		case ENEMY_KIND::GOATMAN:
			enemyConfigPath = GOATMAN_CONFIG_PATH;
			break;
		default:
			enemyConfigPath = "";
	}
	YAML::Node config 				= YAML::LoadFile(enemyConfigPath);
	YAML::Node speed                = config["speed"];
	this->speed                		= speed.as<unsigned int>();
	YAML::Node lifePoints           = config["life-points"];
	this->lifePoints                = lifePoints.as<unsigned int>();
}

HordInfo::~HordInfo() {}

const ENEMY_KIND HordInfo::getEnemyKind() const {
  return this->enemyKind;
}

const size_t HordInfo::getQty() const {
  return this->qty;
}

const size_t HordInfo::getCoolDown() const {
  return this->coolDown;
}

const size_t HordInfo::getSpeed() const {
  return this->speed;
}

const size_t HordInfo::getLifePoints() const {
  return this->lifePoints;
}