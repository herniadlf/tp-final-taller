#ifndef __GAME_OBJECT_H__
#define __GAME_OBJECT_H__

namespace Shared{
	enum class GAME_OBJECT{TOWER, PROJECTILE, ENEMY, SPELL};
	enum class ENEMY_KIND{ABMONIBLE, GOATMAN};
}

#endif
