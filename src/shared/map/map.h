#ifndef __MAPA_H__
#define __MAPA_H__
#include "point.h"
#include "circuit.h"
#include "hord_info.h"
#include <vector>
#include <tuple>
#include <map>
#include <mutex>
#include <utility>
#include <string>

namespace Shared{

  class Map{
    size_t width;
    size_t height;
    int scenarioType;
    std::vector<Position> portalsIn;
    std::vector<Position> portalsOut;
    std::vector<Circuit*> paths;
    std::vector<Position> grounds;
    std::vector<Position> towers;
    std::vector<Position> projectils;
    std::vector<HordInfo*> hordes;
    std::mutex towerMutex;
  public:
    Map();
    Map(const std::string& yamlPath);
    ~Map();
    bool insertProjectile(const Point& point);
    bool canBuildTower(const Point& point);
    bool insertNewTowerOn(const Point& point);
    const std::vector<Circuit*> & getPaths();
    const std::vector<HordInfo*> & getHordes();
    const std::string getMapForClient();
    int getScenario() const;
  };
}

#endif
