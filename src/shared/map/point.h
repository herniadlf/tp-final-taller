#ifndef __POINT_H__
#define __POINT_H__

#include <cstring>
#include <vector>

namespace Shared {
	class Point{
	protected:
		const size_t x;
		const size_t y;
	public:
		Point();
		Point(const size_t x,const size_t y);
		Point(const Point& point);
		bool operator==(const Point& other);
		Point operator=(const Point& other);
		~Point();
		size_t getX() const;
		size_t getY() const;
		bool isInVector(std::vector<Shared::Point*>& points);
	};

  	struct PointComparator {
    	bool operator()(const Point& left, const Point& right) const;
  	};
}

#endif
