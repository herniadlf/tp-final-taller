#ifndef __POSITION_H__
#define __POSITION_H__

#define RATE_POINT_POSITION 40.0
#define RATE_POSITION_POINT 1.0/RATE_POS_COORD

#include <cstring>
#include "point.h"

namespace Shared {
	class Position: public Point{
	public:
		Position();
		Position(const size_t x,const size_t y);
		Position(const Point& p);
		Position(const Position& position);
		bool operator==(const Position& other);
		~Position();
	};
}

#endif
