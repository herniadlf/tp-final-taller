#include "map.h"
#include <iostream>
#include <algorithm>
#include "yaml-cpp/yaml.h"
#define MAP_PATH "maps/"
using std::cout;
using std::vector;
#define COORD_WIDTH 40
#define COORD_HEIGHT 40

using namespace Shared;

Map::Map() : width(0), height(0), scenarioType(0){}

Map::Map(const std::string& yamlPath) : Map() {
  YAML::Node map = YAML::LoadFile(MAP_PATH + yamlPath);
  YAML::Node pathsYaml = map["paths"];
  for (auto pathIterator=pathsYaml.begin(); pathIterator!=pathsYaml.end(); ++pathIterator){
    std::vector<Position> pathVector;
    for (auto circuit=pathIterator->begin(); circuit != pathIterator->end(); ++circuit){ 
      YAML::Node path = *circuit;
      int x = path[0].as<int>();
      int y = path[1].as<int>();
      Point p(x,y);
      Position position(p);
      pathVector.emplace_back(position);
    }
    Circuit *circuit = new Circuit(pathVector);
    this->paths.push_back(circuit);
  }
  YAML::Node hordesYaml = map["hordes"];
  for (auto hordesIterator=hordesYaml.begin(); hordesIterator!=hordesYaml.end(); ++hordesIterator){
    YAML::Node hord = *hordesIterator;
    std::string enemyKindStr= hord[0].as<std::string>();
    ENEMY_KIND enemyKind    = enemyKindStr == "Enemy1" ? ENEMY_KIND::ABMONIBLE : ENEMY_KIND::GOATMAN; 
    size_t qty              = hord[1].as<unsigned int>();
    size_t coolDown         = hord[2].as<unsigned int>();
    HordInfo* hordInfo      = new HordInfo(enemyKind, qty, coolDown);
    this->hordes.push_back(hordInfo);
  }
  YAML::Node grounds = map["grounds"];
  for (auto groundIterator=grounds.begin(); groundIterator!=grounds.end(); ++groundIterator){
    YAML::Node ground = *groundIterator;
    int x = ground[0].as<int>();
    int y = ground[1].as<int>();
    Point p(x,y);
    Position position(p);
    this->grounds.push_back(position);
  }
  YAML::Node portalsIn = map["portals-in"];
  for (auto portalsInIterator=portalsIn.begin(); portalsInIterator!=portalsIn.end(); ++portalsInIterator){
    YAML::Node portalIn = *portalsInIterator;
    int x = portalIn[0].as<int>();
    int y = portalIn[1].as<int>();
    Point p(x,y);
    Position position(p);
    this->portalsIn.push_back(position);
  }
  YAML::Node portalsOut = map["portals-out"];
  for (auto portalsOutIterator=portalsOut.begin(); portalsOutIterator!=portalsOut.end(); ++portalsOutIterator){
    YAML::Node portalOut = *portalsOutIterator;
    int x = portalOut[0].as<int>();
    int y = portalOut[1].as<int>();
    Point p(x,y);
    Position position(p);
    this->portalsOut.push_back(position);
  }
  YAML::Node width          = map["width"];
  this->width               = width.as<unsigned int>();
  YAML::Node height         = map["height"];
  this->height              = height.as<unsigned int>();
  YAML::Node scenario       = map["scenario"];
  this->scenarioType        = scenario.as<int>();
}

Map::~Map(){
  for (Circuit* circuit : this->paths)
    delete circuit;
  for (HordInfo* hordInfo : this->hordes)
    delete hordInfo;
} 

bool Map::insertProjectile(const Point& point){
  Position position(point);
  projectils.push_back(position);
  return true;
}

bool Map::canBuildTower(const Point& point) {
  std::unique_lock<std::mutex> locker(towerMutex);
  std::vector<Position>::iterator groundBegin = this->grounds.begin();
  std::vector<Position>::iterator groundEnd = this->grounds.end();
  Position elementPosition(point);
  bool isGround = std::find(groundBegin,groundEnd,elementPosition) != groundEnd;
  if (!isGround)
    return false;
  std::vector<Position>::iterator towerBegin = this->towers.begin();
  std::vector<Position>::iterator towerEnd = this->towers.end();
  bool hasTower = std::find(towerBegin, towerEnd, elementPosition) != towerEnd;
  return !hasTower; // if there's a tower, you cannot build a new one.
}

bool Map::insertNewTowerOn(const Point& point) {
  std::unique_lock<std::mutex> locker(towerMutex);
  Position position(point);
  towers.push_back(position);
  return true;
}

const vector<Circuit*> & Map::getPaths(){
  return this->paths;
}

const std::vector<HordInfo*>& Map::getHordes() {
  return this->hordes;
}

const std::string Map::getMapForClient() {
  YAML::Node map;
  //Paths
  YAML::Node paths;
  for (Circuit* circuit: this->paths){
    for(const Position& path: circuit->get()){
      YAML::Node pathNode;
      pathNode.push_back(path.getX());
      pathNode.push_back(path.getY());
      paths.push_back(pathNode);
    }
  }
  map["paths"] = paths;
  //Grounds
  YAML::Node grounds;
  for (const Position& ground: this->grounds){
    YAML::Node groundNode;
    groundNode.push_back(ground.getX());
    groundNode.push_back(ground.getY());
    grounds.push_back(groundNode);
  }
  map["grounds"] = grounds;
  //Portals in
  YAML::Node portalsIn;
  for (const Position& portalIn: this->portalsIn){
    YAML::Node portalInNode;
    portalInNode.push_back(portalIn.getX());
    portalInNode.push_back(portalIn.getY());
    portalsIn.push_back(portalInNode);
  }
  map["portals-in"] = portalsIn;
  //Portals out
  YAML::Node portalsOut;
  for (const Position& portalOut: this->portalsOut){
    YAML::Node portalOutNode;
    portalOutNode.push_back(portalOut.getX());
    portalOutNode.push_back(portalOut.getY());
    portalsOut.push_back(portalOutNode);
  }
  map["portals-out"] = portalsOut;
  //Config
  map["width"] = this->width;
  map["height"] = this->height;
  map["scenario"] = this->scenarioType;
  YAML::Emitter emit;
  emit << map;
  std::string mapForClient(emit.c_str());
  return mapForClient;
}

int Map::getScenario() const{
  return this->scenarioType;
}