#include "tower_info.h"
using namespace Shared;
#define VALID_FLAG 0
#define INVALID_FLAG 1

TowerInfo::TowerInfo() : info(NULL), validInfo(false) {}
TowerInfo::~TowerInfo() {
	if (this->info != NULL)
		delete info;
}

const std::string TowerInfo::LEVEL_LABEL = "level";
const std::string TowerInfo::SHOOTING_RATIO_LABEL = "shooting-ratio";
const std::string TowerInfo::HITTING_POINTS_LABEL = "hitting-points";
const std::string TowerInfo::COOLDOWN_LABEL = "cool-down";
const std::string TowerInfo::ACTUAL_XP_LABEL = "actual-xp";
const std::string TowerInfo::UPGRADE_XP_LABEL = "upgrade-xp";
const std::string TowerInfo::VALIDATION_LABEL = "validInfo";

void TowerInfo::addLevel(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[LEVEL_LABEL] = data;
}
void TowerInfo::addShootingRatio(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[SHOOTING_RATIO_LABEL] = data;
}
void TowerInfo::addHittingPoints(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[HITTING_POINTS_LABEL] = data;
}
void TowerInfo::addCoolDown(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[COOLDOWN_LABEL] = data;
}
void TowerInfo::addActualXp(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[ACTUAL_XP_LABEL] = data;
}
void TowerInfo::addUpgradeXp(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[UPGRADE_XP_LABEL] = data;
}

void TowerInfo::addValidation(size_t data){
	if (info == NULL)
		info = new YAML::Node();
	(*info)[VALIDATION_LABEL] = data;
	if(data == VALID_FLAG)
		validInfo = true;
}

void TowerInfo::setValid(){
	validInfo = true;
	addValidation(VALID_FLAG);
}

bool TowerInfo::isValid(){
	return validInfo;
}

YAML::Node* TowerInfo::getInfo(){
	if (this->info == NULL)
		this->fillEmptyInfo();
	return this->info;
}

void TowerInfo::fillEmptyInfo(){
	this->addLevel(0);
	this->addShootingRatio(0);
	this->addHittingPoints(0);
	this->addCoolDown(0);
	this->addActualXp(0);
	this->addUpgradeXp(0);
	this->addValidation(INVALID_FLAG);
}
