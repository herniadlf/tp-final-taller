#include "position.h"
using namespace Shared;

Position::Position() : Point() {}

Position::Position(const size_t x,const size_t y) : Point(x,y) {}
		
Position::Position(const Point& p) : Point(p.getX()*RATE_POINT_POSITION, 
											p.getY()*RATE_POINT_POSITION) {}
Position::Position(const Position& position) : Point(position.getX(), 
													position.getY()) {}
Position::~Position() {}

bool Position::operator==(const Position& other){
	return (this->x == other.getX() && this->y == other.getY());
}
