#include "sender.h"

using namespace Shared;
Sender::Sender(Socket& socket) : socket(socket) {}

Sender::~Sender() {}

void Sender::doSend(const char* message, size_t size){
	char* toSend = new char[size+INT_SIZE];
	unsigned int sizeToSend = htonl(size);
	memcpy(toSend, &sizeToSend, INT_SIZE);
	memcpy(toSend+INT_SIZE, message, size);
	this->socket.s_send(toSend, size+INT_SIZE);
	delete[] toSend;
}
