#ifndef __CONVERSOR_H__
#define __CONVERSOR_H__

#include "../socket/socket.h"
#include "yaml-cpp/yaml.h"
#define INT_SIZE 4

namespace Shared {
	typedef const YAML::detail::iterator_value yaml_iterator_value;
	typedef YAML::detail::iterator_base<yaml_iterator_value> yaml_iterator;

	class Conversor{
	public:
		static const YAML::Node* msgToYaml(const char* msg, size_t size);
		static const char* yamlToMsg(const YAML::Node* msg);
		static const size_t yamlGetSize(const YAML::Node* msg);
	};
}
#endif
