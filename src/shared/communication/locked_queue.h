#ifndef LOCKED_QUEUE_H
#define LOCKED_QUEUE_H
#include <queue>
#include <mutex>
#include <condition_variable>
#include <iostream>
#include "yaml-cpp/yaml.h"

namespace Shared{
  class LockedQueue{
    std::mutex mutex;
    std::condition_variable condvar;
    std::queue<const YAML::Node*> queue;
    bool isFinished;

  public:
    LockedQueue();
    ~LockedQueue();
    // Espero que text haya sido alocado en el heap previamente a ser pusheado.
    void push(const YAML::Node* text);
    // Al hacer pop, LockedQueue se desentiende del free de este elemento.
    const YAML::Node* pop();
    bool hasWork();
    void setFinished();
    size_t getLength();
  };
}

#endif
