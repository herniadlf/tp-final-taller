#ifndef __RECEIVER_H__
#define __RECEIVER_H__

#include "conversor.h"

namespace Shared {
	class Receiver{
	private:
		Socket& socket;
		bool status;
		size_t size;
		char* message;
	public:
		Receiver(Socket& socket);
		~Receiver();
		void prepareSize();
		bool read();
		size_t getSize();
		char msgCode();
		const char* msg();
		void clean();
	};
}
#endif
