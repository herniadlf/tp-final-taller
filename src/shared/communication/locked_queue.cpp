#include "locked_queue.h"
using std::condition_variable;
using namespace Shared;

LockedQueue::LockedQueue() : isFinished(false) {}

LockedQueue::~LockedQueue(){
  while (!queue.empty()){
    const YAML::Node * msg = queue.front();
    queue.pop();
    delete(msg);
  }
}

void LockedQueue::push(const YAML::Node* msg){
  std::unique_lock<std::mutex> locker(mutex);
  queue.push(msg);
  condvar.notify_one();
}

const YAML::Node* LockedQueue::pop(){
  std::unique_lock<std::mutex> locker(mutex);
  while (queue.empty()){
    if (!isFinished)
      condvar.wait(locker);
    else 
      break;
  }
  if (queue.empty())
    return NULL;
  const YAML::Node * msg = queue.front();
  queue.pop();
  return msg;
}

void LockedQueue::setFinished(){
  std::unique_lock<std::mutex> locker(mutex);
  if (!isFinished){
    isFinished = true;
    condvar.notify_one();
  }
}

bool LockedQueue::hasWork(){
  std::unique_lock<std::mutex> locker(mutex);
  return !isFinished;
}

size_t LockedQueue::getLength(){
  std::unique_lock<std::mutex> locker(mutex);
  return queue.size();
}
