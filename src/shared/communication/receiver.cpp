#include "receiver.h"

using namespace Shared;
Receiver::Receiver(Socket& socket) : socket(socket), status(true) {}

Receiver::~Receiver(){
	if (this->status)
		delete[] this->message;
}

void Receiver::prepareSize(){
	unsigned int bytes_to_read = 0;
	int received = this->socket.s_receive((char*)&bytes_to_read,INT_SIZE);
	if (received < 1){
		this->status=false;
		this->size = 0;
	} else {
		this->size = ntohl(bytes_to_read);
		this->message = new char[this->size];
	}
}

bool Receiver::read(){
	if (this->status){
		int received = this->socket.s_receive(this->message,this->size);
		if (received < 1){
			this->status=false;
			delete[] this->message;
		}
	}
	return this->status;
}

size_t Receiver::getSize(){
	return this->size;
}

char Receiver::msgCode(){
	return this->message[0];
}

const char* Receiver::msg(){
	return this->message;
}

void Receiver::clean() {
	delete[] this->message;
	this->message = NULL;
	this->size = 0;
}
