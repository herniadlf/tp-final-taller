#ifndef __SENDER_H__
#define __SENDER_H__

#include "conversor.h"

namespace Shared{
	class Sender{
	private:
		Socket& socket;
	public:
		Sender(Socket& socket);
		~Sender();
		void doSend(const char* message, size_t size);
	};
}

#endif
