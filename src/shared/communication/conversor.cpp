#include "conversor.h"

using namespace Shared;

const YAML::Node* Conversor::msgToYaml(const char* msg,const size_t size){
	char *yamlMsg = new char[size+1];
	memcpy(yamlMsg,msg,size);
	yamlMsg[size] = '\0';
	YAML::Node *command = new YAML::Node();
	*command = YAML::Load(yamlMsg);
	delete[] yamlMsg;
	return command;
}

const char* Conversor::yamlToMsg(const YAML::Node* msg){
	YAML::Emitter emitter;
	emitter << *msg;
	char* copy = new char[emitter.size()];
	memcpy(copy,emitter.c_str(),emitter.size());
	return copy;
}

const size_t Conversor::yamlGetSize(const YAML::Node* msg){
	YAML::Emitter emitter;
	emitter << *msg;
	return emitter.size();
}
