#include "game_exception.h"
using namespace Shared;

GameException::GameException(const char* err) noexcept : msgError(err) {}

const char* GameException::what() const noexcept{
	return msgError.c_str();
}

GameException::~GameException() noexcept{}