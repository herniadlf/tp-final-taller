#ifndef __GAME_EXCEPTION_H__
#define __GAME_EXCEPTION_H__

#include <typeinfo>
#include <string>

namespace Shared{
	class GameException: public std::exception{
		private:
			const std::string msgError;
		public:
			explicit GameException(const char* err) noexcept;
			virtual const char* what() const noexcept;
			virtual ~GameException() noexcept;
	};
}

#endif
