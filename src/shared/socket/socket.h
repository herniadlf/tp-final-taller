#ifndef __SOCKET_H__
#define __SOCKET_H__

#include <netdb.h>
#include <errno.h>
#include <unistd.h>
#include <iostream>
#include <cstring>

#define BACK_LOG 1
#define ERROR_SKT_RECV -1	

namespace Shared{
	class Socket{
	private:
		int fd;
	public:
		Socket();
		~Socket();
		Socket (const Socket&) = delete;
		Socket& operator=(const Socket&) = delete;
		int s_connect(const char* host, const char* service);
		int bind_n_listen(const char * service);
		int s_accept(Socket* new_socket);
		int s_shutdown(const int mode);
		int s_send(const char *buff, unsigned int size);
		int s_receive(char *buff, unsigned int size);
	};
}
#endif
