#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "../shared/socket/socket.h"
#include <string>

class Client{
private:
	Shared::Socket socket;
	std::string host;
	std::string port;
public:
	Client();
	~Client();
	bool start();
	bool end();
};
#endif
