#ifndef GTKMM_GAMEWINDOW_H
#define GTKMM_GAMEWINDOW_H
#include <gtkmm/window.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/frame.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/grid.h>
#include <gtkmm/infobar.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/dialog.h>
#include <gtkmm/stack.h>
#include <glibmm/main.h>
#include <glibmm/dispatcher.h>
#include <gtkmm/comboboxtext.h>
#include "canvas.h"

class ResultWindow : public Gtk::Box{
public:
  ResultWindow(sigc::slot<void> onReplayButtonClicked);
  virtual ~ResultWindow();
  void updateResult();
  void updateResult(bool hasWon);
private:
  Gtk::Label mainLabel;
  Gtk::Button replayButton;
  sigc::slot<void> onReplayButtonClicked;
};

class IWindow : public Gtk::Box{
public:
  IWindow();
  virtual ~IWindow();
  void notify();
protected:
  virtual void onNotificationFromGame()=0;
  Glib::Dispatcher dispatcher;
};


class GameWindow : public IWindow{
public:
  GameWindow(client::Controller &controller,
              sigc::slot<void> onResultNotificationFromGame);
  virtual ~GameWindow();
  void activateGame();
  void deactivateGame();
protected:
  //Signal handlers:
  bool onArrowKeyPressed(GdkEventKey*);
  void on_pop_up_clicked(int response);
  void onNotificationFromGame();
  sigc::slot<void> onResultNotificationFromGame;

private:
  int scroll_hadjustment;
  int scroll_vadjustment;
  Gtk::Box m_box, m_frame_box, m_area_box;
  Gtk::InfoBar infoBar;
  Gtk::Label infoLabel;
  Gtk::Frame m_frame;
  Gtk::ButtonBox m_button_box;
	Gtk::Grid m_Grid;
  Gtk::Button spellButton, towerButton, upgradeButton;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Canvas my_canvas;
  client::Controller &controller;
};


class StartWindow : public IWindow{
public:
  StartWindow(client::Controller &controller,
                sigc::slot<void> onStartNotificationFromGame,
                sigc::slot<void> onCancelButtonClicked);
  virtual ~StartWindow();
  void disableButtons();
  void enableButtons();
  void setHost();
  bool isUserHost();
protected:
  //Signal handlers:
  void onStartButtonClicked();
  void onCancelButtonClicked();
  void onNotificationFromGame();
  sigc::slot<void> onStartNotification;
  sigc::slot<void> onCancelButtonClickedHandler;

private:
  client::Controller &controller;
  Gtk::ButtonBox startButtonBox, cancelButtonBox;
  Gtk::Button startButton, cancelButton;
  bool isHost;
};

class CreateWindow : public IWindow{
public:
  CreateWindow(client::Controller &controller,
                sigc::slot<void> onCreateGameButtonClicked,
                sigc::slot<void> onCancelButtonClicked);
  virtual ~CreateWindow();
  void updateMapsList();

protected:
  //Signal handlers:
  void onNotificationFromGame();
  void onCreateGame();
  sigc::slot<void> onCreateGameButtonClicked;

private:
  client::Controller &controller;
  Gtk::Box createBox, cancelBox;
  Gtk::ComboBoxText mapsComboBox;
  Gtk::Button createButton, cancelButton;
};


class JoinWindow : public IWindow{
public:
  JoinWindow(client::Controller &controller,
                sigc::slot<void> onJoinGameButtonClicked,
                sigc::slot<void> onCancelButtonClicked);
  virtual ~JoinWindow();
  void updateGamesList();
protected:
  //Signal handlers:
  void onNotificationFromGame();
  void onJoinGame();
  sigc::slot<void> onJoinGameButtonClicked;

private:
  client::Controller &controller;
  Gtk::Box joinBox, cancelBox;
  Gtk::ComboBoxText gamesComboBox;
  Gtk::Button joinButton, cancelButton;
};



class FirstWindow : public IWindow{
public:
  FirstWindow(client::Controller &controller,
                sigc::slot<void> onCreateElectionButtonClicked,
                sigc::slot<void> onJoinElectionButtonClicked);
  virtual ~FirstWindow();

protected:
  //Signal handlers:
  void onNotificationFromGame();

private:
  client::Controller &controller;
  Gtk::Box mainBox, createBox, joinBox;
  Gtk::ButtonBox mainButtonBox;
  Gtk::Button createButton, joinButton;
};

class MainStack : public Gtk::Stack{
public:
  MainStack(client::Controller &controller);
  ~MainStack();
  void refresh();
  void onFirstWindowCreateElectionButtonClicked();
  void onFirstWindowJoinElectionButtonClicked();
  void onCreateWindowCreateGameNotification();
  void onJoinWindowJoinButtonClicked();
  void onCancelButtonClicked();
  void onStartNotification();
  void onReplayButtonClicked();
  void onGameWindowNotificationFromGame();
  void onStartWindowCancelButton();
  void onStartWindowCancelCreateButtonClicked();
  void onStartWindowCancelJoinButtonClicked();
  client::Controller& controller;

  ResultWindow resultWindow;
  GameWindow gameWindow;
  CreateWindow createWindow;
  JoinWindow joinWindow;
  StartWindow startWindow;
  FirstWindow firstWindow;
};


class MainWindow : public Gtk::Window{
public:
  MainWindow(client::Controller &controller);
  ~MainWindow();
private:
  client::Controller& controller;
  MainStack mainStack;
  Gtk::Box mainBox;
  void onHide();
};

#endif
