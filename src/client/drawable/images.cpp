#include "images.h"

Image::Image(string filename){
  source = Gdk::Pixbuf::create_from_file(filename);
  width_p = source->get_width();
  height_p = source->get_height();
}

Image::~Image(){}

unsigned int Image::getWidthInPixels(){
  return width_p;
}

unsigned int Image::getHeightInPixels(){
  return height_p;
}

void Image::setWidthInPixels(unsigned int newWidth){
  width_p = newWidth;
}

StaticImage::StaticImage(string filename) : Image(filename){
  staticImage = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, width_p, height_p);
  source->scale(staticImage, 0, 0, width_p, height_p, 0, 0, 1, 1, Gdk::InterpType::INTERP_BILINEAR);
}

StaticImage::~StaticImage(){}

Glib::RefPtr<Gdk::Pixbuf> & StaticImage::get_image(){
  return staticImage;
}

DynamicImage::DynamicImage(string filename, uint32_t imageQuantity) :
  Image(filename),
  imageQuantity(imageQuantity)
  {
    setWidthInPixels(width_p/imageQuantity);
    for (uint32_t i = 0; i < imageQuantity; ++i){
      Glib::RefPtr<Gdk::Pixbuf> staticImage = Gdk::Pixbuf::create_subpixbuf(source, i*width_p, 0, width_p, height_p);
      staticImages_v.push_back(staticImage);
    }
}

DynamicImage::~DynamicImage(){}


Glib::RefPtr<Gdk::Pixbuf> & DynamicImage::get_image(){
  return staticImages_v[0];
}

Glib::RefPtr<Gdk::Pixbuf> & DynamicImage::get_image_at(uint32_t position){
  if(position > imageQuantity){
    return staticImages_v[0];
  }else{
    return staticImages_v[position];
  }
}

const uint32_t DynamicImage::getImageQuantity(){
  return imageQuantity;
}
