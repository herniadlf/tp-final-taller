#ifndef CANVAS_H
#define CANVAS_H
#include <gtkmm/drawingarea.h>
#include <gdkmm/general.h>
#include <glibmm/main.h>
#include "drawablemap.h"

#include "../controller.h"
#include <map>

class Canvas: public Gtk::DrawingArea {
  client::Controller &controller;
  DrawableMap drawableMap;
  size_t mapWidth, mapHeight;
  unsigned int width_draw, height_draw, min_width, min_height,
  min_draw_width, min_draw_height;
  int flag;
  sigc::connection timeoutConnection;
public:
  Canvas(client::Controller &controller);
  Canvas(client::Controller &controller,
          unsigned int width, unsigned int height);
  void on_button_spell_click();
  void on_button_tower_click();
  void on_button_upgrade_click();
  DrawableMap* getDrawableMap();
  int getMapWidthInPixels();
  int getMapHeightInPixels();
  void activateCanvas();
  void deactivateCanvas();

protected:
  bool on_timeout();
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;

private:
  bool on_button_press_event(GdkEventButton *event);
  void pixelToPoint(size_t &pixelX, size_t &pixelY, int &pointX, int &pointY);
};
#endif
