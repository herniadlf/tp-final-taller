#include "windows.h"
#include <gtkmm/adjustment.h>
#include <gdk/gdkkeysyms.h>
#include <gtkmm/application.h>
#include <gtkmm/bin.h>
#include <gdkmm/rgba.h>
#include <algorithm>
#define TILE_WIDTH 158
#define TILE_HEIGHT 79
#define MAX_WIDTH_ADJUSTMENT 300
#define MAX_HEIGHT_ADJUSTMENT 300
#define MEADOW_TYPE 0
#define MEADOW_LABEL "Meadow"
#define VOLCANO_TYPE 1
#define VOLCANO_LABEL "Volcano"


IWindow::IWindow(){
  dispatcher.connect(sigc::mem_fun(*this, &IWindow::onNotificationFromGame));
}

IWindow::~IWindow(){}

void IWindow::notify(){
  dispatcher.emit();
}

GameWindow::GameWindow(client::Controller &controller,
                        sigc::slot<void> onResultNotificationFromGame)  :
                        onResultNotificationFromGame(onResultNotificationFromGame),
                        scroll_hadjustment(0),
                        scroll_vadjustment(0),
                        m_box(Gtk::ORIENTATION_VERTICAL),
                        m_frame_box(Gtk::ORIENTATION_HORIZONTAL),
                        m_area_box(Gtk::ORIENTATION_HORIZONTAL),
                        infoLabel("Welcome to Tower Defense Game"),
                        m_button_box(Gtk::ORIENTATION_HORIZONTAL),
                        spellButton("Spell"),
                        towerButton("Tower"),
                        upgradeButton("Upgrade"),
                        my_canvas(controller, 600, 500),
                        controller(controller)
{
  pack_start(m_box);

  m_box.pack_start(m_button_box, 0, 0);
  m_box.pack_start(m_frame, 2, 2);
  m_box.pack_start(infoBar, Gtk::PACK_SHRINK);

  m_button_box.pack_start(towerButton);
  m_button_box.pack_start(spellButton);
  m_button_box.pack_start(upgradeButton);

  towerButton.signal_clicked().connect(sigc::mem_fun(&my_canvas, &Canvas::on_button_tower_click));
  spellButton.signal_clicked().connect(sigc::mem_fun(&my_canvas, &Canvas::on_button_spell_click));
  upgradeButton.signal_clicked().connect(sigc::mem_fun(&my_canvas, &Canvas::on_button_upgrade_click));


  auto infoBarContainer = dynamic_cast<Gtk::Container*>(infoBar.get_content_area());
  if(infoBarContainer)
    infoBarContainer->add(infoLabel);

  infoBar.add_button("OK", 0);
  infoBar.set_message_type(Gtk::MESSAGE_INFO);
  infoBar.override_color(Gdk::RGBA("red"), Gtk::STATE_FLAG_NORMAL);
  infoBar.signal_response().connect(sigc::mem_fun(*this, &GameWindow::on_pop_up_clicked), false);

  m_frame.add(m_area_box);

  m_area_box.pack_start(m_ScrolledWindow);

  m_ScrolledWindow.set_border_width(10);
  m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  m_ScrolledWindow.add(m_Grid);

  m_Grid.set_row_spacing(10);
  m_Grid.set_column_spacing(10);
  m_Grid.attach(my_canvas, 0, 0, 330, 85);

  signal_key_press_event().connect(sigc::mem_fun(*this, &GameWindow::onArrowKeyPressed), false);

  show_all_children();

  infoBar.hide();
}

GameWindow::~GameWindow(){}

void GameWindow::onNotificationFromGame(){
  if(controller.getGame().isGameOn() && controller.getGame().isTowerInfoValid()){
    infoLabel.set_text(controller.getGame().getTowerInfo());
    infoBar.show();
  }else{
    infoLabel.set_text("Welcome to Tower Defense Game");
    onResultNotificationFromGame();
  }
}

void GameWindow::on_pop_up_clicked(int response){
  infoBar.hide();
}

bool GameWindow::onArrowKeyPressed(GdkEventKey* event){
  int max_width_adj = my_canvas.getMapWidthInPixels()*2;
  int max_height_adj = my_canvas.getMapHeightInPixels()*2;
  if(event-> type == GDK_KEY_PRESS){
    if(event->keyval == GDK_KEY_Left){
      scroll_hadjustment = ((scroll_hadjustment-5 > 0) ? scroll_hadjustment-5 : 0);
    }else
    if(event->keyval == GDK_KEY_Right){
      scroll_hadjustment = ((scroll_hadjustment+5 < max_width_adj) ? scroll_hadjustment+5 : max_width_adj);
    }else
    if(event->keyval == GDK_KEY_Up){
      scroll_vadjustment = ((scroll_vadjustment-5 > 0) ? scroll_vadjustment-5 : 0);
    }else
    if(event->keyval == GDK_KEY_Down){
      scroll_vadjustment = ((scroll_vadjustment+5 < max_height_adj) ? scroll_vadjustment+5 : max_height_adj);
    }
  }
  Glib::RefPtr<Gtk::Adjustment> thisHadj = m_ScrolledWindow.get_hadjustment();
  Glib::RefPtr<Gtk::Adjustment> thisVadj = m_ScrolledWindow.get_vadjustment();
  thisHadj->set_value(scroll_hadjustment);
  thisVadj->set_value(scroll_vadjustment);
  m_ScrolledWindow.set_hadjustment(thisHadj);
  m_ScrolledWindow.set_vadjustment(thisVadj);
  return true;
}


void GameWindow::activateGame(){
  my_canvas.activateCanvas();
}

void GameWindow::deactivateGame(){
  my_canvas.deactivateCanvas();
}

// ************************** START WINDOW ******************************** //


StartWindow::StartWindow(client::Controller &controller,
                          sigc::slot<void> onStartNotificationFromGame,
                          sigc::slot<void> onCancelButtonClicked)  :
                        onStartNotification(onStartNotificationFromGame),
                        onCancelButtonClickedHandler(onCancelButtonClicked),
                        controller(controller),
                        startButtonBox(Gtk::ORIENTATION_VERTICAL),
                        cancelButtonBox(Gtk::ORIENTATION_VERTICAL),
                        startButton("Start Match"),
                        cancelButton("Cancel"),
                        isHost(false)
{
  set_orientation(Gtk::ORIENTATION_HORIZONTAL);
  pack_start(startButtonBox);
  pack_start(cancelButtonBox);
  startButtonBox.pack_start(startButton);
  cancelButtonBox.pack_start(cancelButton);
  startButton.signal_clicked().connect(sigc::mem_fun(this, &StartWindow::onStartButtonClicked));
  cancelButton.signal_clicked().connect(sigc::mem_fun(this, &StartWindow::onCancelButtonClicked));


  startButtonBox.set_halign(Gtk::ALIGN_CENTER);
  startButtonBox.set_valign(Gtk::ALIGN_CENTER);
  startButton.set_halign(Gtk::ALIGN_CENTER);
  startButton.set_valign(Gtk::ALIGN_CENTER);
  cancelButtonBox.set_halign(Gtk::ALIGN_CENTER);
  cancelButtonBox.set_valign(Gtk::ALIGN_CENTER);
  cancelButton.set_halign(Gtk::ALIGN_CENTER);
  cancelButton.set_valign(Gtk::ALIGN_CENTER);

  show_all_children();
}

StartWindow::~StartWindow(){}

void StartWindow::onNotificationFromGame(){
  if(controller.getGame().isMatchValid()){
    onStartNotification();
  }else{
    onCancelButtonClickedHandler();
  }

//  startButton.set_sensitive(true);
}

void StartWindow::onStartButtonClicked(){
  if(controller.getGame().isMatchValid()){
    controller.startGame();
  }else{
    onCancelButtonClickedHandler();
  }
//    startButton.set_sensitive(false);
}

void StartWindow::onCancelButtonClicked(){
  if(isHost){
    controller.cancelGameCreation(controller.getGame().getMatchId());
  }else{
    controller.cancelGameJoin(controller.getGame().getMatchId());
    controller.getGame().cancelJoinGame();
  }
  // Lo manda a la ventana anterior:
//  onCancelButtonClickedHandler();
}

void StartWindow::enableButtons(){
  startButton.set_sensitive(true);
}

void StartWindow::disableButtons(){
  startButton.set_sensitive(false);
  isHost = false;
}

bool StartWindow::isUserHost(){
  return isHost;
}

void StartWindow::setHost(){
  isHost = true;
}

// ************************ RESULT WINDOW ***************************//

ResultWindow::ResultWindow(sigc::slot<void> onReplayButtonClicked) :
                            mainLabel("You Won!!!"),
                            replayButton("Replay"),
                            onReplayButtonClicked(onReplayButtonClicked)
{
  set_orientation(Gtk::ORIENTATION_VERTICAL);
  pack_start(mainLabel);
  pack_start(replayButton, Gtk::PACK_SHRINK);
  replayButton.signal_clicked().connect(onReplayButtonClicked);

  mainLabel.set_halign(Gtk::ALIGN_CENTER);
  mainLabel.set_valign(Gtk::ALIGN_CENTER);
  replayButton.set_halign(Gtk::ALIGN_CENTER);
  replayButton.set_valign(Gtk::ALIGN_CENTER);

  show_all_children();
}

ResultWindow::~ResultWindow(){}

void ResultWindow::updateResult(){
  mainLabel.set_text("Game Over");
}

void ResultWindow::updateResult(bool hasWon){
  if(hasWon){
    mainLabel.set_text("You Won!!!");
  }else{
    mainLabel.set_text("Game Over");
  }

}


// ************************ CREATE WINDOW ***************************//

CreateWindow::CreateWindow(client::Controller &controller,
                            sigc::slot<void> onCreateGameButtonClicked,
                            sigc::slot<void> onCancelButtonClicked) :
                            onCreateGameButtonClicked(onCreateGameButtonClicked),
                            controller(controller),
                            createBox(Gtk::ORIENTATION_VERTICAL),
                            cancelBox(Gtk::ORIENTATION_VERTICAL),
                            createButton("Create"),
                            cancelButton("Cancel")
{
  set_orientation(Gtk::ORIENTATION_HORIZONTAL);
  pack_start(createBox);
  createBox.pack_start(mapsComboBox, Gtk::PACK_SHRINK);
  createBox.pack_start(createButton, Gtk::PACK_SHRINK);

  createBox.set_halign(Gtk::ALIGN_CENTER);
  createBox.set_valign(Gtk::ALIGN_CENTER);
  mapsComboBox.set_halign(Gtk::ALIGN_CENTER);
  mapsComboBox.set_valign(Gtk::ALIGN_CENTER);
  createButton.set_halign(Gtk::ALIGN_CENTER);
  createButton.set_valign(Gtk::ALIGN_CENTER);


  createButton.signal_clicked().connect(sigc::mem_fun(this, &CreateWindow::onCreateGame));

  auto maps = controller.getGame().getMapsVector();
  createButton.set_sensitive(false);

  pack_start(cancelBox);
  cancelBox.pack_start(cancelButton, Gtk::PACK_SHRINK);
  cancelButton.signal_clicked().connect(onCancelButtonClicked);
  cancelBox.set_halign(Gtk::ALIGN_CENTER);
  cancelBox.set_valign(Gtk::ALIGN_CENTER);
  cancelButton.set_halign(Gtk::ALIGN_CENTER);
  cancelButton.set_valign(Gtk::ALIGN_CENTER);

  show_all_children();

}

CreateWindow::~CreateWindow(){}


void CreateWindow::onNotificationFromGame(){
  mapsComboBox.remove_all();
  auto maps = controller.getGame().getMapsVector();

  if(maps.size() == 0){
    createButton.set_sensitive(false);
  }else{
    createButton.set_sensitive(true);
    for(size_t i = 0; i < maps.size(); ++i){
      string aux = maps[i];
      mapsComboBox.append(aux);
    }
    mapsComboBox.set_active(0);
  }
}

void CreateWindow::onCreateGame(){
  int chosenMapIndex = mapsComboBox.get_active_row_number();
  if(chosenMapIndex == -1){
    chosenMapIndex = 0;
  }
  controller.createNewGame(chosenMapIndex);
  onCreateGameButtonClicked();
}

void CreateWindow::updateMapsList(){
  mapsComboBox.remove_all();
  auto maps = controller.getGame().getMapsVector();

  if(maps.size() == 0){
    createButton.set_sensitive(false);
  }else{
    createButton.set_sensitive(true);
    for(size_t i = 0; i < maps.size(); ++i){
      string aux = maps[i];
      mapsComboBox.append(aux);
    }
    mapsComboBox.set_active(0);
  }
}



// ************************ CREATE WINDOW ***************************//

JoinWindow::JoinWindow(client::Controller &controller,
                            sigc::slot<void> onJoinGameButtonClicked,
                            sigc::slot<void> onCancelButtonClicked) :
                            onJoinGameButtonClicked(onJoinGameButtonClicked),
                            controller(controller),
                            joinBox(Gtk::ORIENTATION_VERTICAL),
                            cancelBox(Gtk::ORIENTATION_VERTICAL),
                            joinButton("Join"),
                            cancelButton("Cancel")
{
  set_orientation(Gtk::ORIENTATION_HORIZONTAL);
  pack_start(joinBox);
  joinBox.pack_start(gamesComboBox, Gtk::PACK_SHRINK);
  joinBox.pack_start(joinButton, Gtk::PACK_SHRINK);

  joinBox.set_halign(Gtk::ALIGN_CENTER);
  joinBox.set_valign(Gtk::ALIGN_CENTER);
  gamesComboBox.set_halign(Gtk::ALIGN_CENTER);
  gamesComboBox.set_valign(Gtk::ALIGN_CENTER);
  joinButton.set_halign(Gtk::ALIGN_CENTER);
  joinButton.set_valign(Gtk::ALIGN_CENTER);


  joinButton.signal_clicked().connect(sigc::mem_fun(this, &JoinWindow::onJoinGame));
  joinButton.set_sensitive(false);



  pack_start(cancelBox);
  cancelBox.pack_start(cancelButton, Gtk::PACK_SHRINK);

  cancelBox.set_halign(Gtk::ALIGN_CENTER);
  cancelBox.set_valign(Gtk::ALIGN_CENTER);
  cancelButton.set_halign(Gtk::ALIGN_CENTER);
  cancelButton.set_valign(Gtk::ALIGN_CENTER);


  cancelButton.signal_clicked().connect(onCancelButtonClicked);

  show_all_children();

}

JoinWindow::~JoinWindow(){}


void JoinWindow::onNotificationFromGame(){
  gamesComboBox.remove_all();
    auto games = controller.getGame().getMatchesVector();
    if(games.size() == 0){
      joinButton.set_sensitive(false);
    }else{
      joinButton.set_sensitive(true);
      for(const idScenarioTuple tuple: games){
        size_t id     = std::get<0>(tuple);
        string id_s = std::to_string(id);
        int scenario  = std::get<1>(tuple);
        string scenarioLabel = scenario == MEADOW_TYPE ? MEADOW_LABEL : VOLCANO_LABEL;
        string aux = "Id: " + std::to_string(id) + ", Scenario: " + scenarioLabel;
        gamesComboBox.append(id_s, aux);
      }
      gamesComboBox.set_active(0);
    }

}

void JoinWindow::onJoinGame(){
  string chosenGameId_s = gamesComboBox.get_active_id();
  int chosenGameId = std::stoi(chosenGameId_s);
  if(chosenGameId == -1){
    chosenGameId = 0;
  }
  controller.joinGame(chosenGameId);
  onJoinGameButtonClicked();

}

void JoinWindow::updateGamesList() {
  this->onNotificationFromGame();
}


// ************************ FIRST WINDOW ***************************//

FirstWindow::FirstWindow(client::Controller &controller,
                          sigc::slot<void> onCreateElectionButtonClicked,
                          sigc::slot<void> onJoinElectionButtonClicked)  :
                        controller(controller),
                        mainBox(Gtk::ORIENTATION_HORIZONTAL),
                        createBox(Gtk::ORIENTATION_VERTICAL),
                        joinBox(Gtk::ORIENTATION_VERTICAL),
                        mainButtonBox(Gtk::ORIENTATION_HORIZONTAL),
                        createButton("Create New Game"),
                        joinButton("Join Match")
{
  pack_start(mainBox);

  mainBox.pack_start(createBox);
  createBox.pack_start(createButton, Gtk::PACK_SHRINK);

  createBox.set_halign(Gtk::ALIGN_CENTER);
  createBox.set_valign(Gtk::ALIGN_CENTER);
  createButton.set_halign(Gtk::ALIGN_CENTER);
  createButton.set_valign(Gtk::ALIGN_CENTER);

  mainBox.pack_start(joinBox);
  joinBox.pack_start(joinButton, Gtk::PACK_SHRINK);

  joinButton.set_halign(Gtk::ALIGN_CENTER);
  joinButton.set_valign(Gtk::ALIGN_CENTER);

  joinBox.set_halign(Gtk::ALIGN_CENTER);
  joinBox.set_valign(Gtk::ALIGN_CENTER);

  createButton.signal_clicked().connect(onCreateElectionButtonClicked);
  joinButton.signal_clicked().connect(onJoinElectionButtonClicked);

  show_all_children();
}

FirstWindow::~FirstWindow(){}

void FirstWindow::onNotificationFromGame(){}

// ************************ MAIN STACK ***************************//


MainStack::MainStack(client::Controller &controller):
                    controller(controller),
                    resultWindow(sigc::mem_fun(this, &MainStack::onCancelButtonClicked)),
                    gameWindow(controller,
                      sigc::mem_fun(this, &MainStack::onGameWindowNotificationFromGame)),
                    createWindow(controller,
                      sigc::mem_fun(this, &MainStack::onCreateWindowCreateGameNotification),
                      sigc::mem_fun(this, &MainStack::onCancelButtonClicked)),
                    joinWindow(controller,
                      sigc::mem_fun(this, &MainStack::onJoinWindowJoinButtonClicked),
                      sigc::mem_fun(this, &MainStack::onCancelButtonClicked)),
                    startWindow(controller,
                      sigc::mem_fun(this, &MainStack::onStartNotification),
                      sigc::mem_fun(this, &MainStack::onStartWindowCancelButton)),
                    firstWindow(controller,
                      sigc::mem_fun(this, &MainStack::onFirstWindowCreateElectionButtonClicked),
                      sigc::mem_fun(this, &MainStack::onFirstWindowJoinElectionButtonClicked))
                    {
  add(firstWindow);
  add(startWindow);
  add(createWindow);
  add(joinWindow);
  add(gameWindow);
  add(resultWindow);
  add_events(Gdk::BUTTON_PRESS_MASK);
  controller.getGame().setWindow(&firstWindow);
  set_visible_child(firstWindow);
  firstWindow.show();
}

MainStack::~MainStack() {}

void MainStack::onFirstWindowCreateElectionButtonClicked(){
  controller.getGame().setWindow(&createWindow);
  controller.requestMapList();
  set_visible_child(createWindow);
  createWindow.show();
}

void MainStack::onFirstWindowJoinElectionButtonClicked(){
  controller.getGame().setWindow(&joinWindow);
  controller.requestMatchList();
  set_visible_child(joinWindow);
  joinWindow.show();
}

void MainStack::onCreateWindowCreateGameNotification(){
  startWindow.setHost();
  startWindow.enableButtons();
  controller.getGame().setWindow(&startWindow);
  set_visible_child(startWindow);
  startWindow.show();
}

void MainStack::onJoinWindowJoinButtonClicked(){
  controller.getGame().setWindow(&startWindow);
  startWindow.disableButtons();
  set_visible_child(startWindow);
  startWindow.show();
}

void MainStack::onCancelButtonClicked(){
  set_visible_child(firstWindow);
  firstWindow.show();
}


void MainStack::onStartNotification(){
  gameWindow.activateGame();
  controller.getGame().setWindow(&gameWindow);
  set_visible_child(gameWindow);
  gameWindow.show();
}

void MainStack::onReplayButtonClicked(){
  controller.getGame().setWindow(&firstWindow);
  set_visible_child(firstWindow);
  firstWindow.show();

}

void MainStack::onGameWindowNotificationFromGame(){
  gameWindow.deactivateGame();
  resultWindow.updateResult(controller.getGame().getResult());
//  if (!controller.getGame().getResult()){
//    resultWindow.updateResult();
//  }
  set_visible_child(resultWindow);
  resultWindow.show();
}

void MainStack::onStartWindowCancelButton(){
  if(startWindow.isUserHost()){
    onStartWindowCancelCreateButtonClicked();
  }else{
    onStartWindowCancelJoinButtonClicked();
  }
}

void MainStack::refresh(){
  set_visible_child(firstWindow);
  firstWindow.show();
}



void MainStack::onStartWindowCancelCreateButtonClicked(){
  controller.getGame().setWindow(&createWindow);
  controller.requestMapList();
  set_visible_child(createWindow);
  createWindow.show();
}

void MainStack::onStartWindowCancelJoinButtonClicked(){
  controller.getGame().setWindow(&joinWindow);
  controller.requestMatchList();
  set_visible_child(joinWindow);
  joinWindow.show();
}

// ************************** MAIN WINDOW ******************************** //

MainWindow::MainWindow(client::Controller &controller):
                        controller(controller),
                        mainStack(controller),
                        mainBox(Gtk::ORIENTATION_VERTICAL){
  set_title("Game");
  maximize();
  set_position(Gtk::WIN_POS_CENTER);
  set_border_width(0);
  set_size_request(600, 500);
  signal_hide().connect(sigc::mem_fun(*this, &MainWindow::onHide));

  add(mainStack);
  show_all_children();
}

MainWindow::~MainWindow(){}

void MainWindow::onHide(){
  if (controller.isConnected()){
    controller.closeQueues();
  }
}
