#ifndef __IMAGE_H__
#define __IMAGE_H__
#include <gdkmm/general.h>
#include <string>
using std::string;
#include <vector>

class Image{
protected:
  Glib::RefPtr<Gdk::Pixbuf> source;
  unsigned int width_p, height_p;
public:
  Image(string filename);
  ~Image();
  Glib::RefPtr<Gdk::Pixbuf> & getSource();
  unsigned int getWidthInPixels();
  unsigned int getHeightInPixels();
  void setWidthInPixels(unsigned int newWidth);
};

class StaticImage : public Image{
  Glib::RefPtr<Gdk::Pixbuf> staticImage;
public:
  StaticImage(string filename);
  ~StaticImage();
  Glib::RefPtr<Gdk::Pixbuf> & get_image();
};


class DynamicImage : public Image{
  const uint32_t imageQuantity;
  std::vector<Glib::RefPtr<Gdk::Pixbuf>> staticImages_v;
public:
  DynamicImage(string filename, uint32_t imageQuantity);
  ~DynamicImage();
  Glib::RefPtr<Gdk::Pixbuf> & get_image();
  Glib::RefPtr<Gdk::Pixbuf> & get_image_at(uint32_t position);
  const uint32_t getImageQuantity();
};

#endif
