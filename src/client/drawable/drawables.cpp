#include "drawables.h"
#define TILE_WIDTH 158
#define TILE_HEIGHT 79

#define TOWER_HEIGHT 99
#define TOWER_WIDTH 57

#define ABMONIBLE_KIND 0
#define GOATMAN_KIND 1

Drawable::Drawable(int id) : id(id), position(NULL), isActive(true) {}

Drawable::~Drawable(){
  if (position != NULL)
    delete position;
}

const unsigned int Drawable::getX(){
  return position->getX();
}

const unsigned int Drawable::getY(){
  return position->getY();
}

const unsigned int Drawable::getId(){
  return id;
}

void Drawable::setPosition(unsigned int newX, unsigned int newY){
  if (position != NULL)
    delete position;
  position = new Shared::Position(newX,newY);
}

DrawableGround::DrawableGround(const int id, StaticImage& image): Drawable(id), image(image){}

DrawableGround::~DrawableGround(){}

bool DrawableGround::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  Gdk::Cairo::set_source_pixbuf(cr, image.get_image(), x, y);
  cr->rectangle(x, y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  return true;
}

DrawableBackground::DrawableBackground(const int id, StaticImage& image): Drawable(id), image(image){}

DrawableBackground::~DrawableBackground(){}

bool DrawableBackground::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  Gdk::Cairo::set_source_pixbuf(cr, image.get_image(), x, y);
  cr->rectangle(x, y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  return true;
}

DrawableMonster::DrawableMonster(const int id, DynamicImage & image):
                  Drawable(id),
                  image(image),
                  tile_quantity(image.getImageQuantity()){
  imagePosition = 0;
}

DrawableMonster::~DrawableMonster(){}

bool DrawableMonster::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x - image.getWidthInPixels()/3;
  int real_y = y - image.getHeightInPixels()/6;
  Glib::RefPtr<Gdk::Pixbuf> monsterImage = image.get_image_at(imagePosition)->scale_simple(image.getWidthInPixels()/2, image.getHeightInPixels()/2, Gdk::InterpType::INTERP_BILINEAR);
  Gdk::Cairo::set_source_pixbuf(cr, monsterImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawableMonster::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    imagePosition = 0;
  }else{
    imagePosition++;
  }
}

DrawableAbmonible::DrawableAbmonible(const int id, DynamicImage & image) :
                    DrawableMonster(id,image) {}

DrawableGoatman::DrawableGoatman(const int id, DynamicImage & image) :
                    DrawableMonster(id,image) {}

bool DrawableGoatman::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x - image.getWidthInPixels()/2;
  int real_y = y - image.getHeightInPixels()/2;
  Glib::RefPtr<Gdk::Pixbuf> monsterImage = image.get_image_at(imagePosition)->scale_simple(image.getWidthInPixels(), image.getHeightInPixels(), Gdk::InterpType::INTERP_BILINEAR);
  Gdk::Cairo::set_source_pixbuf(cr, monsterImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

DrawablePath::DrawablePath(const int id, StaticImage & image): Drawable(id), image(image){}

DrawablePath::~DrawablePath(){}

bool DrawablePath::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  Gdk::Cairo::set_source_pixbuf(cr, image.get_image(), x, y);
  cr->rectangle(x, y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  return true;
}

DrawablePortalIn::DrawablePortalIn(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity())
{
  imagePosition = 0;
  scaleX = image.getWidthInPixels()*4/5;
  scaleY = image.getHeightInPixels()*4/5;
}

DrawablePortalIn::~DrawablePortalIn(){}

bool DrawablePortalIn::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x  + (TILE_WIDTH/2) - scaleX/2 - scaleX/3;
  int real_y = y - scaleY/2 - scaleY/10;
  Glib::RefPtr<Gdk::Pixbuf> portalInImage = image.get_image_at(imagePosition)->scale_simple(scaleX, scaleY, Gdk::InterpType::INTERP_BILINEAR);
  Gdk::Cairo::set_source_pixbuf(cr, portalInImage, real_x, real_y);
  cr->rectangle(real_x, real_y, scaleX, scaleY);
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawablePortalIn::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    imagePosition = 0;
  }else{
    imagePosition++;
  }
}


DrawablePortalOut::DrawablePortalOut(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity())
{
  imagePosition = 0;
  scaleX = image.getWidthInPixels()*4/5;
  scaleY = image.getHeightInPixels()*4/5;
}

DrawablePortalOut::~DrawablePortalOut(){}

bool DrawablePortalOut::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x  + (TILE_WIDTH/2) - scaleX/4;
  int real_y = y - scaleY/2 - scaleY/10;
  Glib::RefPtr<Gdk::Pixbuf> portalOutImage = image.get_image_at(imagePosition)->scale_simple(scaleX, scaleY, Gdk::InterpType::INTERP_BILINEAR);
  Gdk::Cairo::set_source_pixbuf(cr, portalOutImage, real_x, real_y);
  cr->rectangle(real_x, real_y, scaleX, scaleY);
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawablePortalOut::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    imagePosition = 0;
  }else{
    imagePosition++;
  }
}



DrawableTower::DrawableTower(const int id, StaticImage & image): Drawable(id), image(image){}

DrawableTower::~DrawableTower(){}

bool DrawableTower::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x + (TILE_WIDTH/2) - image.getWidthInPixels()/2 -3;
  int real_y = y - (image.getHeightInPixels()/2) + 5;
  Gdk::Cairo::set_source_pixbuf(cr, image.get_image(), real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  return true;
}

DrawableProjectile::DrawableProjectile(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity()),
                            imagePosition(0)
{}

DrawableProjectile::~DrawableProjectile(){}

bool DrawableProjectile::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x - TOWER_WIDTH/2 -3;
  int real_y = y - (TOWER_HEIGHT/2) + 5;
  Glib::RefPtr<Gdk::Pixbuf> projectileImage = image.get_image_at(imagePosition);
  Gdk::Cairo::set_source_pixbuf(cr, projectileImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawableProjectile::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    imagePosition = 0;
  }else{
    imagePosition++;
  }
}

DrawableDeadMonster::DrawableDeadMonster(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity()),
                            imagePosition(0)
{}

DrawableDeadMonster::~DrawableDeadMonster(){}

bool DrawableDeadMonster::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  int real_x = x - image.getWidthInPixels()/3;
  int real_y = y - image.getHeightInPixels()/6;
  Glib::RefPtr<Gdk::Pixbuf> monsterImage = image.get_image_at(imagePosition)->scale_simple(image.getWidthInPixels()/2, image.getHeightInPixels()/2, Gdk::InterpType::INTERP_BILINEAR);
  Gdk::Cairo::set_source_pixbuf(cr, monsterImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawableDeadMonster::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    isActive = false;
  }else{
    imagePosition++;
  }
}

DrawableDeadAbmonible::DrawableDeadAbmonible(const int id, DynamicImage & image):
                        DrawableDeadMonster(id, image) {}

DrawableDeadGoatman::DrawableDeadGoatman(const int id, DynamicImage & image):
                        DrawableDeadMonster(id, image) {}

DrawableExplosion::DrawableExplosion(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity()),
                            imagePosition(0)
{}

DrawableExplosion::~DrawableExplosion(){}

bool DrawableExplosion::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  if(!isActive)
    return false;
  int real_x = x - TOWER_WIDTH/2 -3;
  int real_y = y - (TOWER_HEIGHT/2) + 5;
  Glib::RefPtr<Gdk::Pixbuf> explosionImage = image.get_image_at(imagePosition);
  Gdk::Cairo::set_source_pixbuf(cr, explosionImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawableExplosion::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    isActive = false;
  }else{
    imagePosition++;
  }
}


DrawableSpell::DrawableSpell(const int id, DynamicImage & image):
                            Drawable(id),
                            image(image),
                            tile_quantity(image.getImageQuantity()),
                            imagePosition(0)
{}

DrawableSpell::~DrawableSpell(){}

bool DrawableSpell::draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y){
  if(!isActive)
    return false;
  int real_x = x - image.getWidthInPixels()/2;
  int real_y = y - image.getHeightInPixels()/2;
  Glib::RefPtr<Gdk::Pixbuf> spellImage = image.get_image_at(imagePosition);
  Gdk::Cairo::set_source_pixbuf(cr, spellImage, real_x, real_y);
  cr->rectangle(real_x, real_y, image.getWidthInPixels(), image.getHeightInPixels());
  cr->fill();
  updateImagePosition();
  return true;
}

void DrawableSpell::updateImagePosition(){
  if(imagePosition == tile_quantity - 1){
    isActive = false;
  }else{
    imagePosition++;
  }
}
