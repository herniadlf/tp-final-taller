#include "../../shared/thread/thread.h"

#include <gtkmm/application.h>
#include "windows.h"

class App: public Thread{
public:
	Glib::RefPtr<Gtk::Application>& app;
	MainWindow mainWindow;
	App(Glib::RefPtr<Gtk::Application>& app, client::Controller &controller);
	~App();
	void run() override;
	void assignGame(Game &game);
};
