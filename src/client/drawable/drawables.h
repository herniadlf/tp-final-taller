#ifndef __DRAWABLE_H__
#define __DRAWABLE_H__

#include <cairomm/context.h>
#include "images.h"
#include "../../shared/map/position.h"

#include <gdkmm/general.h>
#include <gdkmm/pixbuf.h>

class Drawable{
public:
  Drawable(int id);
  virtual ~Drawable();
  virtual bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y) = 0;
  const unsigned int getX();
  const unsigned int getY();
  void setPosition(unsigned int newX, unsigned int newY);
  const unsigned int getId();
protected:
  const unsigned int id;
  Shared::Position* position;
  bool isActive;
};

class DrawableGround : public Drawable{
public:
  DrawableGround(const int id,  StaticImage& image);
  ~DrawableGround();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  StaticImage& image;
};

class DrawableBackground : public Drawable{
public:
  DrawableBackground(const int id, StaticImage& image);
  ~DrawableBackground();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  StaticImage& image;
};

class DrawableMonster : public Drawable{
public:
  DrawableMonster(const int id, DynamicImage & image);
  virtual ~DrawableMonster();
  virtual bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
protected:
  uint32_t imagePosition;
  DynamicImage & image;
  const uint32_t tile_quantity;
  void updateImagePosition();
};

class DrawableAbmonible : public DrawableMonster{
public:
  DrawableAbmonible(const int id, DynamicImage & image);
};

class DrawableGoatman : public DrawableMonster{
public:
  DrawableGoatman(const int id, DynamicImage & image);
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y) override;
};

class DrawablePath : public Drawable{
public:
  DrawablePath(const int id, StaticImage& image);
  ~DrawablePath();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  StaticImage &image;
};

class DrawablePortalIn : public Drawable{
public:
  DrawablePortalIn(const int id, DynamicImage & image);
  ~DrawablePortalIn();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  uint32_t imagePosition;
  int scaleX, scaleY;
  DynamicImage & image;
  const uint32_t tile_quantity;
  void updateImagePosition();
};

class DrawablePortalOut : public Drawable{
public:
  DrawablePortalOut(const int id, DynamicImage & image);
  ~DrawablePortalOut();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  uint32_t imagePosition;
  int scaleX, scaleY;
  DynamicImage & image;
  const uint32_t tile_quantity;
  void updateImagePosition();
};

class DrawableTower : public Drawable{
public:
  DrawableTower(const int id, StaticImage & image);
  ~DrawableTower();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  StaticImage & image;
};

class DrawableProjectile : public Drawable{
public:
  DrawableProjectile(const int id, DynamicImage & image);
  ~DrawableProjectile();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  DynamicImage & image;
  const uint32_t tile_quantity;
  uint32_t imagePosition;
  void updateImagePosition();
};

class DrawableDeadMonster : public Drawable{
public:
  DrawableDeadMonster(const int id, DynamicImage & image);
  ~DrawableDeadMonster();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  DynamicImage & image;
  const uint32_t tile_quantity;
  uint32_t imagePosition;
  void updateImagePosition();
};

class DrawableDeadAbmonible : public DrawableDeadMonster{
public:
  DrawableDeadAbmonible(const int id, DynamicImage &image);
};

class DrawableDeadGoatman : public DrawableDeadMonster{
public:
  DrawableDeadGoatman(const int id, DynamicImage &image);
};

class DrawableExplosion : public Drawable{
public:
  DrawableExplosion(const int id, DynamicImage & image);
  ~DrawableExplosion();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  DynamicImage & image;
  const uint32_t tile_quantity;
  uint32_t imagePosition;
  void updateImagePosition();
};


class DrawableSpell : public Drawable{
public:
  DrawableSpell(const int id, DynamicImage & image);
  ~DrawableSpell();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
private:
  DynamicImage & image;
  const uint32_t tile_quantity;
  uint32_t imagePosition;
  void updateImagePosition();
};

#endif
