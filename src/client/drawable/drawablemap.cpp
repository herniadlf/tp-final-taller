#include "drawablemap.h"
#include <iostream>
#define OUTDOOR_PATH "drawable/outdoor.png"
#define FORTRESS_PATH "drawable/fortress.png"
#define TOWER_PATH "drawable/tower.png"
#define ABMONIBLE_PATH "drawable/monster.png"
#define GOATMAN_PATH "drawable/monster2.png"
#define GROUND_PATH "drawable/ground.png"
#define PORTAL_IN_PATH "drawable/portal_in2.png"
#define PORTAL_OUT_PATH "drawable/portalout.png"
#define PROJECTILE_PATH "drawable/projectile.png"
#define DEAD_ABMONIBLE_PATH "drawable/deadmonster.png"
#define DEAD_GOATMAN_PATH "drawable/deadmonster2.png"
#define EXPLOSION_PATH "drawable/explosion.png"
#define SPELL_PATH "drawable/spell2.png"
#define TILE_WIDTH 158
#define TILE_HEIGHT 79
#define ABMONIBLE_QUANTITY 6
#define GOATMAN_QUANTITY 8
#define PORTAL_IN_QUANTITY 10
#define PROJECTILE_QUANTITY 15
#define DEAD_ABMONIBLE_QUANTITY 18
#define DEAD_GOATMAN_QUANTITY 19
#define EXPLOSION_QUANTITY 13
#define SPELL_QUANTITY 13
#define PORTAL_OUT_QUANTITY 15

#define BACKGROUND 9999
#define PATH 9998
#define MONSTER 9994
#define GROUND 9995
#define PORTAL_IN 9993
#define PORTAL_OUT 9996

#define RATE_POINT_POS 40.0
#define RATE_POS_POINT 1.0/RATE_POINT_POS

DrawableMap::DrawableMap(ClientMap &map) :
  map(map),
  width(map.getMapWidth()),
  height(map.getMapHeight()),
  backgroundImage(map.getScenarioBackgroundImagePath()),
  pathImage(map.getScenarioPathImagePath()),
  towerImage(TOWER_PATH),
  groundImage(map.getScenarioGroundImagePath()),
  abmonibleImage(ABMONIBLE_PATH, ABMONIBLE_QUANTITY),
  goatmanImage(GOATMAN_PATH, GOATMAN_QUANTITY),
  portalInImage(PORTAL_IN_PATH, PORTAL_IN_QUANTITY),
  portalOutImage(PORTAL_OUT_PATH, PORTAL_OUT_QUANTITY),
  projectileImage(PROJECTILE_PATH, PROJECTILE_QUANTITY),
  deadAbmonibleImage(DEAD_ABMONIBLE_PATH, DEAD_ABMONIBLE_QUANTITY),
  deadGoatmanImage(DEAD_GOATMAN_PATH, DEAD_GOATMAN_QUANTITY),
  explosionImage(EXPLOSION_PATH, EXPLOSION_QUANTITY),
  spellImage(SPELL_PATH, SPELL_QUANTITY)
  {}

DrawableMap::~DrawableMap(){
  std::map<Shared::Point, std::vector<Drawable*>*>::iterator it;

  for(it = index.begin(); it != index.end(); ++it){
    std::vector<Drawable*>* drawables = it->second;
    while(!drawables->empty()){
      delete((*drawables)[drawables->size()-1]);
      drawables->pop_back();
    }
    delete(drawables);
  }
}

void DrawableMap::load(){
  std::unique_lock<std::mutex> locker(mu);

  backgroundImage = StaticImage(map.getScenarioBackgroundImagePath());
  pathImage = StaticImage(map.getScenarioPathImagePath());
  groundImage = StaticImage(map.getScenarioGroundImagePath());

  width = map.getMapWidth();
  height = map.getMapHeight();
  loadPaths();
  loadGrounds();
  loadPortals();
}

std::vector<Drawable*>* DrawableMap::getVectorAt(const Shared::Point& point){

  std::vector<Drawable*>* drawables = NULL;
  std::map<Shared::Point, std::vector<Drawable*>*>::iterator it = index.find(point);
  if(it != index.end()){
    drawables = index[point];
  }else{
    drawables = new std::vector<Drawable*>();
    index[point] = drawables;
  }
  return drawables;
}

void DrawableMap::loadPaths(){

  const std::vector<Shared::Position> paths = map.get_paths();
  for (const Shared::Position& position : paths) {
    DrawablePath * newPath = new DrawablePath(PATH, pathImage);
    unsigned int x = position.getX() * RATE_POS_POINT;
    unsigned int y = position.getY() * RATE_POS_POINT;
    Shared::Point point(x,y);
    vector<Drawable*>* coordDrawables = getVectorAt(point);
    newPath->setPosition(position.getX(), position.getY());
    coordDrawables->push_back(newPath);
  }
}

void DrawableMap::loadGrounds(){

  const std::vector<Shared::Position> grounds = map.get_grounds();
  for (const Shared::Position& position : grounds) {
    DrawableGround * newGround = new DrawableGround(GROUND, groundImage);
    unsigned int x = position.getX() * RATE_POS_POINT;
    unsigned int y = position.getY() * RATE_POS_POINT;
    Shared::Point point(x,y);
    vector<Drawable*>* coordDrawables = getVectorAt(point);
    newGround->setPosition(position.getX(), position.getY());
    coordDrawables->push_back(newGround);
  }
}


void DrawableMap::loadPortals(){

  const std::vector<Shared::Position> portalsIn = map.get_portals_in();
  for (const Shared::Position& position: portalsIn){
    DrawablePortalIn * newPortalIn = new DrawablePortalIn(PORTAL_IN, portalInImage);
    unsigned int x = position.getX() * RATE_POS_POINT;
    unsigned int y = position.getY() * RATE_POS_POINT;
    Shared::Point point(x,y);
    vector<Drawable*>* coordDrawables = getVectorAt(point);
    newPortalIn->setPosition(position.getX(), position.getY());
    coordDrawables->push_back(newPortalIn);
  }
  const std::vector<Shared::Position> portalsOut = map.get_portals_out();
  for (const Shared::Position& position: portalsOut){
    DrawablePortalOut * newPortalOut = new DrawablePortalOut(PORTAL_OUT, portalOutImage);
    unsigned int x = position.getX() * RATE_POS_POINT;
    unsigned int y = position.getY() * RATE_POS_POINT;
    Shared::Point point(x,y);
    vector<Drawable*>* coordDrawables = getVectorAt(point);
    newPortalOut->setPosition(position.getX(), position.getY());
    coordDrawables->push_back(newPortalOut);
  }

}

void DrawableMap::addTower(uint32_t id, const Shared::Point& point){
  std::unique_lock<std::mutex> locker(mu);
  vector<Drawable*>* drawables = getVectorAt(point);
  DrawableTower * newTower = new DrawableTower(id, towerImage);
  Shared::Position position(point);
  newTower->setPosition(position.getX(), position.getY());
  drawables->push_back(newTower);
}

void DrawableMap::movePositionObject(unsigned int id,
                                    unsigned int dest_x,
                                    unsigned int dest_y,
                                    Shared::GAME_OBJECT kind,
                                    Shared::ENEMY_KIND enemyKind){
  std::unique_lock<std::mutex> locker(mu);
  unsigned int destXPoint = dest_x * RATE_POS_POINT;
  unsigned int destYPoint = dest_y * RATE_POS_POINT;
  Shared::Point destPoint(destXPoint,destYPoint);
  auto itDrawable = this->movables.find(id);
  if (itDrawable != this->movables.end()) {
    Drawable* drawable = itDrawable->second;
    unsigned int posXOrig = drawable->getX();
    unsigned int posYOrig = drawable->getY();
    Shared::Position origPosition(posXOrig, posYOrig);
    Shared::Point origPoint(posXOrig*RATE_POS_POINT, posYOrig*RATE_POS_POINT);
    vector<Drawable*>* src = getVectorAt(origPoint);
    vector<Drawable*>* dest = getVectorAt(destPoint);
    if (src != dest) {
      auto itEnemy = std::find(src->begin(), src->end(),drawable);
      if (itEnemy != src->end())
          src->erase(itEnemy);
      dest->push_back(drawable);
    }
    drawable->setPosition(dest_x, dest_y);

  } else {
    vector<Drawable*>* dest = getVectorAt(destPoint);
    Drawable* object = NULL;
    switch(kind){
        case Shared::GAME_OBJECT::TOWER:
            throw Shared::GameException("No corresponde el movimiento de torres - movePositionObject.drawablemap.cpp");
        case Shared::GAME_OBJECT::ENEMY:
            if (enemyKind == Shared::ENEMY_KIND::ABMONIBLE)
              object = new DrawableAbmonible(id, abmonibleImage);
            else if (enemyKind == Shared::ENEMY_KIND::GOATMAN)
              object = new DrawableGoatman(id, goatmanImage);
            break;
        case Shared::GAME_OBJECT::PROJECTILE:
            object = new DrawableProjectile(id, projectileImage);
            break;
        case Shared::GAME_OBJECT::SPELL:
            object = new DrawableSpell(id,spellImage);
            break;
    }
    dest->push_back(object);
    object->setPosition(dest_x, dest_y);
    this->movables[id] = object;
  }
}

void DrawableMap::cleanObject(unsigned int id,
                                    unsigned int x,
                                    unsigned int y,
                                    Shared::GAME_OBJECT kind,
                                    Shared::ENEMY_KIND enemyKind){
  std::unique_lock<std::mutex> locker(mu);
  auto itDrawable = this->movables.find(id);
  if (itDrawable != this->movables.end()) {
    Drawable* drawable = itDrawable->second;
      this->movables.erase(itDrawable);

      unsigned int posXOrig = drawable->getX();
      unsigned int posYOrig = drawable->getY();
      Shared::Position origPosition(posXOrig, posYOrig);
      Shared::Point origPoint(posXOrig*RATE_POS_POINT, posYOrig*RATE_POS_POINT);

      vector<Drawable*>* src = getVectorAt(origPoint);
      if (src) {
        auto itEnemy = std::find(src->begin(), src->end(),drawable);
        if (itEnemy != src->end())
            src->erase(itEnemy);

        switch(kind){
          case Shared::GAME_OBJECT::TOWER:
              throw Shared::GameException("No corresponde el cleaning de torres - movePositionObject.drawablemap.cpp");
          case Shared::GAME_OBJECT::SPELL:
//              throw Shared::GameException("No corresponde el cleaning de hechizos - movePositionObject.drawablemap.cpp");
              break;
          case Shared::GAME_OBJECT::ENEMY:
              if (enemyKind == Shared::ENEMY_KIND::ABMONIBLE){
                Drawable* deadDrawable = new DrawableDeadAbmonible(id, deadAbmonibleImage);
                deadDrawable->setPosition(posXOrig, posYOrig);
                src->push_back(deadDrawable);
              }
              else if (enemyKind == Shared::ENEMY_KIND::GOATMAN){
                Drawable* deadDrawable = new DrawableDeadGoatman(id, deadGoatmanImage);
                deadDrawable->setPosition(posXOrig, posYOrig);
                src->push_back(deadDrawable);
              }
              break;
          case Shared::GAME_OBJECT::PROJECTILE:
              Drawable* explosionDrawable = new DrawableExplosion(id, explosionImage);
              explosionDrawable->setPosition(posXOrig, posYOrig);
              src->push_back(explosionDrawable);
              break;
        }
      }
      delete(drawable);
  } else {
      throw Shared::GameException("Drawable no encontrado - cleanObject.drawablemap.cpp");

  }
}

bool DrawableMap::draw(const Cairo::RefPtr<Cairo::Context>&cr, unsigned int x_from,
                                                  unsigned int y_from,
                                                  unsigned int x_to,
                                                  unsigned int y_to){
  size_t maxCoordX = width;
  size_t maxCoordY = height;
  std::unique_lock<std::mutex> locker(mu);

  DrawableBackground newBackground(BACKGROUND, backgroundImage);
  for (size_t coordY = 0; coordY <= maxCoordY; ++coordY) {
    for (int coordX = maxCoordX; coordX >=0; --coordX) {
      unsigned int tileX = (coordX + coordY) * TILE_WIDTH/2;
      unsigned int tileY = (coordY - coordX + maxCoordX) * TILE_HEIGHT/2;
      newBackground.draw(cr, tileX, tileY);
      Shared::Point point(coordX,coordY);
      for(Drawable* drawable : *getVectorAt(point)) {
        unsigned int xOnTile = drawable->getX() % (int)RATE_POINT_POS;
        unsigned int yOnTile = drawable->getY() % (int)RATE_POINT_POS;
        int offsetTileX = (xOnTile + yOnTile) * RATE_POS_POINT * TILE_WIDTH/2;
        int offsetTileY = ((int)yOnTile - (int)xOnTile) * RATE_POS_POINT * TILE_HEIGHT/2;
        drawable->draw(cr, tileX + offsetTileX, tileY + offsetTileY);
      }
    }
  }
  return true;
}


unsigned int DrawableMap::getMapWidthInPixels(){
  return width*TILE_WIDTH;
}

unsigned int DrawableMap::getMapHeightInPixels(){
  return height*TILE_HEIGHT;
}

void DrawableMap::clearMap(){
  std::unique_lock<std::mutex> locker(mu);

  index.clear();
  movables.clear();

/*  std::map<Shared::Point, std::vector<Drawable*>*>::iterator it;
  for(it = index.begin(); it != index.end(); ++it){
    std::vector<Drawable*>* drawables = it->second;
    while(!drawables->empty()){
      delete((*drawables)[drawables->size()-1]);
      drawables->pop_back();
    }
    delete(drawables);
  }*/

}
