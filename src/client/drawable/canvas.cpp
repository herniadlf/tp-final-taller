#include "canvas.h"
#define TOWER_FLAG 5
#define SPELL_FLAG 9
#define INFO_FLAG 1
#define UPGRADE_FLAG 2
#define OUTDOOR_PATH "outdoor.png"
#define FORTRESS_PATH "fortress.png"
#define TOWER_PATH "tower.png"
#define TILE_WIDTH 158
#define TILE_HEIGHT 79
#define TOWER_WIDTH 57
#define TOWER_HEIGHT 99
#define HEIGHT_DIFFERENCE 40
#define WIDTH_DIFFERENCE 80
#define TIME_TO_REDRAW 50
#include <gtkmm/window.h>

Canvas::Canvas(client::Controller &controller,
                unsigned int width, unsigned int height) :
  controller(controller),
  drawableMap(controller.getGame().getLogicalMap()),
  mapWidth(drawableMap.getMapWidthInPixels()),
  mapHeight(drawableMap.getMapHeightInPixels()),
  width_draw(width),
  height_draw(height),
  min_width(0),
  min_height(0),
  min_draw_width(0),
  min_draw_height(0)
{
  add_events(Gdk::BUTTON_PRESS_MASK);
  controller.getGame().assignDrawableMap(&drawableMap);

//  Glib::signal_timeout().connect(sigc::mem_fun(*this, &Canvas::on_timeout), TIME_TO_REDRAW);
}


int Canvas::getMapWidthInPixels(){
  mapWidth = drawableMap.getMapWidthInPixels();
  return mapWidth;
}

int Canvas::getMapHeightInPixels(){
  mapHeight = drawableMap.getMapHeightInPixels();
  return mapHeight;
}

void Canvas::activateCanvas(){
  timeoutConnection = Glib::signal_timeout().connect(sigc::mem_fun(*this, &Canvas::on_timeout), TIME_TO_REDRAW);
}

void Canvas::deactivateCanvas(){
  timeoutConnection.disconnect();
  drawableMap.clearMap();
}


bool Canvas::on_timeout(){
//  if(!controller.getGame().isGameOn())
//    return false;
  auto win = get_window();
  if(win){
    Gdk::Rectangle r(0, 0, get_allocation().get_width(), get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return true;
}

void Canvas::on_button_tower_click(){
  flag = TOWER_FLAG;
}

void Canvas::on_button_spell_click(){
  flag = SPELL_FLAG;
}

void Canvas::on_button_upgrade_click(){
  flag = UPGRADE_FLAG;
}

void Canvas::pixelToPoint(size_t &pixelX, size_t &pixelY, int &pointX, int &pointY){
  unsigned int mapWidth = controller.getGame().getLogicalMap().getMapWidth();
  int x_i = pixelX/(TILE_WIDTH/2);
  int y_i = pixelY/(TILE_HEIGHT/2);
  int m_x = x_i%2;
  int m_y = y_i%2;
  int x_f = pixelX - (x_i*(TILE_WIDTH/2));
  int y_f = pixelY - (y_i*(TILE_HEIGHT/2));
  if (m_x == 0 && m_y == 0){
    if(y_f < (TILE_HEIGHT/2) - (x_f/(TILE_WIDTH/TILE_HEIGHT))){
      pointX = mapWidth/2 + x_i/2 - y_i/2;
      pointY = x_i/2 + y_i/2 - mapWidth/2 -1;
    }else{
      pointX = mapWidth/2 + x_i/2 - y_i/2;
      pointY = x_i/2 + y_i/2 - mapWidth/2;
    }
  }
  else{
    if(m_x != 0 && m_y != 0){
      if(y_f < (TILE_HEIGHT/2) - (x_f/(TILE_WIDTH/TILE_HEIGHT))){
        pointX = mapWidth/2 + x_i/2 - y_i/2;
        pointY = x_i/2 + y_i/2 - mapWidth/2;

      }else{
        pointX = mapWidth/2 + x_i/2 - y_i/2;
        pointY = x_i/2 + y_i/2 - mapWidth/2 + 1;
    }
  }
  else{
    if(m_x == 0 && m_y != 0){
      if(y_f < (x_f/(TILE_WIDTH/TILE_HEIGHT))){
        pointX = mapWidth/2 + x_i/2 - y_i/2;
        pointY = x_i/2 + y_i/2 - mapWidth/2;
      }else{
          pointX = mapWidth/2 + x_i/2 - y_i/2 - 1;
          pointY = x_i/2 + y_i/2 - mapWidth/2;
      }
    }
    else{
      if (m_x != 0 && m_y == 0){
        if(y_f < (x_f/(TILE_WIDTH/TILE_HEIGHT))){
        pointX = mapWidth/2 + x_i/2 - y_i/2 + 1;
        pointY = x_i/2 + y_i/2 - mapWidth/2;

        }else{
          pointX = mapWidth/2 + x_i/2 - y_i/2;
          pointY = x_i/2 + y_i/2 - mapWidth/2;
        }
      }
    }
  }
}

}


bool Canvas::on_button_press_event(GdkEventButton *event){
    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1)){

      size_t x = event->x;
      size_t y = event->y;
      int x_1, y_1;
      pixelToPoint(x, y, x_1, y_1);
      if(x_1 < 0 || y_1 < 0){
        return false;
      }
      if(flag == TOWER_FLAG){
        if(x_1 >= 0 && y_1 >= 0){
          controller.requestTower(x_1, y_1);
        }
      }
      if(flag == SPELL_FLAG){
        controller.requestSpell(x_1, y_1);
      }

      if(flag == UPGRADE_FLAG){
        controller.requestUpgrade(x_1, y_1);
      }

      if(flag == INFO_FLAG){
        controller.requestTowerInfo(x_1, y_1);
      }
      queue_draw();
    }
    flag = INFO_FLAG;
    return true;
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  drawableMap.draw(cr, 0, 0, mapWidth, mapHeight);
  return true;
}

DrawableMap* Canvas::getDrawableMap(){
  return &drawableMap;
}
