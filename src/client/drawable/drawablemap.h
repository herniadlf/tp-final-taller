#ifndef __DRAWABLE_MAP_H__
#define __DRAWABLE_MAP_H__
#include "../clientmap.h"
#include "../../shared/map/point.h"
#include "../../shared/map/position.h"
#include "../../shared/map/game_object.h"
#include "../../shared/exception/game_exception.h"
#include "drawables.h"
#include <cairomm/context.h>
#include <gdkmm/general.h>
#include "images.h"
#include <map>
#include <vector>
#include <utility>
#include <mutex>
using std::map;
using std::vector;
using std::mutex;

class DrawableMap{
private:
  std::map<Shared::Point, std::vector<Drawable*>*,Shared::PointComparator> index;
  std::map<unsigned int, Drawable*> movables;
  ClientMap &map;
  std::mutex mu;
  size_t width;
  size_t height;

public:
  DrawableMap(ClientMap &map);
  ~DrawableMap();
  void load();
  void clearMap();
  bool draw(const Cairo::RefPtr<Cairo::Context>&cr, unsigned int x_from,
                                                    unsigned int y_from,
                                                    unsigned int x_to,
                                                    unsigned int y_to);
  void addTower(uint32_t id, const Shared::Point& point);
  void movePositionObject(unsigned int id,
                          unsigned int dest_x,
                          unsigned int dest_y,
                          Shared::GAME_OBJECT kind,
                          Shared::ENEMY_KIND enemyKind);
  void cleanObject(unsigned int id,
                          unsigned int x,
                          unsigned int y,
                          Shared::GAME_OBJECT kind,
                          Shared::ENEMY_KIND enemyKind);
  unsigned int getMapWidthInPixels();
  unsigned int getMapHeightInPixels();

private:
  void loadPaths();
  void loadPortals();
  void loadGrounds();
  StaticImage backgroundImage, pathImage, towerImage, groundImage;
  DynamicImage abmonibleImage, goatmanImage, portalInImage, portalOutImage,
                projectileImage, deadAbmonibleImage, deadGoatmanImage,
                explosionImage, spellImage;
  std::vector<Drawable*>* getVectorAt(const Shared::Point& point);
  std::pair<unsigned int, unsigned int> calculateIsometricCoordenates(unsigned int height, unsigned int &x, unsigned int &y);
};

#endif
