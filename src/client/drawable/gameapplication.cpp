#include "gameapplication.h"

App::App(Glib::RefPtr<Gtk::Application>& app, client::Controller &controller) :
	app(app),
	mainWindow(controller)
	{}

App::~App() {}

void App::run() {
	app->run(this->mainWindow);
}
