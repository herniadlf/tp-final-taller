#ifndef __INPUT_H__
#define __INPUT_H__
#include "../shared/thread/thread.h"
#include "../shared/socket/socket.h"
#include "../shared/communication/locked_queue.h"
#include "../shared/communication/receiver.h"

namespace client{
	class Input: public Thread{
	private:
		Shared::Socket& socket;
		Shared::LockedQueue& queue;
		bool& connected;
	public:
		Input(Shared::Socket& socket, Shared::LockedQueue& queue, bool& connected);
		~Input();
		void run() override;
	};
}

#endif
