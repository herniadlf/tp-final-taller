#include "clientmap.h"
#include <iostream>
#include <algorithm>
#include "yaml-cpp/yaml.h"
#define MAP_PATH "maps/"

#define MEADOW_TYPE 0
#define VOLCANO_TYPE 1

#define MEADOW_PATH "drawable/outdoor.png"
#define LAVA_PATH "drawable/lava.png"
#define MEADOW_GROUND_PATH "drawable/ground.png"
#define LAVA_GROUND_PATH "drawable/lavaground.png"

#define LAVA_PATH_PATH "drawable/lavapath.png"
#define MEADOW_PATH_PATH "drawable/fortress.png"

ClientMap::ClientMap() : scenarioType(MEADOW_TYPE) {}

void ClientMap::loadMap(const std::string& yamlMap) {
  portalsIn.clear();
  portalsOut.clear();
  paths.clear();
  grounds.clear();
  towers.clear();
  projectils.clear();
  enemies.clear();
  std::unique_lock<std::mutex> locker(mu);
  YAML::Node map = YAML::Load(yamlMap);
  this->scenarioType = map["scenario"].as<int>();
  this->mapWidth = map["width"].as<unsigned int>();
  this->mapHeight = map["height"].as<unsigned int>();
  this->scenarioType = map["scenario"].as<int>();
  YAML::Node paths = map["paths"];
  for (auto pathIterator=paths.begin(); pathIterator!=paths.end(); ++pathIterator){
    YAML::Node path = *pathIterator;
    int x = path[0].as<int>();
    int y = path[1].as<int>();
    Shared::Position position(x,y);
    this->paths.push_back(position);
  }
  YAML::Node grounds = map["grounds"];
  for (auto groundIterator=grounds.begin(); groundIterator!=grounds.end(); ++groundIterator){
    YAML::Node ground = *groundIterator;
    int x = ground[0].as<int>();
    int y = ground[1].as<int>();
    Shared::Position position(x,y);
    this->grounds.push_back(position);
  }
  YAML::Node portalsIn = map["portals-in"];
  for (auto portalsInIterator=portalsIn.begin(); portalsInIterator!=portalsIn.end(); ++portalsInIterator){
    YAML::Node thisPortalIn = *portalsInIterator;
    int x = thisPortalIn[0].as<int>();
    int y = thisPortalIn[1].as<int>();
    Shared::Position position(x,y);
    this->portalsIn.push_back(position);
  }

  YAML::Node portalsOut = map["portals-out"];
  for (auto portalsOutIterator=portalsOut.begin(); portalsOutIterator!=portalsOut.end(); ++portalsOutIterator){
    YAML::Node thisPortalOut = *portalsOutIterator;
    int x = thisPortalOut[0].as<int>();
    int y = thisPortalOut[1].as<int>();
    Shared::Position position(x,y);
    this->portalsOut.push_back(position);
  }
}

ClientMap::~ClientMap(){}

unsigned int ClientMap::getMapHeight(){
  std::unique_lock<std::mutex> locker(mu);

  return mapHeight;
}

unsigned int ClientMap::getMapWidth(){
  std::unique_lock<std::mutex> locker(mu);

  return mapWidth;
}

bool ClientMap::insert_tower_on(const Shared::Point& point){
  std::unique_lock<std::mutex> locker(mu);

  size_t length = grounds.size();
  double x2 = point.getX();
  double y2 = point.getY();
  for(size_t i = 0; i < length; ++i){
    double x1 = grounds[i].getX();
    double y1 = grounds[i].getY();
    if((fabs(x1 - x2) < 0.1) && (fabs(y1 - y2) < 0.1)){
      Shared::Position position(x2,y2);
      towers.push_back(position);
      return true;
    }
  }
  return false;
}

bool ClientMap::insert_projectils_on(const Shared::Point& point){
  std::unique_lock<std::mutex> locker(mu);

  Shared::Position position(point);
  projectils.push_back(position);
  return true;
}

const vector<Shared::Position> & ClientMap::get_portals_in(){
  std::unique_lock<std::mutex> locker(mu);
  return portalsIn;
}

const vector<Shared::Position> & ClientMap::get_portals_out(){
  std::unique_lock<std::mutex> locker(mu);
  return portalsOut;
}

const vector<Shared::Position> & ClientMap::get_paths(){
  std::unique_lock<std::mutex> locker(mu);
  return paths;
}

const vector<Shared::Position> & ClientMap::get_towers(){
  std::unique_lock<std::mutex> locker(mu);
  return towers;
}

const vector<Shared::Position> & ClientMap::get_grounds(){
  std::unique_lock<std::mutex> locker(mu);
  return grounds;
}
const vector<Shared::Position> & ClientMap::get_projectils(){
  std::unique_lock<std::mutex> locker(mu);
  return projectils;
}

std::map<unsigned int, Shared::Position>& ClientMap::getEnemies(){
  std::unique_lock<std::mutex> locker(mu);
  return enemies;
}

// Pasarlo al ingles
string ClientMap::getScenarioBackgroundImagePath(){
  std::unique_lock<std::mutex> locker(mu);
  if(scenarioType == MEADOW_TYPE){
    return MEADOW_PATH;
  }else{
    return LAVA_PATH;
  }
}

string ClientMap::getScenarioGroundImagePath(){
  std::unique_lock<std::mutex> locker(mu);
  if(scenarioType == MEADOW_TYPE){
    return MEADOW_GROUND_PATH;
  }else{
    return LAVA_GROUND_PATH;
  }
}


string ClientMap::getScenarioPathImagePath(){
  std::unique_lock<std::mutex> locker(mu);
  if(scenarioType == MEADOW_TYPE){
    return MEADOW_PATH_PATH;
  }else{
    return LAVA_PATH_PATH;
  }
}
