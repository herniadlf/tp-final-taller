#ifndef __MESSAGE_HANDLER_H__
#define __MESSAGE_HANDLER_H__
#include <map>
#include "executor/executor.h"
#include "executor/tower_executor.h"
#include "executor/spell_executor.h"
#include "executor/movement_executor.h"
#include "executor/election_executor.h"
#include "executor/finish_executor.h"

class MessageHandler{
private:
	std::map<const char,Executor::Executor*> handler;
public:
	MessageHandler();
	~MessageHandler();
	void insert(const char key,Executor::Executor* value);
	Executor::Executor* get(const char& key);
};

#endif
