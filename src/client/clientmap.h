#ifndef __CLIENT_MAP_H__
#define __CLIENT_MAP_H__
#include <vector>
#include <string>
#include <map>
#include <utility>
#include "../shared/map/position.h"
using std::vector;
using std::string;
using std::map;
#include <mutex>

class ClientMap{
private:
  unsigned int mapHeight, mapWidth;
  std::vector<Shared::Position> portalsIn;
  std::vector<Shared::Position> portalsOut;
  std::vector<Shared::Position> paths;
  std::vector<Shared::Position> grounds;
  std::vector<Shared::Position> towers;
  std::vector<Shared::Position> projectils;
  std::map<unsigned int, Shared::Position> enemies;
  int scenarioType;
  size_t matchId;
  std::mutex mu;

public:
  ClientMap();
  void loadMap(const std::string& yamlPath);
  ~ClientMap();
  bool insert_tower_on(const Shared::Point& point);
  bool insert_projectils_on(const Shared::Point& point);
  unsigned int getMapHeight();
  unsigned int getMapWidth();
  const vector<Shared::Position> & get_portals_in();
  const vector<Shared::Position> & get_portals_out();
  const vector<Shared::Position> & get_paths();
  const vector<Shared::Position> & get_towers();
  const vector<Shared::Position> & get_grounds();
  const vector<Shared::Position> & get_projectils();
  std::map<unsigned int, Shared::Position>& getEnemies();
  string getScenarioBackgroundImagePath();
  string getScenarioGroundImagePath();
  string getScenarioPathImagePath();
};

#endif
