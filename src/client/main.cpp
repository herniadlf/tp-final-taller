#include <gtkmm/window.h>
#include <gtkmm/application.h>
#include "client.h"
#include "../shared/thread/thread.h"

int main(int argc, char *argv[])
{
	Client cli;
	int result = EXIT_SUCCESS;
	if (!cli.start())
		result =EXIT_FAILURE;
	return result;
}
