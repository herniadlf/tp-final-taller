#include "output.h"

using namespace client;

Output::Output(Shared::Socket& socket, Shared::LockedQueue& queue, bool& connected) :
			socket(socket), queue(queue), connected(connected) {}
Output::~Output() {}
void Output::run() {
	Shared::Sender sender(socket);
	while (this->connected && this->queue.hasWork()){
		const YAML::Node* yamlMessage = this->queue.pop();
		if (yamlMessage != NULL){
			const char* msg = Shared::Conversor::yamlToMsg(yamlMessage);
			size_t size = Shared::Conversor::yamlGetSize(yamlMessage);
			sender.doSend(msg, size);
			delete[] msg;
			delete yamlMessage;
		}
 	}
}
