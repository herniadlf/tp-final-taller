#include "tower_executor.h"

using namespace Executor;

Tower::Tower() {}

void Tower::doExecute(Game& game, const YAML::Node* message){
	const YAML::Node node = *message;
	auto function = node.begin();
	std::string functionHeader = function->first.as<std::string>();
	if (functionHeader == NEW_TOWER_COMMAND){
		int result = node["return"].as<int>();
		if (result==0){
			unsigned int id = node["id"].as<unsigned int>();
			YAML::Node arguments = node[functionHeader];
			unsigned int x = arguments[0].as<unsigned int>();
			unsigned int y = arguments[1].as<unsigned int>();
			game.addTower(x,y,id);
		} else {
			// NOTIFICAR QUE NO SE PUDO
		}
	} else if (functionHeader == INFO_TOWER_COMMAND){
		YAML::Node infoNode = node[INFO_TOWER_COMMAND];
		Shared::TowerInfo* towerInfo = this->readTowerInfo(infoNode);
		game.updateTowerInfo(towerInfo);
	}
}

Shared::TowerInfo* Tower::readTowerInfo(const YAML::Node& info) {

	size_t level = info[Shared::TowerInfo::LEVEL_LABEL].as<unsigned int>();
	size_t shootingRatio = info[Shared::TowerInfo::SHOOTING_RATIO_LABEL].as<unsigned int>();
	size_t hittingPoints = info[Shared::TowerInfo::HITTING_POINTS_LABEL].as<unsigned int>();
	size_t coolDown = info[Shared::TowerInfo::COOLDOWN_LABEL].as<unsigned int>();
	size_t actualXp = info[Shared::TowerInfo::ACTUAL_XP_LABEL].as<unsigned int>();
	size_t upgradeXp = info[Shared::TowerInfo::UPGRADE_XP_LABEL].as<unsigned int>();
	size_t validation = info[Shared::TowerInfo::VALIDATION_LABEL].as<unsigned int>();
	Shared::TowerInfo *towerInfo = new Shared::TowerInfo();
	towerInfo->addLevel(level);
	towerInfo->addShootingRatio(shootingRatio);
	towerInfo->addHittingPoints(hittingPoints);
	towerInfo->addCoolDown(coolDown);
	towerInfo->addActualXp(actualXp);
	towerInfo->addUpgradeXp(upgradeXp);
	towerInfo->addValidation(validation);
	return towerInfo;
}