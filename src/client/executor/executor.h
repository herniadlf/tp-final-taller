#ifndef __EXECUTOR_H__
#define __EXECUTOR_H__

#include <map>
#include "../game.h"
#include "yaml-cpp/yaml.h"
#include "../../shared/map/tower_info.h"
#include <cstring>

namespace Executor{

	class Executor{
	public:
		Executor();
		virtual ~Executor();
		virtual void doExecute(Game& game,const YAML::Node* message) = 0;
	};
}

#endif
