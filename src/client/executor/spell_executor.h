#ifndef __SPELL_EXECUTOR_H__
#define __SPELL_EXECUTOR_H__

#include "executor.h"

#define NEW_SPELL_COMMAND "Snew-spell"

namespace Executor{
	class Spell: public Executor{
	public:
		Spell();
		void doExecute(Game& game,const YAML::Node* message) override;
	};
}

#endif