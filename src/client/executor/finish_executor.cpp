#include "finish_executor.h"

using namespace Executor;

Finish::Finish() {}

void Finish::doExecute(Game& game, const YAML::Node* message){
	const YAML::Node node = *message;
	std::string functionHeader = node.begin()->first.as<std::string>();
	if (functionHeader == FINISH_WON_COMMAND){
		game.setResult(true);
	} else if (functionHeader == FINISH_LOOSE_COMMAND){
		game.setResult(false);
	}
	game.notifyEndOfGame();
}