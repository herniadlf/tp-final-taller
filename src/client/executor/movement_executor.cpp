#include "movement_executor.h"
using namespace Executor;

Movement::Movement() {}

void Movement::doExecute(Game& game, const YAML::Node* message){
	const YAML::Node node = *message;
	std::string functionHeader = node.begin()->first.as<std::string>();
	YAML::Node arguments = node[functionHeader];
	unsigned int objectId = arguments[0].as<unsigned int>();
	unsigned int x = arguments[1].as<unsigned int>();
	unsigned int y = arguments[2].as<unsigned int>();
	unsigned int kindAux = arguments[3].as<unsigned int>();
	Shared::GAME_OBJECT kind = static_cast<Shared::GAME_OBJECT>(kindAux);
	Shared::ENEMY_KIND enemyKind = Shared::ENEMY_KIND::ABMONIBLE;
	if (kind == Shared::GAME_OBJECT::ENEMY){
		unsigned int enemyKindAux = arguments[4].as<unsigned int>();
		enemyKind = static_cast<Shared::ENEMY_KIND>(enemyKindAux);
	}
	if (functionHeader == MOVE_COMMAND){
		game.moveObject(objectId, x, y, kind, enemyKind);
	} else if (functionHeader == CLEAN_COMMAND){
		game.cleanObject(objectId, x, y, kind, enemyKind);
	}
}