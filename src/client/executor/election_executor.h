#ifndef __ELECTION_EXECUTOR_H__
#define __ELECTION_EXECUTOR_H__

#include "executor.h"

#define ELECTION_CREATE_COMMAND "Ecreate"
#define ELECTION_JOIN_COMMAND "Ejoin"
#define ELECTION_CREATE_MAP_COMMAND "Ecreate-map"
#define ELECTION_LIST_MAPS_COMMAND "Elist-map"
#define ELECTION_LIST_MATCHS_COMMAND "Elist-match"
#define ELECTION_START_COMMAND "Estart"
#define ELECTION_CANCEL_CREATE_COMMAND "Ecancel-create"
#define ELECTION_CANCEL_JOIN_COMMAND "Ecancel-join"

namespace Executor{
	class Election: public Executor{
	public:
		Election();
		void doExecute(Game& game,const YAML::Node* message) override;
	};
}

#endif
