#ifndef __FINISH_EXECUTOR_H__
#define __FINISH_EXECUTOR_H__

#include "executor.h"

#define FINISH_WON_COMMAND "FWon"
#define FINISH_LOOSE_COMMAND "FLoose"

namespace Executor{
	class Finish: public Executor{
	public:
		Finish();
		void doExecute(Game& game,const YAML::Node* message) override;
	};
}

#endif