#include "spell_executor.h"
using namespace Executor;

Spell::Spell() {}

void Spell::doExecute(Game& game, const YAML::Node* message){
	const YAML::Node node = *message;
	std::string functionHeader = node.begin()->first.as<std::string>();
	if (functionHeader == NEW_SPELL_COMMAND){
		int result = node["return"].as<int>();
		if (result==0){
			YAML::Node arguments = node[functionHeader];
			unsigned int x = arguments[0].as<unsigned int>();
			unsigned int y = arguments[1].as<unsigned int>();
			game.addSpell(x,y);
		}
		else {
			// NOTIFICAR QUE NO SE PUDO
		}
	}
}