#ifndef __MOVEMENT_EXECUTOR_H__
#define __MOVEMENT_EXECUTOR_H__

#include "executor.h"

#define MOVE_COMMAND "Mmove-object"
#define CLEAN_COMMAND "Mclean-object"

namespace Executor{
	class Movement: public Executor{
	public:
		Movement();
		void doExecute(Game& game,const YAML::Node* message) override;
	};
}

#endif