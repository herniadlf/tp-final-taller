#ifndef __TOWER_EXECUTOR_H__
#define __TOWER_EXECUTOR_H__

#include "executor.h"

#define NEW_TOWER_COMMAND "Tnew-tower"
#define UPGRADE_TOWER_COMMAND "Tupgrade-tower"
#define INFO_TOWER_COMMAND "Tinfo-tower"


namespace Executor{
	class Tower: public Executor{
	private:
		Shared::TowerInfo* readTowerInfo(const YAML::Node& info);
	public:
		Tower();
		void doExecute(Game& game,const YAML::Node* message) override;
	};
}

#endif