#include "election_executor.h"
#include <iostream> // DELETE

using namespace Executor;

Election::Election() {}

void Election::doExecute(Game& game, const YAML::Node* message){
	const YAML::Node node = *message;
	std::string functionHeader = node.begin()->first.as<std::string>();
	
	if (functionHeader == ELECTION_CREATE_MAP_COMMAND){
		std::string yamlMap = node[functionHeader].as<std::string>();
		size_t matchId = node["id"].as<unsigned int>();
		game.loadMap(yamlMap);
		game.setMatchId(matchId);

	} else if (functionHeader == ELECTION_LIST_MAPS_COMMAND){
		YAML::Node mapVector = node[functionHeader];
		std::vector<std::string> mapFiles;
		for (auto it=mapVector.begin(); it!=mapVector.end(); ++it){
			std::string map = it->as<std::string>();
			mapFiles.push_back(map);
		}
		game.loadMapList(mapFiles);

	} else if (functionHeader == ELECTION_LIST_MATCHS_COMMAND){
		YAML::Node matchVector = node[functionHeader];
		std::vector<idScenarioTuple> auxVector;
		for (auto it=matchVector.begin(); it!=matchVector.end(); ++it){
			YAML::Node tupleNode = *it;
			size_t id 		= tupleNode["id"].as<unsigned int>();
			int scenario 	= tupleNode["scenario"].as<int>();
			idScenarioTuple tuple(id,scenario);
			auxVector.push_back(tuple);
		}
		game.loadMatchList(auxVector);

	} else if (functionHeader == ELECTION_START_COMMAND){
		game.start();

	} else if (functionHeader == ELECTION_CANCEL_CREATE_COMMAND){
		game.cancelCreateGame();

	} else if(functionHeader == ELECTION_CANCEL_JOIN_COMMAND){
		game.cancelJoinGame();
	}
}
