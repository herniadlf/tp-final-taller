#include "controller.h"
#include "drawable/windows.h"
#include <utility>
#include <sstream>
#define TILE_WIDTH 158
#define TILE_HEIGHT 79

Game::Game() : drawableMap(NULL),started(false),gameOn(true),
					result(true),window(NULL), towerInfo(NULL), matchId(0){}

Game::~Game() {
	if (towerInfo != NULL)
		delete towerInfo;
}


void Game::addTower(unsigned int x, unsigned int y, unsigned int id){
	if(drawableMap){
		Shared::Point point(x,y);
		logicalMap.insert_tower_on(point);
		drawableMap->addTower(id, point);
	}
}

void Game::updateTowerInfo(Shared::TowerInfo* towerInfo){
	if (this->towerInfo != NULL)
		delete this->towerInfo;
	this->towerInfo = towerInfo;
	if(isTowerInfoValid()){
		if(window)
			window->notify();
	}
}


void Game::addSpell(unsigned int x, unsigned int y){
	Shared::Point point(x,y);
	logicalMap.insert_projectils_on(point);
}

void Game::assignDrawableMap(DrawableMap * thisDrawableMap){
	drawableMap = thisDrawableMap;
}

void Game::moveObject(unsigned int id,
						unsigned int x,
						unsigned int y,
						Shared::GAME_OBJECT kind,
						Shared::ENEMY_KIND enemyKind){
	//busca al objeto en la lista, si no esta, lo crea. si esta, lo mueve
	if(drawableMap){
		drawableMap->movePositionObject(id, x, y, kind, enemyKind);
	}
}

void Game::cleanObject(unsigned int id,
						unsigned int x,
						unsigned int y,
						Shared::GAME_OBJECT kind,
						Shared::ENEMY_KIND enemyKind){
	if(drawableMap)
		drawableMap->cleanObject(id, x, y, kind, enemyKind);
}

void Game::loadMap(const std::string& yamlMap) {
	drawableMap->clearMap();
	this->logicalMap.loadMap(yamlMap);
	drawableMap->load();
}

void Game::loadMapList(const std::vector<std::string>& mapFiles){
	this->mapList.clear();
	for (std::string map: mapFiles)
		this->mapList.push_back(map);
		if(window)
			window->notify();
}

void Game::loadMatchList(const std::vector<idScenarioTuple>& matchList){
	this->matchList.clear();
	for (idScenarioTuple tuple: matchList)
		this->matchList.push_back(tuple);
	if(window)
		window->notify();
}
void Game::start() {
	this->started = true;
	gameOn=true;
	if(this->window){
		window->notify();
	}
}

ClientMap& Game::getLogicalMap(){
	return logicalMap;
}

void Game::setWindow(IWindow* window) {
	std::unique_lock<std::mutex> locker(mu);
	this->window = window;
}

bool& Game::getResult(){
	return result;
}

void Game::setResult(bool gameResult){
	gameOn = false;
	result = gameResult;
}


bool& Game::isGameOn(){
	return gameOn;
}

void Game::notifyEndOfGame(){
	matchId = 0;
	if(this->window){
		window->notify();
	}
}

string Game::getTowerInfo(){
	std::stringstream info_st;
	info_st << *(towerInfo->getInfo());
	string info = info_st.str();
	return info;
}

bool Game::isTowerInfoValid(){
	std::unique_lock<std::mutex> locker(mu);
	if(towerInfo){
		return towerInfo->isValid();
	}
	return false;
}

std::vector<std::string>& Game::getMapsVector(){
	return mapList;
}

std::vector<idScenarioTuple>& Game::getMatchesVector(){
	return matchList;
}

std::string& Game::getMapNameAt(int index){
	return mapList[index];
}

void Game::setMatchId(size_t id){
	matchId = id;
}

size_t Game::getMatchId(){
	return matchId;
}

bool Game::isMatchValid(){
	return !(matchId == 0);
}

void Game::cancelCreateGame(){
	matchId = 0;
	if(window)
		window->notify();
}

void Game::cancelJoinGame(){
	matchId = 0;
	if(window)
		window->notify();
}
