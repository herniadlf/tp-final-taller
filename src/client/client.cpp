#include "client.h"
#include <string>
#include <fstream>
#include <iostream>
#include "controller.h"
#include "yaml-cpp/yaml.h"
#include "drawable/gameapplication.h"

#define CONFIG_PATH "config/config.yaml"

Client::Client() {
	YAML::Node clientConfig = YAML::LoadFile(CONFIG_PATH);
	this->host = clientConfig["host"].as<std::string>();
	this->port = clientConfig["port"].as<std::string>();
}
Client::~Client() {}

bool Client::start(){
	int result = this->socket.s_connect(this->host.data(),this->port.data());
	if (result != 0)
		return false;
	Game game;
	// BEGIN-GAME
	client::Controller controller(this->socket,game);
	controller.start();
	auto app = Gtk::Application::create();
	App a(app, controller);
	a.start();
	a.join();
	this->end();
	controller.join();
	return true;
}

bool Client::end(){
	int result = this->socket.s_shutdown(SHUT_RDWR);
	if (result != 0)
		return false;
	return true;
}
