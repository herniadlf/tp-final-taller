#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__
#include "../shared/thread/thread.h"
#include "../shared/socket/socket.h"
#include "../shared/communication/locked_queue.h"
#include "../shared/map/point.h"
#include "message_handler.h"
#include <string>

#define TOWER_COMMAND_CODE 'T'
#define SPELL_COMMAND_CODE 'S'
#define MOVEMENT_COMMAND_CODE 'M'
#define ELECTION_COMMAND_CODE 'E'
#define FINISH_COMMAND_CODE 'F'

namespace client{
	class Controller: public Thread{
	private:
		Shared::Socket& socket;
		Game& game;
		bool connected;
		Shared::LockedQueue inputQueue;
		Shared::LockedQueue outputQueue;
		MessageHandler messageHandler;
	public:
		Controller(Shared::Socket& socket,Game& game);
		~Controller();
		bool isConnected() const;
		void requestTower(const size_t x, const size_t y);
		void requestTowerInfo(const size_t x, const size_t y);
		void requestUpgrade(const size_t x, const size_t y);
		void requestSpell(const size_t x, const size_t y);
		void requestMapList();
		void requestMatchList();
		void createNewGame(int mapIndex);
		void joinGame(int matchIndex);
		void startGame();
		void run() override;
		void closeQueues();
		Game& getGame();
		void cancelGameCreation(size_t matchId);
		void cancelGameJoin(size_t matchId);

	};
}

#endif
