#include "input.h"

using namespace client;

Input::Input(Shared::Socket& socket, Shared::LockedQueue& queue, bool& connected) :
			socket(socket), queue(queue), connected(connected) {}
Input::~Input() {}
void Input::run() {
	Shared::Receiver receiver(socket);
	while (this->connected && this->queue.hasWork()){
		receiver.prepareSize();
		if (receiver.getSize() > 0 && receiver.read()){
			const YAML::Node * msg = Shared::Conversor::msgToYaml(receiver.msg(), receiver.getSize());
			const Shared::yaml_iterator begin = msg->begin();
			const Shared::yaml_iterator end = msg->end();
			for (Shared::yaml_iterator it=begin; it!=end; ++it){
				const YAML::Node *node = new YAML::Node(*it);
				this->queue.push(node);
			}
			receiver.clean();
			delete msg;
		} else {
			this->connected = false;
			this->queue.setFinished();
		}
	}
}
