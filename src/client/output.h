#ifndef __OUTPUT_H__
#define __OUTPUT_H__
#include "../shared/thread/thread.h"
#include "../shared/socket/socket.h"
#include "../shared/communication/locked_queue.h"
#include "../shared/communication/sender.h"
#include <string>

namespace client{
	class Output: public Thread{
	private:
		Shared::Socket& socket;
		Shared::LockedQueue& queue;
		bool& connected;
	public:
		Output(Shared::Socket& socket, Shared::LockedQueue& queue, bool& connected);
		~Output();
		void run() override;
	};
}

#endif