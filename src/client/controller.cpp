#include "controller.h"
#include "output.h"
#include "input.h"
#include <string>
using namespace client;

Controller::Controller(Shared::Socket& socket, Game& game) :
		socket(socket), game(game), connected(true)
{
	Executor::Executor *tower = new Executor::Tower();
	this->messageHandler.insert(TOWER_COMMAND_CODE, tower);
	Executor::Executor *spell = new Executor::Spell();
	this->messageHandler.insert(SPELL_COMMAND_CODE, spell);
	Executor::Executor *movement = new Executor::Movement();
	this->messageHandler.insert(MOVEMENT_COMMAND_CODE, movement);
	Executor::Executor *election = new Executor::Election();
	this->messageHandler.insert(ELECTION_COMMAND_CODE, election);
	Executor::Executor *finish = new Executor::Finish();
	this->messageHandler.insert(FINISH_COMMAND_CODE, finish);
}

Controller::~Controller() {}

bool Controller::isConnected() const{
	return this->connected;
}


void Controller::run(){
	Input serverInput(this->socket, inputQueue, this->connected);
	serverInput.start();
	Output serverOutput(this->socket, outputQueue, this->connected);
	serverOutput.start();
	while (this->connected && this->inputQueue.hasWork()){
		const YAML::Node *toProcess = this->inputQueue.pop();
		if (toProcess != NULL){
			std::string function = toProcess->begin()->first.as<std::string>();
			const char& code = function[0];
			Executor::Executor* executor = this->messageHandler.get(code);
			executor->doExecute(this->game,toProcess);
			delete toProcess;
		}
	}
	this->closeQueues();
	serverInput.join();
	serverOutput.join();
}

void Controller::closeQueues(){
	if (connected){
		this->inputQueue.setFinished();
		this->outputQueue.setFinished();
		this->connected = false;
	} else{
		this->outputQueue.setFinished();
	}
	game.notifyEndOfGame();
}

void Controller::requestTower(const size_t x, const size_t y) {
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(x);
	arguments.push_back(y);
	(*command)[NEW_TOWER_COMMAND] = arguments;
	this->outputQueue.push(command);
}

void Controller::requestTowerInfo(const size_t x, const size_t y){
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(x);
	arguments.push_back(y);
	(*command)[INFO_TOWER_COMMAND] = arguments;
	this->outputQueue.push(command);
}

void Controller::requestUpgrade(const size_t x, const size_t y) {
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(x);
	arguments.push_back(y);
	(*command)[UPGRADE_TOWER_COMMAND] = arguments;
	this->outputQueue.push(command);
}

void Controller::requestSpell(const size_t x, const size_t y) {
	YAML::Node* command = new YAML::Node();
	YAML::Node arguments;
	arguments.push_back(x);
	arguments.push_back(y);
	(*command)[NEW_SPELL_COMMAND] = arguments;
	this->outputQueue.push(command);
}

void Controller::requestMapList(){
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_LIST_MAPS_COMMAND] = 0;
	this->outputQueue.push(command);
}

void Controller::requestMatchList(){
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_LIST_MATCHS_COMMAND] = 0;
	this->outputQueue.push(command);
}

Game& Controller::getGame(){
	return game;
}

void Controller::createNewGame(int mapIndex) {
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_CREATE_COMMAND] = mapIndex;
	this->outputQueue.push(command);
}

void Controller::joinGame(int matchIndex){
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_JOIN_COMMAND] = matchIndex;
	this->outputQueue.push(command);
}

void Controller::startGame() {
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_START_COMMAND] = 0;
	this->outputQueue.push(command);
}

void Controller::cancelGameCreation(size_t matchId){
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_CANCEL_CREATE_COMMAND] = matchId;
	this->outputQueue.push(command);
}

void Controller::cancelGameJoin(size_t matchId){
	YAML::Node* command = new YAML::Node();
	(*command)[ELECTION_CANCEL_JOIN_COMMAND] = matchId;
	this->outputQueue.push(command);
}
