#ifndef __GAME_H__
#define __GAME_H__
#include "drawable/drawablemap.h"
#include "../shared/map/game_object.h"
#include "../shared/map/tower_info.h"
#include "clientmap.h"
#include <mutex>

class IWindow;
typedef std::tuple<size_t,int> idScenarioTuple;

class Game{
private:
	DrawableMap * drawableMap;
	ClientMap logicalMap;
	bool started;
	bool gameOn;
	bool result;
	IWindow* window;
	Shared::TowerInfo* towerInfo;
	std::mutex mu;
	size_t matchId;
	std::vector<std::string> mapList;
	std::vector<idScenarioTuple> matchList;
public:
	Game();
	~Game();
	void addTower(unsigned int x, unsigned int y, unsigned int id);
	void updateTowerInfo(Shared::TowerInfo* towerInfo);
	void addSpell(unsigned int x, unsigned int y);
	void assignDrawableMap(DrawableMap * theDrawableMap);
	void moveObject(unsigned int id,
				unsigned int x,
				unsigned int y,
				Shared::GAME_OBJECT kind,
				Shared::ENEMY_KIND enemyKind);
	void cleanObject(unsigned int id,
				unsigned int x,
				unsigned int y,
				Shared::GAME_OBJECT kind,
				Shared::ENEMY_KIND enemyKind);
	void loadMap(const std::string& yamlMap);
	void loadMapList(const std::vector<std::string>& mapFiles);
	void loadMatchList(const std::vector<idScenarioTuple>& matchList);
	void start();
	ClientMap& getLogicalMap();
	void setWindow(IWindow* window);
	bool& getResult();
	string getTowerInfo();
	bool& isGameOn();
	void setResult(bool gameResult);
	void notifyEndOfGame();
	bool isTowerInfoValid();
	std::vector<std::string>& getMapsVector();
	std::vector<idScenarioTuple>& getMatchesVector();
	std::string& getMapNameAt(int index);
	void setMatchId(size_t id);
	size_t getMatchId();
	bool isMatchValid();
	void cancelCreateGame();
	void cancelJoinGame();
};
#endif
