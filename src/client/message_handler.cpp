#include "message_handler.h"

MessageHandler::MessageHandler() {}
MessageHandler::~MessageHandler() {
	auto messagesBegin = this->handler.begin();
	auto messagesEnd = this->handler.end();
	for (auto it=messagesBegin; it!=messagesEnd; ++it)
		delete it->second;
}

void MessageHandler::insert(const char key,Executor::Executor* value){
	this->handler.insert(std::pair<const char, Executor::Executor*>(key,value));
}

Executor::Executor* MessageHandler::get(const char& key) {
	return handler[key];
}

