cmake_minimum_required (VERSION 3.5)

set(CMAKE_CXX_FLAGS "-Wall -Werror -pedantic -pedantic-errors -O3 -ggdb -DDEBUG -fno-inline -std=c++11")
set(CMAKE_EXE_LINKER_FLAGS "-pthread")

find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0) 

add_subdirectory(client) 
add_subdirectory(server) 
add_subdirectory(shared) 
add_subdirectory(editor) 
