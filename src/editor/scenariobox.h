#ifndef GTKMM_SCENARIO_BOX_H
#define GTKMM_SCENARIO_BOX_H
#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/grid.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/adjustment.h>
#include "scenariodrawingarea.h"
#include <vector>
#include <tuple>
using std::tuple;
using std::vector;


class ScenarioBox : public Gtk::Box{
public:
  ScenarioBox(std::vector<Shared::Point*>& points, Gtk::Button &addCircuitButton);
  ~ScenarioBox();
  void resizeScenarioArea(unsigned int rows, unsigned int cols);
private:
  ScenarioDrawingArea scenarioDrawingArea;
  Gtk::Frame buttonsFrame, scenarioFrame;
  Gtk::Box buttonsHBox, scenarioVBox;
  Gtk::ScrolledWindow scenarioScrolledWindow;
  Gtk::Grid scenarioGrid;
  Gtk::Button eraseButton;
};

#endif
