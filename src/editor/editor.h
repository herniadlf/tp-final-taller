#ifndef EDITOR_H
#define EDITOR_H
#include "horde.h"
#include "circuit.h"
#include <string>
using std::string;
#include <vector>
using std::vector;

class Editor{
public:
  Editor();
  ~Editor();

  std::vector<Shared::Point*>& getCircuitVector();
  size_t& getEnemy1Quantity();
  size_t& getCoolDown();
  void printContent(string filename, int scenarioType);
  bool isValidationOkay();
  void setMapDimensions(size_t thisWidth, size_t thisHeight);
  void addHorde(int enemyType);
  void addCircuit();
  bool validateCircuit();
private:
//  HordDescription actualHord;
  Horde actualHorde;
  Circuit actualCircuit;
  std::vector<Horde*> hordes;
  std::vector<Circuit*> circuits;
  std::vector<Shared::Point*> portalsIn, portalsOut, totalPoints, paths, grounds;
  size_t width, height;
  void addPortals();
  void addGrounds();
  Shared::Point * calculateGround(size_t index);
  void calculateWidthHeight();
};

#endif
