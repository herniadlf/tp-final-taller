#ifndef GTKMM_DRAWINGAREA_H
#define GTKMM_DRAWINGAREA_H
#include "../shared/map/point.h"
#include <gtkmm/drawingarea.h>
#include <vector>


class ScenarioDrawingArea: public Gtk::DrawingArea {
    public:
      ScenarioDrawingArea(std::vector<Shared::Point*>& points);
      ~ScenarioDrawingArea();
      void resize(unsigned int rows, unsigned int cols);
      void onEraseButton();
    protected:
      bool on_timeout();
      virtual bool on_draw(const Cairo::RefPtr < Cairo::Context > & cr);
      bool on_motion_notify_event(GdkEventMotion *event);
      bool on_button_press_event(GdkEventButton *event);
    private:
      unsigned int pixelsWidth, pixelsHeight, rowQ, colQ;
      std::vector<Shared::Point*>& points;
      void draw_grid(const Cairo::RefPtr<Cairo::Context>& cr);
      float calculate_coordenate_x(float coord);
      float calculate_coordenate_y(float coord);
};
#endif // DRAWINGAREA_H
