#include "horde.h"

Horde::Horde() : enemyType(MONSTER1), enemyQ(1), coolDown(1)
{}

Horde::Horde(int enemyType, size_t enemyQ, size_t coolDown) :
      enemyType(enemyType),
      enemyQ(enemyQ),
      coolDown(coolDown)
{}

Horde::~Horde(){}

size_t& Horde::getEnemyQuantity(){
  return enemyQ;
}

size_t& Horde::getCoolDown(){
  return coolDown;
}

std::string Horde::getEnemyType(){
  if(enemyType == 0)
    return "Enemy1";

  return "Enemy2";
}

void Horde::reset(){
  enemyType = MONSTER1;
  enemyQ = 1;
  coolDown = 1;
}
