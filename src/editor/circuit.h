#ifndef CIRCUIT_H
#define CIRCUIT_H

#include "../shared/map/point.h"
#include <cstring>
#include <vector>
using std::vector;

class Circuit{
public:
  Circuit();
  ~Circuit();
  std::vector<Shared::Point*> &getCircuit();
  bool validateCircuit();

private:
  std::vector<Shared::Point*> circuit;
};

#endif
