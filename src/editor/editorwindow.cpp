#include "editorwindow.h"
#include <iostream>
#define DEFAULT_MAP_SIZE 10

EditorWindow::EditorWindow() :
  editor(),
  colsAdjustment(Gtk::Adjustment::create(DEFAULT_MAP_SIZE, 1.0, 20.0, 1.0, 5.0, 0.0)),
  rowsAdjustment(Gtk::Adjustment::create(DEFAULT_MAP_SIZE, 1.0, 20.0, 1.0, 5.0, 0.0)),
  rowsSpinButton(rowsAdjustment),
  colsSpinButton(colsAdjustment),
  mainVBox(Gtk::ORIENTATION_VERTICAL),
  mainHBox(Gtk::ORIENTATION_HORIZONTAL),
  saveHBox(Gtk::ORIENTATION_HORIZONTAL),
  addHordeButton("Add"),
  addCircuitButton("Add"),
  saveButton("Save"),
  scenarioPage(rowsSpinButton, colsSpinButton),
  hordasPage(editor.getEnemy1Quantity(),
             editor.getCoolDown(),
             addHordeButton),
  scenarioBox(editor.getCircuitVector(),
             addCircuitButton)

  {
    set_title("Editor");
    set_border_width(10);
    set_default_size(600, 400);
    add(mainVBox);

    editorNotebook.set_border_width(10);
    mainVBox.pack_start(editorNotebook);
    editorNotebook.append_page(scenarioPage, "Escenario");
    editorNotebook.append_page(hordasPage, "Hordas");
    editorNotebook.append_page(scenarioBox, "Circuito");

    mainVBox.pack_start(saveHBox, Gtk::PACK_SHRINK);
    saveHBox.pack_start(saveButton);
    saveHBox.set_halign(Gtk::ALIGN_END);
    saveHBox.set_valign(Gtk::ALIGN_END);

    rowsSpinButton.signal_value_changed().connect(sigc::mem_fun(*this,
                              &EditorWindow::onResizeButtons));
    colsSpinButton.signal_value_changed().connect(sigc::mem_fun(*this,
                              &EditorWindow::onResizeButtons));
    saveButton.signal_clicked().connect(sigc::mem_fun(*this,
                              &EditorWindow::onSaveButton));
    addHordeButton.signal_clicked().connect(sigc::mem_fun(*this,
                              &EditorWindow::onAddHordeButton));
    addCircuitButton.signal_clicked().connect(sigc::mem_fun(*this,
                              &EditorWindow::onAddCircuitButton));
    show_all_children();
  }

EditorWindow::~EditorWindow(){}

void EditorWindow::onResizeButtons(){
  size_t width = rowsSpinButton.get_value_as_int();
  size_t height = colsSpinButton.get_value_as_int();
  editor.setMapDimensions(height, width);
  scenarioBox.resizeScenarioArea(height, width);
}

void EditorWindow::onSaveButton(){
  if(editor.isValidationOkay()){
    string filename = scenarioPage.getFilename();
    int scenarioType = scenarioPage.getScenarioType();
    editor.printContent(filename, scenarioType);
    hide();
  }
}

void EditorWindow::onAddHordeButton(){
  int enemyType = hordasPage.getEnemyType();
  editor.addHorde(enemyType);
  hordasPage.resetButtons();
}

void EditorWindow::onAddCircuitButton(){
  if(editor.validateCircuit()){
    editor.addCircuit();
  }
}
