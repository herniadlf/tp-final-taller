#ifndef GTKMM_EDITORWINDOW_H
#define GTKMM_EDITORWINDOW_H
#include <gtkmm/window.h>
#include <gtkmm/notebook.h>
#include <gtkmm/box.h>
#include <gtkmm/buttonbox.h>
#include "scenariopage.h"
#include "hordaspage.h"
#include "scenariobox.h"
#include "editor.h"
#include <gtkmm/spinbutton.h>
#include <gtkmm/adjustment.h>

class EditorWindow : public Gtk::Window{
public:
  EditorWindow();
  ~EditorWindow();
protected:
  void onResizeButtons();
  void onSaveButton();
  void onAddHordeButton();
  void onAddCircuitButton();
private:
  Editor editor;
  Glib::RefPtr<Gtk::Adjustment> colsAdjustment, rowsAdjustment;
  Gtk::SpinButton rowsSpinButton, colsSpinButton;
  Gtk::Notebook editorNotebook;
  Gtk::Box mainVBox, mainHBox;
  Gtk::ButtonBox saveHBox;
  Gtk::Button addHordeButton, addCircuitButton, saveButton;
  ScenarioPage scenarioPage;
  HordasPage hordasPage;
  ScenarioBox scenarioBox;
};

#endif
