#include "hordaspage.h"

HordasPage::HordasPage(size_t &enemy1Q, size_t &coolDown, Gtk::Button &addHordButton) :
                      enemy1Q(enemy1Q),
                      coolDown(coolDown),
                      addHordButton(addHordButton),
                      timeFrame("CoolDown"),
                      enemyFrame("Enemy"),
                      timeHBox(Gtk::ORIENTATION_HORIZONTAL),
                      enemyVBox(Gtk::ORIENTATION_VERTICAL),
                      enemy1HBox(Gtk::ORIENTATION_HORIZONTAL),
                      enemyTypeHBox(Gtk::ORIENTATION_HORIZONTAL),
                      timeLabel("Time"),
                      enemy1Label("Enemy Quantity"),
                      enemyTypeLabel("Enemy Type"),
                      timeAdjustment(Gtk::Adjustment::create(1.0, 1.0, 20.0, 1.0, 5.0, 0.0)),
                      enemy1Adjustment(Gtk::Adjustment::create(1.0, 1.0, 20.0, 1.0, 5.0, 0.0)),
                      timeSpinButton(timeAdjustment),
                      enemy1SpinButton(enemy1Adjustment)
{
  set_orientation(Gtk::ORIENTATION_VERTICAL),

  pack_start(enemyFrame);

  enemyFrame.add(enemyVBox);
  enemyVBox.pack_start(enemyTypeHBox);
  enemyTypeHBox.pack_start(enemyTypeLabel);
  enemyTypeHBox.pack_start(enemyTypeComboBox);
  enemyTypeComboBox.append("Enemigo 1");
  enemyTypeComboBox.append("Enemigo 2");
  enemyTypeComboBox.set_active(0);

  enemyVBox.pack_start(enemy1HBox);
  enemy1HBox.pack_start(enemy1Label);
  enemy1HBox.pack_start(enemy1SpinButton);
  enemy1SpinButton.signal_value_changed().connect(sigc::mem_fun(*this, &HordasPage::onEnemy1Button));

  pack_start(timeFrame);
  timeFrame.add(timeHBox);
  timeHBox.pack_start(timeLabel);
  timeHBox.pack_start(timeSpinButton);
  timeSpinButton.signal_value_changed().connect(sigc::mem_fun(*this, &HordasPage::onTimeButton));

  pack_start(addHordButton);

  show_all_children();

}

HordasPage::~HordasPage(){}

void HordasPage::resetButtons(){
  enemyTypeComboBox.set_active(0);
  enemy1SpinButton.set_value(1);
  timeSpinButton.set_value(1);
}

void HordasPage::onTimeButton(){
  coolDown = timeSpinButton.get_value_as_int();
}

void HordasPage::onEnemy1Button(){
  enemy1Q = enemy1SpinButton.get_value_as_int();
}

int HordasPage::getEnemyType(){
  int i = enemyTypeComboBox.get_active_row_number();
  if (i == -1){
    return 0;
  }
  return i;
}
