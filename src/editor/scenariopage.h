#ifndef GTKMM_SCENARIO_PAGE_H
#define GTKMM_SCENARIO_PAGE_H
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/frame.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/spinbutton.h>
#include <string>

class ScenarioPage : public Gtk::Box{
public:
  ScenarioPage(Gtk::SpinButton &rowsSpinButton,
               Gtk::SpinButton &colsSpinButton);
  ~ScenarioPage();
  std::string getFilename();
  int getScenarioType();
private:
  Gtk::Box mapNameHBox, scenarioHBox, rowsHBox, colsHBox, dimensionsVBox;
  Gtk::Label mapNameLabel, scenarioLabel, rowLabel, colLabel;
  Gtk::Entry mapNameEntry;
  Gtk::Frame mapNameFrame, scenarioTypeFrame, dimensionsFrame;
  Gtk::ComboBoxText scenarioComboBox;
  Gtk::SpinButton &rowsSpinButton, &colsSpinButton;
};

#endif
