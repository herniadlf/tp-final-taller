#include "scenariopage.h"

ScenarioPage::ScenarioPage(Gtk::SpinButton &rowsSpinButton,
             Gtk::SpinButton &colsSpinButton) :
             mapNameHBox(Gtk::ORIENTATION_HORIZONTAL),
             scenarioHBox(Gtk::ORIENTATION_HORIZONTAL),
             rowsHBox(Gtk::ORIENTATION_HORIZONTAL),
             colsHBox(Gtk::ORIENTATION_HORIZONTAL),
             dimensionsVBox(Gtk::ORIENTATION_VERTICAL),
             mapNameLabel("Map Name: "),
             scenarioLabel("Scenario: "),
             rowLabel("Rows"),
             colLabel("Columns"),
             scenarioTypeFrame("Scenario Type"),
             dimensionsFrame("Map Dimensions"),
             rowsSpinButton(rowsSpinButton),
             colsSpinButton(colsSpinButton)
{
  set_orientation(Gtk::ORIENTATION_VERTICAL);
  pack_start(mapNameFrame);
  mapNameFrame.add(mapNameHBox);
  mapNameHBox.pack_start(mapNameLabel);
  mapNameHBox.pack_start(mapNameEntry);
  mapNameEntry.set_text("filename");

  pack_start(scenarioTypeFrame);
  scenarioTypeFrame.add(scenarioHBox);
  scenarioHBox.pack_start(scenarioLabel);
  scenarioHBox.pack_start(scenarioComboBox);

  scenarioComboBox.append("Pradera");
  scenarioComboBox.append("Volcano");
  scenarioComboBox.set_active(0);

  pack_start(dimensionsFrame);
  dimensionsFrame.add(dimensionsVBox);
  dimensionsVBox.pack_start(rowsHBox);
  dimensionsVBox.pack_start(colsHBox);
  rowsHBox.pack_start(rowLabel);
  rowsHBox.pack_start(rowsSpinButton);
  colsHBox.pack_start(colLabel);
  colsHBox.pack_start(colsSpinButton);

  show_all_children();
}

ScenarioPage::~ScenarioPage() {}

std::string ScenarioPage::getFilename(){
  return mapNameEntry.get_text();
}

int ScenarioPage::getScenarioType(){
  int i = scenarioComboBox.get_active_row_number();
  if(i == -1){
    return 0;
  }
  return i;
}
