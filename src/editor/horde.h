#ifndef HORDE_H
#define HORDE_H

#define MONSTER1 0
#define MONSTER2 1

#include <string>
#include <cstring>

class Horde{
public:
  Horde();
  Horde(int enemyType, size_t enemyQ, size_t coolDown);
  ~Horde();
  size_t& getEnemyQuantity();
  size_t& getCoolDown();
  std::string getEnemyType();
  void reset();

private:
  int enemyType;
  size_t enemyQ, coolDown;
};

#endif
