//g++ main.cpp editorwindow.cpp scenariobox.cpp scenariodrawingarea.cpp mapa.cpp point.cpp -o tp `pkg-config --cflags --libs gtkmm-3.0`
#include "editorwindow.h"
#include <gtkmm/application.h>

int main(int argc, char *argv[]){
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");
  EditorWindow window;
  return app->run(window);
}
