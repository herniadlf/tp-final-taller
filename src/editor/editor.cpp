#include "editor.h"
#include <iostream>
#include "yaml-cpp/yaml.h"
#include <fstream>
#define MAP_PATH "../server/maps/"
#define EXTENSION ".yaml"
#define MAP_LIST_FILE "../server/maps/available-maps.yaml"
#define SECONDS_UNIT 50

Editor::Editor() : width(10), height(10)
{}

Editor::~Editor(){
  for(size_t i = 0; i < totalPoints.size(); ++i){
    delete(totalPoints[i]);
  }
}

std::vector<Shared::Point*>& Editor::getCircuitVector(){
  return actualCircuit.getCircuit();
}

size_t& Editor::getEnemy1Quantity(){
  return actualHorde.getEnemyQuantity();
}

size_t& Editor::getCoolDown(){
  return actualHorde.getCoolDown();
}


bool Editor::isValidationOkay(){
  return(hordes.size() > 0 && circuits.size() > 0);
}

void Editor::setMapDimensions(size_t thisWidth, size_t thisHeight){
  width = thisWidth;
  height = thisHeight;
}

void Editor::addHorde(int enemyType){
  Horde * newHorde = new Horde(enemyType,
                               actualHorde.getEnemyQuantity(),
                               actualHorde.getCoolDown()*SECONDS_UNIT);
  hordes.push_back(newHorde);
  actualHorde.reset();
}


bool Editor::validateCircuit(){
  std::vector<Shared::Point*>& actualCircuitVector = actualCircuit.getCircuit();
  return (actualCircuitVector.size() > 0);
}

void Editor::addCircuit(){
  std::vector<Shared::Point*>& actualCircuitVector = actualCircuit.getCircuit();
  if (actualCircuitVector.size() > 0){
    addPortals();
    Circuit * newCircuit = new Circuit();
    std::vector<Shared::Point*>& newCircuitVector = newCircuit->getCircuit();
    while(!actualCircuitVector.empty()){
      Shared::Point * thisPoint = actualCircuitVector[0];
      newCircuitVector.push_back(thisPoint);
      if(!thisPoint->isInVector(totalPoints)){
        totalPoints.push_back(thisPoint);
        paths.push_back(thisPoint);
      }
      actualCircuitVector.erase(actualCircuitVector.begin());
    }
    circuits.push_back(newCircuit);
  }
}

void Editor::addPortals(){
  std::vector<Shared::Point*>& circuit = actualCircuit.getCircuit();
  Shared::Point* portalIn = circuit[0];
  if(!portalIn->isInVector(portalsIn)){
    portalsIn.push_back(portalIn);
  }

  Shared::Point* portalOut = circuit[circuit.size()-1];
  if (!portalOut->isInVector(portalsOut)){
    portalsOut.push_back(portalOut);
  }
}

void Editor::addGrounds(){
  std::vector<Shared::Point*>& circuit = paths;
  for(size_t i = 0; i < circuit.size(); ++i){
    Shared::Point* point = calculateGround(i);
    while(point != NULL){
      totalPoints.insert(totalPoints.end(), point);
      grounds.insert(grounds.end(), point);
      point = calculateGround(i);
    }
  }
}

Shared::Point * Editor::calculateGround(size_t index){
  std::vector<Shared::Point*>& circuit = paths;
  Shared::Point* point = circuit[index];
  size_t pointX = point->getX();
  size_t pointY = point->getY();
  Shared::Point point1(pointX+1, pointY);

  if(!point1.isInVector(totalPoints)){
    return new Shared::Point(pointX+1, pointY);
  }
  Shared::Point point2(pointX, pointY+1);
  if(!point2.isInVector(totalPoints)){
    return new Shared::Point(pointX, pointY+1);
  }
  if(pointX > 0){
    Shared::Point point3(pointX-1, pointY);
    if(!point3.isInVector(totalPoints)){
      return new Shared::Point(pointX-1, pointY);
    }
  }
  if(pointY > 0){
    Shared::Point point4(pointX, pointY-1);
    if(!point4.isInVector(totalPoints)){
      return new Shared::Point(pointX, pointY-1);
    }
  }
  return NULL;
}

void Editor::calculateWidthHeight(){
  for(size_t i = 0; i < totalPoints.size(); ++i){
    Shared::Point* thisPoint = totalPoints[i];
    size_t thisX = thisPoint->getX();
    size_t thisY = thisPoint->getY();
    if(thisX > width){
      width = thisX;
    }
    if(thisY > height){
      height = thisY;
    }
  }
}

void Editor::printContent(string filename, int scenarioType){
  YAML::Node map;
  map["scenario"] = scenarioType;
  map["width"] = width;
  map["height"] = height;

  YAML::Node circuitsList;
  for(size_t i = 0; i < circuits.size(); ++i){
    Circuit* thisCircuit = circuits[i];
    std::vector<Shared::Point*>& thisCircuitVector = thisCircuit->getCircuit();
    YAML::Node circuit;
    circuitsList.push_back(circuit);
    for(size_t j = 0; j < thisCircuitVector.size(); ++j){
      Shared::Point* point = thisCircuitVector[j];
      YAML::Node ypoint;
      ypoint.push_back(point->getX());
      ypoint.push_back(point->getY());
      circuit.push_back(ypoint);
    }
  }
  map["paths"] = circuitsList;

  addGrounds();
  YAML::Node groundsList;
  for(size_t i = 0; i < grounds.size(); ++i){
    Shared::Point* point = grounds[i];
    YAML::Node yGround;
    yGround.push_back(point->getX());
    yGround.push_back(point->getY());
    groundsList.push_back(yGround);
  }
  map["grounds"] = groundsList;

  YAML::Node portalsInList;
  for(size_t i = 0; i < portalsIn.size(); ++i){
    Shared::Point* point = portalsIn[i];
    YAML::Node yPortalIn;
    yPortalIn.push_back(point->getX());
    yPortalIn.push_back(point->getY());
    portalsInList.push_back(yPortalIn);
  }
  map["portals-in"] = portalsInList;

  YAML::Node portalsOutList;
  for(size_t i = 0; i < portalsOut.size(); ++i){
    Shared::Point* point = portalsOut[i];
    YAML::Node yPortalOut;
    yPortalOut.push_back(point->getX());
    yPortalOut.push_back(point->getY());
    portalsOutList.push_back(yPortalOut);
  }
  map["portals-out"] = portalsOutList;

  YAML::Node hordesList;
  for(size_t i = 0; i < hordes.size(); ++i){
    YAML::Node horde;
    Horde * thisHorde = hordes[i];
    horde.push_back(thisHorde->getEnemyType());
    horde.push_back(thisHorde->getEnemyQuantity());
    horde.push_back(thisHorde->getCoolDown());
    hordesList.push_back(horde);
  }
  map["hordes"] = hordesList;

  YAML::Emitter out;
  out << map;
  std::ofstream file(MAP_PATH+filename+EXTENSION);
  file << out.c_str();

  YAML::Node mapList = YAML::LoadFile(MAP_LIST_FILE);
  string mapName(filename+EXTENSION);
  mapList.push_back(mapName);
  YAML::Emitter listOut;
  listOut << mapList;
  std::ofstream listFile(MAP_LIST_FILE);
  listFile << listOut.c_str();

}
