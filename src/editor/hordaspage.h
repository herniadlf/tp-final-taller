#ifndef GTKMM_HORDAS_PAGE_H
#define GTKMM_HORDAS_PAGE_H
#include <gtkmm/box.h>
#include <gtkmm/comboboxtext.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>

class HordasPage : public Gtk::Box{
public:
  HordasPage(size_t &enemy1Q, size_t &coolDown, Gtk::Button &addHordButton);
  ~HordasPage();
  void resetButtons();
  int getEnemyType();
protected:
  void onTimeButton();
  void onEnemy1Button();
private:
  size_t &enemy1Q, &coolDown;
  Gtk::Button &addHordButton;
  Gtk::Frame timeFrame, enemyFrame;
  Gtk::Box timeHBox, enemyVBox, enemy1HBox, enemyTypeHBox;
  Gtk::Label timeLabel, enemy1Label, enemyTypeLabel;
  Glib::RefPtr<Gtk::Adjustment> timeAdjustment, enemy1Adjustment;
  Gtk::SpinButton timeSpinButton, enemy1SpinButton;
  Gtk::ComboBoxText enemyTypeComboBox;
};

#endif
