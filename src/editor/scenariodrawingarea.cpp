#include "scenariodrawingarea.h"
#include <glibmm/main.h>
#include <iostream>
#define ALLOCATION_SIZE 300
#define SQUARE_SIZE 30
#define TIME_TO_REDRAW 30
#define DEFAULT_MAP_SIZE 10

#define EMPTY_FLAG 0
#define START_PATH_FLAG 1
#define END_PATH_FLAG 2
#define ERASE_FLAG 3

ScenarioDrawingArea::ScenarioDrawingArea(std::vector<Shared::Point*>& points) :
  rowQ(DEFAULT_MAP_SIZE),
  colQ(DEFAULT_MAP_SIZE),
  points(points)
  {
    add_events(Gdk::BUTTON_PRESS_MASK);
    add_events(Gdk::POINTER_MOTION_MASK);
    Gtk::Allocation allocation = get_allocation();
    allocation.set_width(SQUARE_SIZE*colQ);
    allocation.set_height(SQUARE_SIZE*rowQ);
    Glib::signal_timeout().connect(sigc::mem_fun(*this, &ScenarioDrawingArea::on_timeout), TIME_TO_REDRAW);
}

ScenarioDrawingArea::~ScenarioDrawingArea(){}

bool ScenarioDrawingArea::on_timeout(){
  auto win = get_window();
  if(win){
    Gdk::Rectangle r(0, 0, get_allocation().get_width(), get_allocation().get_height());
    win->invalidate_rect(r, false);
  }
  return true;
}

void ScenarioDrawingArea::onEraseButton(){
  for(size_t i = 0; i < points.size(); ++i){
    delete(points[i]);
  }
  points.clear();
}

bool ScenarioDrawingArea::on_button_press_event(GdkEventButton *event){
    if( (event->type == GDK_BUTTON_PRESS) && (event->button == 1)){
      float x1 = calculate_coordenate_x(event->x);
      float y1 = calculate_coordenate_y(event->y);
      Shared::Point *point = new Shared::Point(x1, y1);
      if(!point->isInVector(points)){
        points.insert(points.end(), point);

      }else{
        delete(point);
      }
      queue_draw();
      return true;

    }
    return false;
}


bool ScenarioDrawingArea::on_motion_notify_event(GdkEventMotion *event){
    if (event->state == GDK_BUTTON1_MASK){
      float x1 = calculate_coordenate_x(event->x);
      float y1 = calculate_coordenate_y(event->y);
      Shared::Point *point = new Shared::Point(x1, y1);
      if(!point->isInVector(points)){
        points.insert(points.end(), point);
      }else{
        delete(point);
      }
    queue_draw();
    return true;

    }
    return false;
}


void ScenarioDrawingArea::draw_grid(const Cairo::RefPtr<Cairo::Context>& cr){
  cr->set_source_rgb(0.8,0,0);
  cr->set_line_width(1);
  float x, y;
  float x_i = 0;
  do{
    x = x_i*(SQUARE_SIZE);
    cr->move_to(x, 0);
    cr->line_to(x, SQUARE_SIZE*rowQ);
    cr->stroke();
    x_i++;
  }while(x_i < colQ+1);

  float y_i = 0;
  do{
    y = y_i*(SQUARE_SIZE);
    cr->move_to(0, y);
    cr->line_to(SQUARE_SIZE*colQ, y);
    cr->stroke();
    y_i++;
  }while(y_i < rowQ+1);
}

void ScenarioDrawingArea::resize(unsigned int rows, unsigned int cols){
  rowQ = rows;
  colQ = cols;
  Gtk::Allocation allocation = get_allocation();
  allocation.set_width(SQUARE_SIZE*colQ);
  allocation.set_height(SQUARE_SIZE*rowQ);
}

bool ScenarioDrawingArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr){
  Gtk::Allocation allocation = get_allocation();
  allocation.set_width(SQUARE_SIZE*colQ);
  allocation.set_height(SQUARE_SIZE*rowQ);
  pixelsWidth = allocation.get_width();
  pixelsHeight = allocation.get_height();
  cr->set_source_rgba(1.0, 1.0, 1.0, 0.8);
  cr->paint();
  draw_grid(cr);

  cr->set_line_width(2);
  cr->set_source_rgb(0,0.10,0.20);
  for(std::vector<Shared::Point*>::iterator it = points.begin(); it != points.end(); it++){
    Shared::Point* point = *it;
    float c_x = point->getX();
    float c_y = point->getY();
    cr->set_source_rgb(0,1,0);
    cr->rectangle(c_x*SQUARE_SIZE, c_y*SQUARE_SIZE, SQUARE_SIZE -1 , SQUARE_SIZE-1);
    cr->fill();
  }

  cr->stroke();
  return true;
}

float ScenarioDrawingArea::calculate_coordenate_x(float coord){
  float min_diff = 1000;
  float diff = 0;
  float new_coord = coord;
  float point = SQUARE_SIZE/2;
  while(point < pixelsWidth){
    diff = abs(coord-point);
    if(diff < min_diff){
      min_diff = diff;
      new_coord = point + 1 - SQUARE_SIZE/2;
    }
    point += SQUARE_SIZE;
  }
  return new_coord/SQUARE_SIZE;
}

float ScenarioDrawingArea::calculate_coordenate_y(float coord){
  float min_diff = 1000;
  float diff = 0;
  float new_coord = coord;
  float point = SQUARE_SIZE/2;
  while(point < pixelsHeight){
    diff = abs(coord-point);
    if(diff < min_diff){
      min_diff = diff;
      new_coord = point - SQUARE_SIZE/2;
    }
    point += SQUARE_SIZE;
  }
  return new_coord/SQUARE_SIZE;
}
