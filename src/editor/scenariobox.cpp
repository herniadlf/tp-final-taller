#include "scenariobox.h"
#include <iostream>
ScenarioBox::ScenarioBox(std::vector<Shared::Point*>& points, Gtk::Button &addCircuitButton) :
scenarioDrawingArea(points),
buttonsHBox(Gtk::ORIENTATION_HORIZONTAL),
scenarioVBox(Gtk::ORIENTATION_VERTICAL),
eraseButton("Erase")
{
  set_orientation(Gtk::ORIENTATION_VERTICAL);
  pack_start(buttonsHBox, Gtk::PACK_SHRINK);
  buttonsHBox.pack_start(eraseButton);

  pack_start(scenarioVBox);
  scenarioVBox.pack_start(scenarioScrolledWindow);
  scenarioScrolledWindow.add(scenarioGrid);
  scenarioScrolledWindow.set_border_width(10);
  scenarioScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);

  scenarioGrid.attach(scenarioDrawingArea, 0, 0, 100, 100);
  scenarioGrid.set_row_spacing(10);
  scenarioGrid.set_column_spacing(10);
  pack_start(addCircuitButton, Gtk::PACK_SHRINK);
  eraseButton.signal_clicked().connect(sigc::mem_fun(&scenarioDrawingArea, &ScenarioDrawingArea::onEraseButton));
  show_all_children();
}


ScenarioBox::~ScenarioBox(){}

void ScenarioBox::resizeScenarioArea(unsigned int rows, unsigned int cols){
  scenarioDrawingArea.resize(rows, cols);
}
