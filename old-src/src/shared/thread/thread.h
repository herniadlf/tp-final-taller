#ifndef __THREAD__
#define __THREAD__

#include <iostream>
#include <thread>

class Thread{
protected:
	 std::thread thread;
public:
	Thread() {}
	void join();
	void start();
	virtual void run() = 0;
	virtual ~Thread() {}
	Thread (const Thread&) = delete;
	Thread& operator=(const Thread&) = delete;
	Thread(Thread&& other);
	Thread& operator=(Thread&& other);
};

#endif
