#define _POSIX_C_SOURCE 200112L
#include "socket.h"

Socket::Socket() : fd(0) {}
Socket::~Socket() {
	close(this->fd);
}

int Socket::s_connect(const char* host, const char* service) {
	int s = 0;
	bool connected = false;
	struct addrinfo hints;
	struct addrinfo *result, *ptr;
	std::memset(&hints, 0 , sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	s = getaddrinfo(host, service, &hints, &result);
	if (s != 0){
		std::cerr << "Error in getaddrinfo: " << gai_strerror(s) << "\n";
		return -1;
	}
	for (ptr = result; (ptr != NULL) && (connected == false); ptr = ptr->ai_next){
		this->fd=socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
		if (this->fd == -1){
			std::cerr << "Error: " << strerror(errno) << "\n";
			freeaddrinfo(result);
			return -1;
		}
		s = connect(this->fd, ptr->ai_addr, ptr->ai_addrlen);
		if (s == -1){
			std::cerr << "Error: " << strerror(errno) << "\n";
			close(this->fd);
			freeaddrinfo(result);
			return -1;
		}
		connected = (s != -1);
	}
	freeaddrinfo(result);
	if (connected == false){
		std::cerr << "Error in the socket connection\n";
		return -1;
	}
	return 0;
}
int Socket::bind_n_listen(const char * service){
	int result=0;
	int skt;
	struct addrinfo hints;
	struct addrinfo *ptr;

	std::memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	result = getaddrinfo(NULL, service, &hints, &ptr);
	if (result != 0){
		std::cerr << "Error in getaddrinfo: " << gai_strerror(result) << "\n";
		return -1;
	}
	skt = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);
	if (skt == -1){
		std::cerr << "Error: " << strerror(errno) << "\n";
		freeaddrinfo(ptr);
		return -1;
	}
	result = bind(skt, ptr->ai_addr, ptr->ai_addrlen);
	if (result == -1){
		std::cerr << "Error: " << strerror(errno) << "\n";
		close(skt);
		freeaddrinfo(ptr);
		return -1;
	}
	freeaddrinfo(ptr);
	result = listen(skt,BACK_LOG);
	if (result == -1){
		std::cerr << "Error: " << strerror(errno) << "\n";
		close(skt);
		return -1;
	}
	this->fd = skt;
	return 0;
}
int Socket::s_accept(Socket* new_socket) {
	int peer = accept(this->fd, NULL, NULL);
	if (peer == -1){
		return -1;
	}
	new_socket->fd = peer;
	return 0;
}
int Socket::s_shutdown(const int mode){
	if (shutdown(this->fd, mode) == 0)
		return 0;
	else
		return -1;	
}
int Socket::s_send(const char *buff, unsigned int size) {
	unsigned int bytes_sent = 0;
	bool is_socket_error = false;
	bool is_socket_closed = false;
	while (bytes_sent < size && !is_socket_error && !is_socket_closed){
		int result = send(this->fd,&buff[bytes_sent],size-bytes_sent,MSG_NOSIGNAL);
		if (result < 0){
			std::cerr << "Error: " << strerror(errno) << "\n";
			is_socket_error = true;
		} else if (result == 0){
			std::cerr << "Remote socket closed\n";
			is_socket_closed = true;
		} else {
			bytes_sent += result;
		}
	}
	if (is_socket_closed || is_socket_error)
		return -1;
	else
		return 0;
}
int Socket::s_receive(char *buff, unsigned int size){
	unsigned int received = 0;
	int result = 0;
	bool is_socket_closed = false;
	bool is_socket_valid = true;
	while (received < size && is_socket_valid && is_socket_closed == false){
		result = recv(this->fd, &buff[received], size-received, MSG_NOSIGNAL);
		if (result == 0)
			is_socket_closed = true;
		else if (result < 0)
			is_socket_valid = false;
		else
			received += result;
	}
	if (is_socket_valid == false)
		return ERROR_SKT_RECV;
	else if (is_socket_closed && received == 0)
		return result;
	return received;
}
