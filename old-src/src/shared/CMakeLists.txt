set(CMAKE_CXX_FLAGS "-Wall -Werror -pedantic -pedantic-errors -O3 -ggdb -DDEBUG -fno-inline -std=c++11")
set(CMAKE_EXE_LINKER_FLAGS "-pthread")

add_subdirectory(socket)
add_subdirectory(thread)
add_subdirectory(mapa)
add_subdirectory(communication)
