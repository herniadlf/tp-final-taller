#include "communication.h"
#define INT_SIZE 4

//////////////////////////
// RECEIVER 
//////////////////////////

Receiver::Receiver(Socket* socket) : socket(socket), status(true) {
	unsigned int bytes_to_read = 0;
	int received = this->socket->s_receive((char*)&bytes_to_read,INT_SIZE);
	if (received < 1){
		this->status=false;
	} else {
		this->size = ntohl(bytes_to_read);
		this->message = new char[this->size];
	}
}

Receiver::~Receiver(){
	if (this->status)
		delete[] this->message;
}

const bool& Receiver::read(){
	if (this->status){
		int received = this->socket->s_receive(this->message,this->size);
		if (received < 1){
			this->status=false;
			delete[] this->message;
		}
	}
	return this->status;
}

const unsigned int& Receiver::get_size(){
	return this->size;
}

const char& Receiver::msg_code(){
	return this->message[0];
}

const char* Receiver::msg(){
	return this->message;
}

//////////////////////////
// SENDER 
//////////////////////////

Sender::Sender(Socket* socket) : socket(socket), status(true) {}

Sender::~Sender() {}

void Sender::run(const char* message, unsigned int total_size){
	this->socket->s_send(message, total_size);
}