#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#include "../socket/socket.h"

class Receiver{
private:
	Socket* socket;
	bool status;
	unsigned int size;
	char* message;
public:
	Receiver(Socket* socket);
	~Receiver();
	const bool& read();
	const unsigned int& get_size();
	const char& msg_code();
	const char* msg();
};

class Sender{
private:
	Socket* socket;
	bool status;
public:
	Sender(Socket* socket);
	~Sender();
	void run(const char* message, unsigned int total_size);
};

#endif
