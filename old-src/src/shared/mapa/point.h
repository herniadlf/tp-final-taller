#ifndef __POINT_H__
#define __POINT_H__
class Point{
public:
	const unsigned int x;
	const unsigned int y;
	Point(const unsigned int x,const unsigned int y);
	~Point();
};
#endif
