#include "mapa.h"
#include <iostream>
#include <algorithm>
using std::cout;

Mapa::Mapa(){
  paths.push_back(std::make_tuple(0, 0));
  paths.push_back(std::make_tuple(0, 1));
  paths.push_back(std::make_tuple(1, 2));
  paths.push_back(std::make_tuple(0, 3));
  paths.push_back(std::make_tuple(1, 4));
  paths.push_back(std::make_tuple(1, 5));

  grounds.push_back(std::make_tuple(0, 3));
  grounds.push_back(std::make_tuple(2, 3));
  grounds.push_back(std::make_tuple(3, 3));
  grounds.push_back(std::make_tuple(4, 3));
  grounds.push_back(std::make_tuple(5, 3));

  towers.push_back(std::make_tuple(1, 2));
  towers.push_back(std::make_tuple(0, 4));
}

Mapa::~Mapa(){}

bool Mapa::insert_tower_on(float coord_x, float coord_y){
  size_t length = grounds.size();
  double x2 = coord_x;
  double y2 = coord_y;
  for(size_t i = 0; i < length; ++i){
    double x1 = std::get<0>(grounds[i]);
    double y1 = std::get<1>(grounds[i]);
    if((fabs(x1 - x2) < 0.1) && (fabs(y1 - y2) < 0.1)){
      towers.push_back(std::make_tuple(coord_x, coord_y));
      grounds.erase(grounds.begin()+i);
      return true;
    }
  }
  return false;
}

bool Mapa::insert_projectils_on(float coord_x, float coord_y){
  projectils.push_back(std::make_tuple(coord_x, coord_y));
  return true;
}

const vector<std::tuple<float, float>> & Mapa::get_portals_in(){
  return portals_in;
}

const vector<std::tuple<float, float>> & Mapa::get_portals_out(){
  return portals_out;
}

const vector<std::tuple<float, float>> & Mapa::get_paths(){
  return paths;
}

const vector<std::tuple<float, float>> & Mapa::get_towers(){
  return towers;
}

const vector<std::tuple<float, float>> & Mapa::get_grounds(){
  return grounds;
}
const vector<std::tuple<float, float>> & Mapa::get_projectils(){
  return projectils;
}



// Eliminar en el futuro:

bool Mapa::can_tower_be_built_on(float coord_x, float coord_y){
  size_t length = grounds.size();
  double x2 = coord_x;
  double y2 = coord_y;
  for(size_t i = 0; i < length; ++i){
    double x1 = std::get<0>(grounds[i]);
    double y1 = std::get<1>(grounds[i]);
    if((fabs(x1 - x2) < 0.1) && (fabs(y1 - y2) < 0.1)){
      return true;
    }
  }
  return false;
}

bool Mapa::can_spell_be_casted_on(float coord_x, float coord_y){
  size_t length = paths.size();
  double x2 = coord_x;
  double y2 = coord_y;
  for(size_t i = 0; i < length; ++i){
    double x1 = std::get<0>(paths[i]);
    double y1 = std::get<1>(paths[i]);
    if((fabs(x1 - x2) < 0.1) && (fabs(y1 - y2) < 0.1)){
      return true;
    }
  }
  return false;
}

std::tuple<float, float> Mapa::get_portal_in_at(size_t pos){
  return portals_in[pos];
}

std::tuple<float, float> Mapa::get_portal_out_at(size_t pos){
  return portals_out[pos];
}

std::tuple<float, float> Mapa::get_path_at(size_t pos){
  return paths[pos];
}

std::tuple<float, float> Mapa::get_ground_at(size_t pos){
  return grounds[pos];
}

std::tuple<float, float> Mapa::get_tower_at(size_t pos){
  return towers[pos];
}

Point Mapa::get_portal_in_point_at(size_t pos){
  std::tuple<float, float> my_point = portals_in[pos];
  const int x = std::get<0>(my_point);
  const int y = std::get<1>(my_point);
  return Point(x, y);
}

Point Mapa::get_portal_out_point_at(size_t pos){
  std::tuple<float, float> my_point = portals_out[pos];
  const int x = std::get<0>(my_point);
  const int y = std::get<1>(my_point);
  return Point(x, y);
}

Point Mapa::get_path_point_at(size_t pos){
  std::tuple<float, float> my_point = paths[pos];
  const int x = std::get<0>(my_point);
  const int y = std::get<1>(my_point);
  return Point(x, y);
}

Point Mapa::get_ground_point_at(size_t pos){
  std::tuple<float, float> my_point = grounds[pos];
  const int x = std::get<0>(my_point);
  const int y = std::get<1>(my_point);
  return Point(x, y);
}

Point Mapa::get_tower_point_at(size_t pos){
  std::tuple<float, float> my_point = towers[pos];
  const int x = std::get<0>(my_point);
  const int y = std::get<1>(my_point);
  return Point(x, y);
}

size_t Mapa::get_portal_out_total(){
  return portals_out.size();
}

size_t Mapa::get_portal_in_total(){
  return portals_in.size();
}

size_t Mapa::get_paths_total(){
  return paths.size();
}

size_t Mapa::get_grounds_total(){
  return grounds.size();
}

size_t Mapa::get_towers_total(){
  return towers.size();
}
