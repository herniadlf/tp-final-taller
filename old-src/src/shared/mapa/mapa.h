#ifndef __MAPA_H__
#define __MAPA_H__
#include "point.h"
#include <vector>
#include <tuple>
using std::vector;
using std::tuple;

class Mapa{
  std::vector<std::tuple<float, float>> portals_in;
  std::vector<std::tuple<float, float>> portals_out;
  std::vector<std::tuple<float, float>> paths;
  std::vector<std::tuple<float, float>> grounds;
  std::vector<std::tuple<float, float>> towers;
  std::vector<std::tuple<float, float>> projectils;


public:
  Mapa();
  ~Mapa();
  bool insert_tower_on(float coord_x, float coord_y);
  bool insert_projectils_on(float coord_x, float coord_y);

  const vector<std::tuple<float, float>> & get_portals_in();
  const vector<std::tuple<float, float>> & get_portals_out();
  const vector<std::tuple<float, float>> & get_paths();
  const vector<std::tuple<float, float>> & get_towers();
  const vector<std::tuple<float, float>> & get_grounds();
  const vector<std::tuple<float, float>> & get_projectils();

// Eliminar en el futuro:
  bool can_tower_be_built_on(float coord_x, float coord_y);
  bool can_spell_be_casted_on(float coord_x, float coord_y);

  std::tuple<float, float> get_portal_in_at(size_t pos);
  std::tuple<float, float> get_portal_out_at(size_t pos);
  std::tuple<float, float> get_path_at(size_t pos);
  std::tuple<float, float> get_ground_at(size_t pos);
  std::tuple<float, float> get_tower_at(size_t pos);

  Point get_portal_in_point_at(size_t pos);
  Point get_portal_out_point_at(size_t pos);
  Point get_path_point_at(size_t pos);
  Point get_ground_point_at(size_t pos);
  Point get_tower_point_at(size_t pos);

  size_t get_portal_out_total();
  size_t get_portal_in_total();
  size_t get_paths_total();
  size_t get_grounds_total();
  size_t get_towers_total();
};

#endif
