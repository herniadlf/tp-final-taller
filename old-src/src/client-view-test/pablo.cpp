// Se compila con:
// g++ main.cpp $(pkg-config --cflags --libs gtkmm-3.0) -std=c++11

#include <gtkmm/window.h>
#include <gtkmm/application.h>
#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>
#include <gdkmm/general.h>
#include <cairomm/context.h>
#define SOIL_PATH "soil.png"
#define SOIL_X1 379
#define SOIL_X2 640
#define SOIL_Y1 15
#define SOIL_Y2 186
#define SOIL_WIDTH (SOIL_X2-SOIL_X1)
#define SOIL_HEIGHT (SOIL_Y2-SOIL_Y1)

class Canvas: public Gtk::DrawingArea {
public:
  Canvas();
protected:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
private:
  Glib::RefPtr<Gdk::Pixbuf> image;
  void drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y);
};

Canvas::Canvas() {
  Glib::RefPtr<Gdk::Pixbuf> tileMap = Gdk::Pixbuf::create_from_file(SOIL_PATH);
  this->image = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, SOIL_WIDTH, SOIL_HEIGHT);
  tileMap->scale(this->image, 0, 0, SOIL_WIDTH, SOIL_HEIGHT, -SOIL_X1, -SOIL_Y1, 1, 1, Gdk::InterpType::INTERP_BILINEAR);
}

void Canvas::drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y) {
  Gdk::Cairo::set_source_pixbuf(cr, image, x, y);
  cr->rectangle(x, y, SOIL_WIDTH, SOIL_HEIGHT);
  cr->fill();

  cr->set_source_rgb(1,0,0);
  cr->rectangle(x, y, SOIL_WIDTH, SOIL_HEIGHT);
  cr->stroke();
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();
  int x_init = 0;
  for(size_t y = 0; y < SOIL_HEIGHT/2 + 2; y+= SOIL_HEIGHT/2) {
    for(float x = x_init; x < width; x += SOIL_WIDTH) {
      drawImage(cr, x,y);
    }
    x_init = (x_init == 0 ? SOIL_WIDTH/2 : 0);
  }
  return true;
}

int main(int argc, char** argv)
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  Gtk::Window win;
  win.maximize();
  Canvas canvas;
  win.add(canvas);
  win.show_all();

  return app->run(win);
}

