// Se compila con:
// g++ main.cpp $(pkg-config --cflags --libs gtkmm-3.0) -std=c++11

#include <gtkmm/window.h>
#include <gtkmm/application.h>
#include <gtkmm/drawingarea.h>
#include <gdkmm/pixbuf.h>
#include <gdkmm/general.h>
#include <cairomm/context.h>
#include <list>
#include <iostream>
#define OUTDOOR_PATH "outdoor.png"
#define FORTRESS_PATH "fortress.png"
#define TOWER_PATH "tower.png"
#define TILE_WIDTH 158
#define TILE_HEIGHT 79
#define TOWER_WIDTH 57
#define TOWER_HEIGHT 99

class Point{
public:
	const unsigned int x;
	const unsigned int y;
	Point(const unsigned int x,const unsigned int y);
	~Point();
};

Point::Point(const unsigned int x,const unsigned int y) : x(x), y(y) {}
Point::~Point() {}

class Tower: public Gtk::DrawingArea{
public:
	Tower();
	bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
private:
	void drawImage(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
	Glib::RefPtr<Gdk::Pixbuf> image;
	std::list<Point> points;
};

class Path {
public:
	Path();
	bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
private:
	void drawImage(const Cairo::RefPtr<Cairo::Context>&cr, int x, int y);
	Glib::RefPtr<Gdk::Pixbuf> image;
	std::list<Point> points;
};

class Canvas: public Gtk::DrawingArea {
public:
  Canvas();
protected:
  bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr) override;
private:
  Glib::RefPtr<Gdk::Pixbuf> image;
  Path paths;
  Tower towers;
  void drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y);
};

Canvas::Canvas() {
  Glib::RefPtr<Gdk::Pixbuf> tileMap = Gdk::Pixbuf::create_from_file(OUTDOOR_PATH);
  this->image = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, TILE_WIDTH, TILE_HEIGHT);
  tileMap->scale(this->image, 0, 0, TILE_WIDTH, TILE_HEIGHT, 0, 0, 1, 1, Gdk::InterpType::INTERP_BILINEAR);
  this->image->composite_color_simple(TILE_WIDTH, TILE_HEIGHT, Gdk::InterpType::INTERP_BILINEAR,255, 2, 0, 0);
}

void Canvas::drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y) {
	Gdk::Cairo::set_source_pixbuf(cr, image, x, y);
  	cr->rectangle(x, y, TILE_WIDTH, TILE_HEIGHT);
  	cr->fill();
}

bool Canvas::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();
  int x_init = 0;
  for(size_t y = 0; y < height; y+= TILE_HEIGHT/2) { 
    for(float x = x_init; x < width; x += TILE_WIDTH) {
      this->drawImage(cr, x,y);
    }
    x_init = (x_init == 0 ? TILE_WIDTH/2 : 0);
  }
  paths.on_draw(cr);
  towers.on_draw(cr);
  return true;
}

Tower::Tower() {
  	Glib::RefPtr<Gdk::Pixbuf> tileMap = Gdk::Pixbuf::create_from_file(TOWER_PATH);
	this->image = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, TOWER_WIDTH, TOWER_HEIGHT);
	tileMap->scale(this->image, 0, 0, TOWER_WIDTH, TOWER_HEIGHT, 0, 0, 1, 1, Gdk::InterpType::INTERP_BILINEAR);
	points.emplace_back(2,2);
	points.emplace_back(4,3);
}

void Tower::drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y) {
  	Gdk::Cairo::set_source_pixbuf(cr, image, x, y);
  	cr->rectangle(x, y, TOWER_WIDTH, TOWER_HEIGHT);
  	cr->fill();
}

bool Tower::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  for (auto it=points.begin(); it!=points.end(); ++it){
    float x_position = (it->y % 2 == 0) ? it->x * TOWER_WIDTH : TOWER_WIDTH/2 + it->x * TOWER_WIDTH;
  	float y_position = it->y * TOWER_HEIGHT/2;
  	this->drawImage(cr,x_position,y_position);
  }
  return true;
}

Path::Path() {
  	Glib::RefPtr<Gdk::Pixbuf> tileMap = Gdk::Pixbuf::create_from_file(FORTRESS_PATH);
	this->image = Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, TILE_WIDTH, TILE_HEIGHT);
	tileMap->scale(this->image, 0, 0, TILE_WIDTH, TILE_HEIGHT, 0, 0, 1, 1, Gdk::InterpType::INTERP_BILINEAR);
	points.emplace_back(0,0);
  points.emplace_back(0,1);
  points.emplace_back(1,2);
  points.emplace_back(1,3);
  points.emplace_back(2,2);
  points.emplace_back(2,3);
  points.emplace_back(2,3);
	points.emplace_back(3,4);
}

void Path::drawImage(const Cairo::RefPtr<Cairo::Context>& cr, int x, int y) {
  	Gdk::Cairo::set_source_pixbuf(cr, image, x, y);
  	cr->rectangle(x, y, TILE_WIDTH, TILE_HEIGHT);
  	cr->fill();
}

bool Path::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
  for (auto it=points.begin(); it!=points.end(); ++it){
  	float x_position = (it->y % 2 == 0) ? it->x * TILE_WIDTH : TILE_WIDTH/2 + it->x * TILE_WIDTH;
  	float y_position = it->y * TILE_HEIGHT/2;
  	this->drawImage(cr,x_position,y_position);
  }
  return true;
}

int main(int argc, char** argv)
{
  auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

  Gtk::Window win;
  win.maximize();
  Canvas canvas;
  win.add(canvas);
  win.show_all();

  return app->run(win);
}

