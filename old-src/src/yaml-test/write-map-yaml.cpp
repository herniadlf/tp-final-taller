/*#include "socket/socket.h"*/
#include <fstream>
#include <string>
#include <list>
#include "yaml-cpp/yaml.h"



int main(int argc, char const *argv[])
{
	YAML::Node map;
	YAML::Node inner_portal_list;
	YAML::Node portal;
	portal.push_back(1);
	portal.push_back(2);
	inner_portal_list[1] = portal;
	map["Inner Portal"] = inner_portal_list;
	YAML::Node out_portal_list;
	YAML::Node out_portal;
	out_portal.push_back(2);
	out_portal.push_back(4);
	out_portal_list[1] = out_portal;
	map["Out Portal"] = out_portal_list;
	YAML::Node paths_list;
	YAML::Node path;
	path.push_back(2);
	path.push_back(3);
	paths_list[1] = path;
	YAML::Node path2;
	path2.push_back(2);
	path2.push_back(3);
	paths_list[2] = path2;
	map["Paths"] = paths_list;
	YAML::Emitter out;
	out << map;
	std::ofstream file("map.yaml");
	file << out.c_str();
	return 0;
}
