/*#include "socket/socket.h"*/
#include <iostream>
#include <string>
#include "yaml-cpp/yaml.h"

int main(int argc, char const *argv[])
{
	YAML::Node configuracion = YAML::LoadFile("config.yaml");
	std::cout << "CLIENT WORKING 3.0" << std::endl;
	YAML::Node partida = configuracion["Partida"];
	std::cout << "Config - Partida" << std::endl;
	for (YAML::const_iterator it=partida.begin(); it!=partida.end(); ++it)
		std::cout << it->first.as<std::string>() << ": " << it->second.as<int>() << std::endl; 
	std::cout << "Config - Monstruos" << std::endl;
	YAML::Node monstruos = configuracion["Monsters"];
	for (YAML::const_iterator m=monstruos.begin(); m!=monstruos.end(); ++m){
		YAML::Node mostro = *m;
		std::cout << "Mostro : " << mostro["nombre"].as<std::string>() << " Vida " << mostro["vida"].as<int>() << std::endl;
	}
	return 0;
}
