/*#include "socket/socket.h"*/
#include <fstream>
#include <string>
#include "yaml-cpp/yaml.h"

int main(int argc, char const *argv[])
{
	YAML::Node root;
	YAML::Node partida_map;
	YAML::Node partida;
	partida["Velocidad"] = 1;
	partida["Tiempo"] = 2;
	partida["Hordas"] = 7;
	partida_map["Partida"] = partida;
	root.push_back(partida_map);
	YAML::Node monsters_map;
	YAML::Node monsters;
	YAML::Node aux1;
	aux1["nombre"]="Bicho";
	aux1["vida"]=50;
	monsters.push_back(aux1);
	YAML::Node aux2;
	aux2["nombre"]="Fantasma";
	aux2["vida"]=100;
	monsters.push_back(aux2);
	monsters_map["Monsters"] = monsters;
	root.push_back(monsters_map);	
	YAML::Emitter out;
	out << root;
	std::ofstream file("written.yaml");
	file << out.c_str();
	return 0;
}
