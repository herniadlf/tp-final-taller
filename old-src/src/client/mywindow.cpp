#include "mywindow.h"

MyWindow::MyWindow():
  m_box(),
//  m_image("outdoor-floor.png"),
  m_Button_Quit("Quit")
{
  GTimeVal time;
  g_get_current_time(&time);
  set_title("Juego");
  set_border_width(10);
  set_default_size(600, 600);
  add(m_box);
  m_box.pack_start(m_Button_Quit, Gtk::PACK_SHRINK);
  m_Button_Quit.signal_clicked().connect(sigc::mem_fun(*this,
            &MyWindow::on_button_quit) );

//  m_box.pack_start(m_image);
  m_box.pack_start(m_area);
  show_all_children();
}

MyWindow::~MyWindow(){}

void MyWindow::on_button_quit()
{
  hide();
}
