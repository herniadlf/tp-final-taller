add_subdirectory(protocol-test)

set(CMAKE_CXX_FLAGS "-Wall -Werror -pedantic -pedantic-errors -O3 -ggdb -DDEBUG -fno-inline -std=c++11")
set(CMAKE_EXE_LINKER_FLAGS "-pthread")

link_directories(
    ${GTKMM_LIBRARY_DIRS}  )

include_directories(
	${GTKMM_INCLUDE_DIRS}
    "${PROJECT_SOURCE_DIR}/../shared"  )

add_executable(client
	main.cpp
	client.cpp
	mywindow.cpp
  myarea.cpp)

target_link_libraries(client
	${GTKMM_LIBRARIES}
	socket)

