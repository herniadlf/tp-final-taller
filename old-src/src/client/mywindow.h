#ifndef GTKMM_MYWINDOW_H
#define GTKMM_MYWINDOW_H
#include <gtkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
//#include <gtkmm/image.h>
//#include <gdkmm/pixbufanimation.h>
#include "myarea.h"
//#include "mybox1.h"

class MyWindow : public Gtk::Window{
public:
  MyWindow();
  ~MyWindow();
private:
//  MyBox1 m_box;
protected:
  void on_button_quit();
  void on_button_clicked2();
  Gtk::Box m_box;
//  Gdk::PixbufAnimation m_animation;
//  Gtk::Image m_image;
  Gtk::Button m_Button_Quit;
  MyArea m_area;
};

#endif
