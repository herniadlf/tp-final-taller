///////////
// Para correr: ./client [host] [port] [archivo-con-comandos]
//////////
#include "client.h"
#include "mywindow.h"
#include <gtkmm/application.h>


int main(int argc, char *argv[])
{
	Client cli;
	int result = 0;
	if (cli.validate_args(argc,argv)){
//		if (cli.start()){
			auto app = Gtk::Application::create();
			MyWindow window;
			app->run(window);

//			result = cli.end() ? EXIT_SUCCESS : EXIT_FAILURE;
	//	} else{
	//		result = EXIT_FAILURE;
	//	}
	} else {
		result = EXIT_FAILURE;
	}
	return result;
}
