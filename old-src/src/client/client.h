#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "../shared/socket/socket.h"
#include <string>
#include "mywindow.h"
#include <gtkmm/application.h>


class Client{
private:
	Socket socket;
	std::string host;
	std::string port;
	std::string path;
public:
	Client();
	~Client();
	bool validate_args(int argc, char *argv[]);
	bool start();
	bool end();
};
#endif
