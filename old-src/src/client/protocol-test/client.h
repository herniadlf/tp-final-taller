#ifndef __CLIENT_H__
#define __CLIENT_H__
#include "../../shared/socket/socket.h"
#include "../../shared/communication/communication.h"
#include <string>

class Client{
private:
	Socket socket;
	std::string host;
	std::string port;
public:
	Client();
	~Client();
	bool validate_args(int argc, char const *argv[]);	
	bool start();
	bool end();
};
#endif
