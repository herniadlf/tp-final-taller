#include "client.h"
#include <string>
#include <fstream>
#include <iostream>
#include "yaml-cpp/yaml.h"

#define INT_SIZE 4
#define CHAR_SIZE 1

Client::Client() {}
Client::~Client() {}
bool Client::validate_args(int argc, char const *argv[]) {
	if (argc != 3)
		return false;
	this->host = std::string(argv[1]);
	this->port = std::string(argv[2]);
	return true;
}

bool Client::start(){
	int result = this->socket.s_connect(this->host.data(),this->port.data());
	if (result != 0)
		return false;
	std::string input;
	std::cin >> input;
	while (input != "go"){
		input.clear();
		std::cin >> input;
	}
	this->socket.s_send(input.data(),input.size());
	// BEGIN-GAME
/*	std::string s("hola"); 
	// YAML
	YAML::Node map;
	YAML::Node inner_portal_list;
	YAML::Node portal;
	portal.push_back(1);
	portal.push_back(2);
	inner_portal_list[1] = portal;
	map["Inner Portal"] = inner_portal_list;
	YAML::Node out_portal_list;
	YAML::Node out_portal;
	out_portal.push_back(2);
	out_portal.push_back(4);
	out_portal_list[1] = out_portal;
	map["Out Portal"] = out_portal_list;
	YAML::Node paths_list;
	YAML::Node path;
	path.push_back(2);
	path.push_back(3);
	paths_list[1] = path;
	YAML::Node path2;
	path2.push_back(2);
	path2.push_back(3);
	paths_list[2] = path2;
	map["Paths"] = paths_list;
	YAML::Emitter out;
	out << map;
	//		
	unsigned int command_size = INT_SIZE + s.size() + out.size();
	char* send_msg = new char[command_size];
	unsigned int size_aux = htonl(s.size()+out.size());
	memcpy(send_msg,&size_aux,INT_SIZE);
	std::cout << "msg " << send_msg << std::endl;
	memcpy(send_msg + INT_SIZE,s.data(),s.size());
	std::cout << "msg " << send_msg << std::endl;
	memcpy(send_msg + INT_SIZE + s.size(),out.c_str(),out.size());
	std::cout << "msg " << send_msg << std::endl;*/
	YAML::Node command;
	YAML::Node arguments;
	arguments.push_back(2);
	arguments.push_back(4);
	command["new-tower"] = arguments;
	YAML::Emitter yaml_command;
	yaml_command << command;
	char command_code = 'T';
	unsigned int total_size = INT_SIZE+CHAR_SIZE+yaml_command.size();
	char *send_msg = new char[total_size];
	unsigned int size_aux = htonl(CHAR_SIZE+yaml_command.size());
	unsigned int bytes = 0;
	memcpy(send_msg+bytes,&size_aux,INT_SIZE);
	bytes += INT_SIZE;
	memcpy(send_msg+bytes,&command_code,CHAR_SIZE);
	bytes += CHAR_SIZE;
	memcpy(send_msg+bytes, yaml_command.c_str(), yaml_command.size()); 
	Sender sender(&(this->socket));
	sender.run(send_msg, total_size);
	delete[] send_msg;
	return true;
}

bool Client::end(){
	int result = this->socket.s_shutdown(SHUT_RDWR);
	if (result != 0)
		return false;
	return true;
}
