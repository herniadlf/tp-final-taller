///////////
// Para correr: ./client [host] [port]
//////////
#include "client.h"

int main(int argc, char const *argv[])
{
	Client cli;
	int result = 0;
	if (cli.validate_args(argc,argv)){
		if (cli.start()){
			result = cli.end() ? EXIT_SUCCESS : EXIT_FAILURE;
		} else{
			result = EXIT_FAILURE;
		}
	} else {
		result = EXIT_FAILURE;
	}
	return result;
}
