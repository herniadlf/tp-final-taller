#include "client.h"
#include <string>
#include <fstream>
#include <iostream>

#define ID_WIDTH 4
#define M_COMMAND_WIDTH 4

Client::Client() {}

Client::~Client() {}

bool Client::validate_args(int argc, char *argv[]) {
	if (argc != 4)
		return false;
	this->host = std::string(argv[1]);
	this->port = std::string(argv[2]);
	this->path = std::string(argv[3]);
	return true;
}

/*
bool Client::start(){
	int result = this->socket.s_connect(this->host.data(),this->port.data());
	if (result != 0)
		return false;
	std::string input;
	std::cin >> input;
	while (input != "go"){
		input.clear();
		std::cin >> input;
	}
	this->socket.s_send(input.data(),input.size());
	std::ifstream file(this->path);
	while (file.good()){
		unsigned int command_size = ID_WIDTH+M_COMMAND_WIDTH + 1;
		char *send_msg = new char[command_size];
		file.read(send_msg,command_size);
		if (file.eof()){
			delete[] send_msg;
			break;
		}
		this->socket.s_send(send_msg,command_size);
		delete[] send_msg;
		char *receive_msg = new char[command_size+1];
		int received = this->socket.s_receive(receive_msg,command_size);
		if (received < 1){
			this->socket.s_shutdown(SHUT_RDWR);
			delete[] receive_msg;
			return false;
		}
		receive_msg[command_size] = '\0';
		std::cout << "Mensaje recibido: " << receive_msg << std::endl;
		delete[] receive_msg;
	}
	return true;
}

bool Client::end(){
	int result = this->socket.s_shutdown(SHUT_RDWR);
	if (result != 0)
		return false;
	return true;
}*/
