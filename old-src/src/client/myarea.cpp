#include "myarea.h"
#include <cairomm/context.h>
#include <giomm/resource.h>
#include <gdkmm/general.h> // set_source_pixbuf()
#include <glibmm/fileutils.h>
#include <iostream>
#include <gdkmm/pixbufanimation.h>
using std::cout;

MyArea::MyArea()
{
  try
  {
    // The fractal image has been created by the XaoS program.
    // http://xaos.sourceforge.net
//    Gdk::PixbufAnimation m_animation;
//    m_animation.create_from_file("outdoor-floor.png");
//    GTimeVal time;
//    Gdk::PixbufAnimationIter m_iter = m_animation.get_iter(&time);
//m_image = m_iter.get_pixbuf();

//  m_image = Gdk::Pixbuf::create_from_file("outdoor-floor.png");
//  m_image = Gdk::Pixbuf::create_from_file("outdoor-floor.png");
  Glib::RefPtr<Gdk::Pixbuf> m_aux = Gdk::Pixbuf::create_from_file("outdoor-floor.png");
//  m_image = m_aux->apply_embedded_orientation();
//  Gdk::InterpType m_type = Gdk::INTERP_TILES;

//  m_aux->composite(m_image, 5, 5, 10, 10, 0, 0, 1, 1, m_type, 255);
  m_image = Gdk::Pixbuf::create_subpixbuf(m_aux, 0, 0, 160, 87);
//  m_aux->copy_area(0, 0, 200, 200, m_image, 200, 200);
  }
  catch(const Gio::ResourceError& ex)
  {
    std::cerr << "ResourceError: " << ex.what() << std::endl;
  }
  catch(const Gdk::PixbufError& ex)
  {
    std::cerr << "PixbufError: " << ex.what() << std::endl;
  }

  // Show at least a quarter of the image.
  if (m_image){
    set_size_request(m_image->get_width()/2, m_image->get_height()/2);
    std::cout << "Entra" << '\n';
  }
    }

MyArea::~MyArea()
{
}

bool MyArea::on_draw(const Cairo::RefPtr<Cairo::Context>& cr)
{
  if (!m_image)
    return false;

  Gtk::Allocation allocation = get_allocation();
  const int width = allocation.get_width();
  const int height = allocation.get_height();

  //m_image.apply_embedded_orientation();

  // Draw the image in the middle of the drawing area, or (if the image is
  // larger than the drawing area) draw the middle part of the image.
  Gdk::Cairo::set_source_pixbuf(cr, m_image,
    (width - m_image->get_width())/2, (height - m_image->get_height())/2);

  cr->paint();

  return true;
}
