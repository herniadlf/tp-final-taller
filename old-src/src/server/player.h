#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <string>

class Player{
private:
	unsigned int id;
	std::string name;
public:
	Player(unsigned int id, std::string name);
	~Player();
	std::string description();
};

#endif
