#ifndef __GAME_ROOM_H__
#define __GAME_ROOM_H__

#include <vector>
#include "player.h"
#include "scenario.h"
#include "game_state.h"

class Game_Room{
protected:
	unsigned int id;
	std::vector<Player*> players;
	Scenario scenario;
	Game_State* state;
public:
	Game_Room(unsigned int id);
	~Game_Room();
	void new_player(unsigned int id, std::string name);
	void new_tower(unsigned int x, unsigned int y);
	void begin_game();
};

#endif
