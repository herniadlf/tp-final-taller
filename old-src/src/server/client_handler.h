#ifndef __CLIENT_HANDLER_H__
#define __CLIENT_HANDLER_H__
#include "../shared/thread/thread.h"
#include "../shared/socket/socket.h"
#include "../shared/communication/communication.h"
#include "executor.h"
#include "message_handler.h"
#include "game_room.h"
#include <mutex>
#include <string>
#include <vector>
#include <list>

class Client_Handler: public Thread{
private:
	Socket* peer;
	Message_Handler& message_handler;
	Game_Room* room;
	bool& running;
	bool connected;
	std::mutex mut;
public:
	Client_Handler(Socket* peer,
				    Message_Handler& message_handler,
					Game_Room* room,
					bool& running);
	~Client_Handler();
	void run() override;
	void disconnect();
	bool is_connected() const;
};

class Accept_Handler: public Thread{
private:
	Socket& listener;
	Message_Handler& message_handler;
	std::list<Game_Room*>& rooms;
	bool& running;
	std::vector<Client_Handler*> clients;
public:
	Accept_Handler(Socket& listener,
				   Message_Handler& message_handler,
				   std::list<Game_Room*>& rooms,
				   bool& running);
	~Accept_Handler();
	void run() override;
};

#endif
