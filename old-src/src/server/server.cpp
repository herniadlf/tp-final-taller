#include "server.h"
#include <string>

Server::Server() : running(true) {
	Tower_Executor *tower = new Tower_Executor();
	this->message_handler.insert('T', tower);
}
Server::~Server() {
	for (auto it=this->rooms.begin(); it!=this->rooms.end(); ++it)
		delete *it;	
}

bool Server::validate_args(int argc, char** argv){
	if (argc != 2)
		return false;
	this->port = std::string(argv[1]);
	return true;
}

bool Server::run(){
	int result = this->socket.bind_n_listen(this->port.data());
	if (result != 0)
		return false;
	Game_Room *room = new Game_Room(1);
	this->rooms.push_back(room);
	Accept_Handler accepter(this->socket,
							this->message_handler,
							this->rooms,
							this->running);
	accepter.start();
	std::string input;
	std::cin >> input;
	while (input != "q"){
		input.clear();
		std::cin >> input;
	}
	this->running = false;
	this->socket.s_shutdown(SHUT_RDWR);
	accepter.join();
	return true;
}
