#ifndef __SCENARIO_H__
#define __SCENARIO_H__

#include <string>

class Scenario{
protected:
	std::string name;
public:
	Scenario(std::string name);
	~Scenario();
	const std::string& get_name();
};

#endif
