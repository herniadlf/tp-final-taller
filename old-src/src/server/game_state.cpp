#include "game_state.h"
#include <iostream>

/////////////////////
// GAME_STATE
/////////////////////

Game_State::Game_State() {}
Game_State::~Game_State() {}

/////////////////////
// WAITING
/////////////////////

Waiting::Waiting() {}
Waiting::~Waiting() {}
void Waiting::run(Scenario& scenario,
					 std::vector<Player*>& players) {
	std::cout << "STARTING WAITING - MAP ";
	std::cout << scenario.get_name() << std::endl;
}

/////////////////////
// PLAYING
/////////////////////

Playing::Playing() {}
Playing::~Playing() {}
void Playing::run(Scenario& scenario,
					 std::vector<Player*>& players) {
	std::cout << "STARTING PLAYING ROOM - MAP ";
	std::cout << scenario.get_name() << std::endl;
	std::cout << "PLAYER: " << players.front()->description() << std::endl;
}