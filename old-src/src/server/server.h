#ifndef __SERVER_H__
#define __SERVER_H__

#include "../shared/socket/socket.h"
#include "client_handler.h"
#include <string>
#include <vector>

class Server{
private:
	Socket socket;
	std::string port;
	bool running;
	std::list<Game_Room*> rooms;
	Message_Handler message_handler;
public:
	Server();
	~Server();
	bool validate_args(int argc, char** argv);
	bool run();
};
#endif
