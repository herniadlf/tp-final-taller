#include "client_handler.h"
#include <string>
#define  INT_SIZE 4

////////////////////////
// Client_Handler
////////////////////////

Client_Handler::Client_Handler(Socket* peer, 
				   				Message_Handler& message_handler,
								Game_Room* room, 
								bool& running):
		peer(peer), message_handler(message_handler), room(room), running(running) {
	room->new_player(1,"Hernan");			
}

Client_Handler::~Client_Handler() {}

void Client_Handler::run(){
	std::cout << "New Player connected" << std::endl;
	//Pido confirmacion al cliente para comenzar.
	bool launch_game = false;
	while (!launch_game && this->running){
		char *confirmation = new char[2+1];
		peer->s_receive(confirmation,2);
		confirmation[2] = '\0';
		std::string input(confirmation);
		launch_game = (input == "go");
		input.clear();
		delete[] confirmation;
	}
	this->room->begin_game();
	while (this->running){
		std::unique_lock<std::mutex> lock(this->mut);
		Receiver receiver(peer);
		if (receiver.read()){
			Executor* executor = this->message_handler.get(receiver.msg_code());
			executor->do_execute(this->room,receiver.msg(), receiver.get_size());
			if (executor->has_message()){
				//Sender s
			}
		} else {
			peer->s_shutdown(SHUT_RDWR);
			break;
		}
	}
	this->disconnect();
}	

void Client_Handler::disconnect(){
	std::unique_lock<std::mutex> lock(this->mut);
	this->peer->s_shutdown(SHUT_RDWR);
	delete this->peer;
}		

bool Client_Handler::is_connected() const{
	return this->connected;
} 

////////////////////////
// Accept_Handler
////////////////////////

Accept_Handler::Accept_Handler(Socket& listener, 
				   			   Message_Handler& message_handler,
							   std::list<Game_Room*>& rooms,
							   bool& running) :
		listener(listener), message_handler(message_handler),
				rooms(rooms), running(running) {}



Accept_Handler::~Accept_Handler() {
	for (unsigned int i = 0; i < this->clients.size(); i++)
		delete this->clients.at(i);	
}

void Accept_Handler::run(){
	while (this->running){
		Socket* peer = new Socket();
		int result = this->listener.s_accept(peer);
		if (result == 0){
			Client_Handler *handler = new Client_Handler(peer,
													this->message_handler,
													this->rooms.front(),
													this->running);
			this->clients.push_back(handler);
			handler->start();
		} else {
			delete peer;
		}
	}
	for (unsigned int i = 0; i < this->clients.size(); i++)
		this->clients.at(i)->join();
}
