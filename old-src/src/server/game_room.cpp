#include "game_room.h"
#include <iostream> // RDELETEEE

/////////////////////
// GAME_ROOM
/////////////////////

Game_Room::Game_Room(unsigned int id) : 
					id(id), scenario("Mapa-1") {
	this->state = new Waiting();
	this->state->run(this->scenario,this->players);						
}
Game_Room::~Game_Room() {
	for (unsigned int i=0; i < this->players.size(); i++)
		delete this->players.at(i);
	delete this->state;
}
void Game_Room::new_player(unsigned int id, std::string name){
	Player *player = new Player(id,name);
	this->players.push_back(player);
}

void Game_Room::new_tower(unsigned int x, unsigned int y){
	std::cout << "Nueva Torre X: " << x << " Y: " << y << std::endl;
}

void Game_Room::begin_game() {
	delete this->state;
	this->state = new Playing();
	this->state->run(this->scenario,this->players);
}