#ifndef __EXECUTOR_H__
#define __EXECUTOR_H__

#include <map>
#include "game_room.h"
#include "yaml-cpp/yaml.h"

class Executor{
public:
	Executor();
	virtual ~Executor();
	virtual void do_execute(Game_Room* game, 
			const char* message, const unsigned int& msg_size) = 0;
	virtual bool has_message() = 0;
};

class Tower_Executor: public Executor{
public:
	Tower_Executor();
	void do_execute(Game_Room* game, 
			const char* message, const unsigned int& msg_size) override;
	bool has_message() override;
};
#endif
