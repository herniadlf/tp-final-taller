#include "message_handler.h"

Message_Handler::Message_Handler() {}
Message_Handler::~Message_Handler() {
	auto messages_begin = this->handler.begin();
	auto messages_end = this->handler.end();
	for (auto it=messages_begin; it!=messages_end; ++it)
		delete it->second;
}

void Message_Handler::insert(const char key,Executor* value){
	this->handler.insert(std::pair<const char, Executor*>(key,value));
}

Executor* Message_Handler::get(const char& key) {
	return handler[key];
}

