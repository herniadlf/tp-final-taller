////////////////
// Para correr ./server [port]
// Se termina la ejecución ingresando 'q' por stdin
///////////////
#include "server.h"

int main(int argc, char *argv[]) {
	Server server;
	int result = EXIT_SUCCESS;
	if (server.validate_args(argc,argv)){
		server.run();
	} else {
		result = EXIT_FAILURE;
	}
	return result;
}
