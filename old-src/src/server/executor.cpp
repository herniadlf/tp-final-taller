#include "executor.h"
#define CHAR_SIZE 1
#include <iostream> // RDELETEEE
#include <cstring> 
Executor::Executor() {}
Executor::~Executor() {}

Tower_Executor::Tower_Executor() {}

void Tower_Executor::do_execute(Game_Room* game, 
	const char* message, const unsigned int& msg_size){
	char *yaml_msg = new char[msg_size+1];
	memcpy(yaml_msg,message,msg_size);
	yaml_msg[msg_size] = '\0';
	YAML::Node command = YAML::Load(yaml_msg);
	std::string command_aux = command.begin()->first.as<std::string>();
	if (command_aux == "Tnew-tower"){
		YAML::Node arguments = command[command_aux];
		unsigned int x = arguments[0].as<unsigned int>();
		unsigned int y = arguments[1].as<unsigned int>();
		game->new_tower(x,y);		
	}
	delete[] yaml_msg;
}

bool Tower_Executor::has_message() { return false; } 
