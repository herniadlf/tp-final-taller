#ifndef __GAME_STATE_H__
#define __GAME_STATE_H__

#include <vector>
#include "scenario.h"
#include "player.h"

class Game_State{
public:
	Game_State();
	virtual ~Game_State();
	virtual void run(Scenario& scenario,
					 std::vector<Player*>& players) = 0;
};

class Waiting: public Game_State{
public:
	Waiting();
	~Waiting();
	void run(Scenario& scenario,
					 std::vector<Player*>& players) override;
};

class Playing: public Game_State{
public:
	Playing();
	~Playing();
	void run(Scenario& scenario,
					 std::vector<Player*>& players) override;
};

#endif
