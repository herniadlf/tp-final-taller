#include "player.h"

Player::Player(unsigned int id, std::string name) :
				id(id), name(std::move(name)) {}
Player::~Player() {}

std::string Player::description() {
	std::string descrip("Jugador_");
	descrip += std::to_string(id);
	descrip += ": ";
	descrip += name;
	return std::move(descrip);
}
