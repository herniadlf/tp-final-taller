#ifndef __MESSAGE_HANDLER_H__
#define __MESSAGE_HANDLER_H__
#include <map>
#include "executor.h"

class Message_Handler{
private:
	std::map<const char,Executor*> handler;
public:
	Message_Handler();
	~Message_Handler();
	void insert(const char key,Executor* value);
	Executor* get(const char& key);
};

#endif
