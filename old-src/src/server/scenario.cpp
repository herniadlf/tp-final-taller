#include "scenario.h"

Scenario::Scenario(std::string name) :
					name(std::move(name)) {}

Scenario::~Scenario() {}

const std::string& Scenario::get_name(){
	return this->name;
}					 

