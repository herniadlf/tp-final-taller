cmake_minimum_required (VERSION 3.5)

set(CMAKE_CXX_FLAGS "-Wall -Werror -pedantic -pedantic-errors -O3 -ggdb -DDEBUG -fno-inline -std=c++11")
set(CMAKE_EXE_LINKER_FLAGS "-pthread")

project(client)
project(server)
project(shared)

find_package(PkgConfig)
pkg_check_modules(GTKMM gtkmm-3.0) 

add_subdirectory(src)
