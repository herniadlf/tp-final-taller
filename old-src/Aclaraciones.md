# Aclaraciones #

Para utilizar estas carpetas para ir probando lo de yaml o leyendolo al menos, fijense que el codigo fuente está en src/client.

Si hacen un ".cpp" nuevo, fijensé de meterse al archivo src/client/CMakeLists.txt y repliquen lo que está hecho para los dos archivos existentes (los 3 pasos: ejecutable, link library, properties).

Por otro lado, para compilar la primera vez parensé en src/ y corran "cmake ..". Esto crea los Makefiles para compilar. Acto seguido, correr en consola "make". Esto compila y genera los ejecutables. Metansé a src/src/client y ahí tendrán los ejecutables. En caso de necesitar buildear de nuevo cualquier archivo .cpp fuente que hayan puesto/modificado en la carpeta "cliente", basta con correr "make" desde la carpeta src/src/client/

Es un lío pero tengo que perfeccionarlo para que no queden las cosas tan separadas en distintos lados.