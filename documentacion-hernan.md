# Organización del proyecto (idea) #

Pienso que estaría bueno tener el src dividido, en principio, en tres paquetes: client, server, shared. 

## Client ##

Tendrá toda la implementación que genera el ejecutable para el cliente.

## Server ##

Tendrá toda la implementación que generar el ejecutable para el server.

## Shared ##

Pienso que puede estar bueno dejar acá todas las librerías compartidas tanto por el client y el server. Ejemplo: socket, yaml, gtkmm, etc.

# CMake #

## Instalación ##

sudo apt-get install cmake

## Buildear nuestro proyecto ##

La explicación anterior de los paquetes es porque en cada paquete el CMake necesita un archivo llamado "CMakeLists.txt" que le da instrucciones de cómo generar los Makefile.

Les explico mejor en persona, pero pueden ir chusmeando cada uno de estos archivos para ver si lo pueden comprender.

# Yaml #

## Instalación (requiere CMake) ##

Veremos como unificar la librería luego, pero si quieren hacer pruebas aparte tienen que meterse a la carpeta yaml-cpp y luego correr: (si les falla algo, usen sudo antes de cada comando)

* mkdir build
* cd build
* cmake ..
* make
* make install

Con esto corrido, ya pueden crear cualquier archivo y usar #include "yaml-cpp/yaml.h" y tienen toda la librería incluída.